# Pushit2Mobile

Pushit2Mobile is a content distribution platform that allows users to push desired content to clients that implement the Pushit2Mobile SDK. 
The platform consists of three pieces that interact with each other over the internet.
   - Native SDK's (iOS and Android). Enables developers to create applications using the distributed content;
   - Pushit2Mobile CMS (Content Management System) is a user interface for content owners to manage the distribution of their content;
   - REST API provides communication between the Pushit2Mobile CMS and the SDK's over the HTTP protocol.

## Requirements
 - php 7.*
 - MySQL / MariaDB
 - composer
 - nodejs
 - npm
 - redis

## Installation

To install and configure the Pushit2Mobile API and CMS please follow these steps

- Clone the repository
`git clone git@github.com:Eskimo2/snique-platform.git`

- cd into the directory
`cd snique-platform`

- ONLY IF INSTALLING LOCALLY! fetch the `development` branch
`git fetch origin development:development`
`git checkout development`

- Create a .env file
`touch .env`

- Copy the contents from the `.env.example` file and amend them according to your server and database setup.

- create a MySQL / MariaDB database and update these credentials in the `.env` file:
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=dbname
DB_USERNAME=homestead
DB_PASSWORD=secret
```

- Install composer dependencies
`composer install`

- Install javascript dependencies
`npm install`
or
`yarn install`

- Migrate and seed the database
`php artisan migrate --seed`

- generate the application key
`php artisan key:generate`

- Mix all the assets
for development
`npm run watch`
for production
`npm run production`

- Connect to `redis`. In `.env` update these credentials:
```
QUEUE_DRIVER=sync
REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379
```
- set `QUEUE_DRIVER=redis` and update the other credentials

- run redis daemon
`php artisan queue:work redis`

## Homestead redis installation
to Set up REDIS to work on UNIT tests:

For Homestead 0.4 above. Due to redis security setting, it binds only for 127.0.0.1

SSH to you VM.

`sudo vi /etc/redis/redis.conf`

Scroll to the line bind `127.0.0.1` add extra IP address `192.168.10.10`, it will look like this

`bind 127.0.0.1 192.168.10.10`

save and exit.

Restart redis server and exit your server. 
`sudo /etc/init.d/redis-server restart`

## Running phpunit tests

- [OPTIONAL] Create an alias for testing
`alias test="./vendor/bin/phpunit"`

- Create a `phpunit.xml` file. You may use `phpunit.example.xml` file as reference

- make sure you update these credentials in `phpunit.xml` according to your needs
```
<env name="APP_ENV" value="testing"/>
<env name="APP_URL" value="http://snique.app"/>
<env name="DB_HOST" value="127.0.0.1"/>
<env name="DB_PORT" value="33060"/>
<env name="DB_DATABASE" value="snique"/>
<env name="CACHE_DRIVER" value="array"/>
<env name="SESSION_DRIVER" value="array"/>
<env name="QUEUE_DRIVER" value="sync"/>
<env name="MAIL_DRIVER" value="log"/>
```

- run the test suite
`test tests/`

## Collaboration

To collaborate please follow the installation steps. Once everything is setup and working locally create a new branch from `development` branch with the naming convention as follows `feature/name_of_the_feature`, `feature/issue_number` or `bugfix/issue_number`. 

You should only push straight to `development` branch if the changes you've made affect no more than 1 file.
