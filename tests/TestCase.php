<?php

namespace Tests;

use App\Ad;
use Generate;
use App\User;
use App\Client;
use App\Channel;
use App\Device;
use App\Content;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function callWithResponse($method, $uri, $payload = [], $token = null, $deviceUid)
    {
        $device = Device::where('uid', $deviceUid)->first();
        $authHeader = [
            'Content-Type'  =>  'application/json',
            'HTTP_DEVICE-UUID'   => $deviceUid,
            'HTTP_ONESIGNAL-UUID'   => ($device && $device->onesignal_uid) ? $device->onesignal_uid : 'e8983a37-7ede-4aa2-be6e-efc4a810e9f5',
        ];

        $authHeader['HTTP_Authorization'] = $token;

        // $response = $this->call($method, $uri, $payload, [], [], $authHeader, []);
         $response = $this->withHeaders($authHeader)->json($method, $uri, $payload);

        return json_decode($response->getContent());
    }

    public function getJwtTokenFor(Client $client)
    {
        $jwt = new \App\Services\JwtService;
        $jwt->setPayload([ 'secret' => encrypt($client->secret) ]);

        return 'Bearer ' . $jwt->generateToken();
    }

    public function mockContent(User $user, $channel = null)
    {
        $content = Content::create([
            'uid'   =>  Generate::noDashUid(),
            'title' =>  'test',
            'description' =>  'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque harum, ipsam velit necessitatibus at voluptates, eos fuga! Repellat dolores labore similique cumque molestiae praesentium illo quia, velit facilis voluptates ipsum.',
            'resource_uri'  =>  'test',
            'categories'  =>  'somecat',
            'user_id'   =>  $user->id,
        ]);

        // if ($channel) {
        //     // attach to channel
        //     $channel->contents()->attach($content->id);
        // }

        // dd($channel->contents);

        return $content;
    }

    public function mockApiSetup()
    {
        $ret = new \stdClass();

        $ret->device = Device::create([
            'uid'   =>  Generate::uid(),
            'onesignal_uid'    =>  '28fa2c7a-609f-41c9-8464-b3ae3ade8df4',
        ]);

        $ret->user = User::create([
            'name'  =>  'testing user', 
            'email' =>  'thisisaetst@noemail.com', 
            'password' => bcrypt('mypass'), 
            'uid'   =>  Generate::noDashUid(),
        ]);

        $ret->ad = Ad::create([
            'uid'   =>  Generate::uid(),
            'title' =>  'ad title',
            'description'   =>  'ad descr',
            'user_id'   =>  $ret->user->id,
        ]);


        $ret->channel = Channel::create([
            'label' =>  'testing channel',
            'name'  =>  'testing channel',
            'description'   =>  'descritpinos of the channe;',
            'asset_path'    =>  '/test/img.jpg',
            'uid'   =>  Generate::noDashUid(),
            'user_id'   =>  $ret->user->id,
        ]);

        $ret->client = Client::create([
            'uid'   =>  Generate::noDashUid(),
            'name'   =>  'test client',
            'description'   =>  'description',
            'user_id'   =>  $ret->user->id,
            'secret'    =>  'clients_secret',
            'onesignal_app_id'  =>  'eed077b2-fec1-49ed-aae1-8149855ae434',
            'onesignal_api_key' =>  'YTlhOTU5MzctMzQyNy00NjcyLWIwNmYtYmRjMjBlYWRmZTU5',
        ]);

        $ret->content = $this->mockContent($ret->user);

        $ret->content->channels()->attach([$ret->channel->id]);

        $ret->client->channels()->attach([$ret->channel->id]);
        $ret->client->devices()->attach([$ret->device->id]);

        return $ret;
    }
}
