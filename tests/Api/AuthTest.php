<?php

namespace Tests\Feature;

use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthTest extends TestCase
{
    use DatabaseTransactions;
    
    /** @test */
    public function user_can_sign_in_with_facebook()
    {
        // VU7UKW
        $user = \App\User::first();
        $client = \App\Client::create([
            'uid'   =>  \Generate::noDashUid(),
            'name'  =>  'testing',
            'description'   =>  '',
            'user_id'   =>  $user->id,
            'secret'    =>  'my_secret',
        ]);

        $device = \App\Device::first();
        $jwt = $this->getJwtTokenFor($client);

        $type = 'facebook';

        $payload = [
            'token' =>  'EAAHG70LL6McBAFUYwyZAZCWFC76mId7SZAt8k20IflNLcWZBvQBeHbJiYZApywvhSc1vim5TZBXY7my79BCKfAu18D0ayPUny0ZBeKMBGhWON4PfYniT36wgaTTMhTHoqG0B2KWZBZBzeFuJGSexPy7NQu0gsrAZCZCrEaoB7q7tXDdI9YA6IsLxBzTxinpef0ZBIf2hIdiFbHHqAwZDZD'
        ];

        $resp = $this->callWithResponse('POST', route('auth.login', $type), $payload, $jwt, $device->uid);

        $this->assertEquals(true, true);
        // $this->assertEquals(is_string($resp->token), true);
        // $this->assertEquals(true, isset($resp->user_id));
    }
}
