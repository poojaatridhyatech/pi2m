<?php

namespace Tests\Feature;

use App\User;
use App\Device;
use App\Channel;
use App\Bookmark;
use Tests\TestCase;
use App\Subscription;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SubscribeTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_can_subscribe_to_a_channel()
    {
        $ti = $this->mockApiSetup();
        $jwt = $this->getJwtTokenFor($ti->client);

        $deviceUid = $ti->device->uid;
        $resp = $this->callWithResponse('POST', route('channel.subscribe', $ti->channel->uid), [], $jwt, $deviceUid);
        $device = \App\Device::where('uid', $deviceUid)->first(); // created device
        $this->assertEquals($resp->success, true);

        // check if subscription has been stored
        $subscription = Subscription::where('device_id', $device->id)
                ->where('channel_id', $ti->channel->id)
                ->first();

        $this->assertNotNull($subscription);
    }

    /** @test */
    public function it_can_unsubscribe_from_a_channel()
    {
        $ti = $this->mockApiSetup();
        $jwt = $this->getJwtTokenFor($ti->client);

        $ti->device->subscriptions()->attach($ti->channel->id);

        //  attempt to un-subscribe
        $resp = $this->callWithResponse('POST', route('channel.subscribe', $ti->channel->uid), [], $jwt, $ti->device->uid);

        $this->assertEquals($resp->success, true);

        // check if subscription has been stored
        $subscription = Subscription::where('device_id', $ti->device->id)
                ->where('channel_id', $ti->channel->id)
                ->first();

        $this->assertEquals($subscription->subscribed, false);
    }
}
