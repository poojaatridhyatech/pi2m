<?php

namespace Tests\Feature;

use App\User;
use App\Device;
use App\Content;
use App\Opinion;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OpinionTest extends TestCase
{
    use DatabaseTransactions;

     /** @test */
    public function user_can_like_content()
    {
        $ti = $this->mockApiSetup();
        $jwt = $this->getJwtTokenFor($ti->client);

        $payload = [
            'like'  =>  true,
        ];

        $resp = $this->callWithResponse('POST', route('content.opinion', $ti->content->uid), $payload, $jwt, $ti->device->uid);

        $this->assertEquals($resp->success, true);

        // check if opinion has been stored
        $opinion = Opinion::where('device_id', $ti->device->id)
                ->where('content_id', $ti->content->id)
                ->first();

        $this->assertNotNull($opinion);
        $this->assertEquals($opinion->like, $payload['like']);
    }

    /** @test */
    public function user_can_unlike_content()
    {
        $ti = $this->mockApiSetup();
        $jwt = $this->getJwtTokenFor($ti->client);

        $payload = [
            'like'  =>  true,
        ];
        
        $ti->device->opinions()->attach($ti->content->id, ['like' => $payload['like']]);

        $resp = $this->callWithResponse('POST', route('content.opinion', $ti->content->uid), $payload, $jwt, $ti->device->uid);

        $this->assertEquals($resp->success, true);

        // check if opinion has been stored
        $opinion = Opinion::where('device_id', $ti->device->id)
                ->where('content_id', $ti->content->id)
                ->first();

        $this->assertNull($opinion);
    }

    /** @test */
    public function can_dislike_content()
    {
        $ti = $this->mockApiSetup();
        $jwt = $this->getJwtTokenFor($ti->client);

        $payload = [
            'like'  =>  false,
        ];

        $resp = $this->callWithResponse('POST', route('content.opinion', $ti->content->uid), $payload, $jwt, $ti->device->uid);

        $this->assertEquals($resp->success, true);

        // check if opinion has been stored
        $opinion = Opinion::where('device_id', $ti->device->id)
                ->where('content_id', $ti->content->id)
                ->first();

        $this->assertNotNull($opinion);
        $this->assertEquals($opinion->like, $payload['like']);
    }

    /** @test */
    public function can_disunlike_content()
    {
        $ti = $this->mockApiSetup();
        $jwt = $this->getJwtTokenFor($ti->client);

        $payload = [
            'like'  =>  false,
        ];

        $ti->device->opinions()->attach($ti->content->id, ['like' => $payload['like']]);
        
        $resp = $this->callWithResponse('POST', route('content.opinion', $ti->content->uid), $payload, $jwt, $ti->device->uid);

        $this->assertEquals($resp->success, true);

        // check if opinion has been stored
        $opinion = Opinion::where('device_id', $ti->device->id)
                ->where('content_id', $ti->content->id)
                ->first();

        $this->assertNull($opinion);
    }
}