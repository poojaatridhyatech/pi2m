<?php

namespace Tests\Feature;

use App\User;
use App\Device;
use App\Content;
use App\Opinion;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SearchTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_can_search_for_content()
    {
        $ti = $this->mockApiSetup();
        $jwt = $this->getJwtTokenFor($ti->client);

        $payload = [
            'term'  =>  $ti->content->title
        ];

        $resp = $this->callWithResponse('POST', route('content.search'), $payload, $jwt, $ti->device->uid);

        $this->assertEquals(true, true);
        // $this->assertEquals(is_array($resp->data), true);
    }
}
