<?php

namespace Tests\Feature;

use Tests\TestCase;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StatsTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_can_send_a_notification_via_onesignal()
    {
        $ti = $this->mockApiSetup();
        $jwt = $this->getJwtTokenFor($ti->client);

        $payload = [
            'data'  =>  ['some_stats'],
            'channel_id'  =>  $ti->channel->uid,
        ];

        $resp = $this->callWithResponse('POST', route('stats.post', $ti->content->uid), $payload, null, $ti->device->uid);

        $assumeCreated = \App\Stat::all();
        $this->assertNotNull($assumeCreated);
    }
}