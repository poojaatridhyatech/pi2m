<?php

namespace Tests\Feature;

use App\Ad;
use Generate;
use App\View;
use App\User;
use App\Device;
use App\Content;
use App\ViewType;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function ads_that_belong_to_a_video_can_be_returned()
    {
        $ti = $this->mockApiSetup();
        $ad = $this->setupAd($ti->content);

        $jwt = $this->getJwtTokenFor($ti->client);

        $resp = $this->callWithResponse('GET', route('ads.per_video', $ti->content->uid), [], $jwt, $ti->device->uid);

        $this->assertEquals($resp->data[0]->uid, $ad->uid);
    }

    /** @test */
    public function device_can_wish_for_ad()
    {
        $ti = $this->mockApiSetup();
        $ad = $this->setupAd($ti->content);

        $jwt = $this->getJwtTokenFor($ti->client);

        $resp = $this->callWithResponse('POST', route('ads.wish', [
            'uid' => $ti->content->uid,
            'aduid' => $ad->uid,
        ]), [], $jwt, $ti->device->uid);
            
        $this->assertEquals($resp->success, true);

        $this->assertEquals( $ti->device->wishes[0]->uid, $ad->uid );
    }

    /** @test */
    public function it_can_return_device_wishes()
    {
        $ti = $this->mockApiSetup();
        $jwt = $this->getJwtTokenFor($ti->client);

        $ad = $this->setupAd($ti->content);

        // wish for it
        $resp = $this->callWithResponse('POST', route('ads.wish', [
            'uid' => $ti->content->uid,
            'aduid' => $ad->uid,
        ]), [], $jwt, $ti->device->uid);


        $resp = $this->callWithResponse('GET', route('ads.wishes'), [], $jwt, $ti->device->uid);
        
        $this->assertEquals( (count($resp->data) > 0), true);
        $this->assertEquals($ad->devices[0]->uid, $ti->device->uid);
    }

    /**
     * Helper to create an ad
     *
     * @param Content $content
     * @return Ad
     */
    protected function setupAd(Content $content)
    {
        $type = [
            'type'  =>  'banner',
            'postition' =>  'top',
            'skippable' =>  true,
        ];
        // create an Ad
        $ad = Ad::create([
            'uid'   =>  Generate::noDashUid(),
            'title' =>  'ad title',
            'description'   =>  'desc',
            'expires_at'    =>  '0000-00-00 00:00:00',
            'asset_path'    =>  '/test',
            'user_id'       =>  $content->user->id,
            'resource_uri'  =>  '/test',
            'type'          =>  json_encode($type),
        ]);

        //$content->ads()->attach([$ad->id]);
        
        return $ad;
    }
}
