<?php

namespace Tests\Feature;

use App\Category;
use App\Device;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ChannelTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_can_return_all_available_categories_for_client()
    {
        $ti = $this->mockApiSetup();

        // create a category
        $category = Category::create([
            'name'  =>  'sports',
            'description'   =>  'this is a sports collection of channels',
            'user_id'   =>  $ti->user->id,
        ]);

        $jwt = $this->getJwtTokenFor($ti->client);

        $resp = $this->callWithResponse('GET', route('channel.categories'), [], $jwt, $ti->device->uid);

        $this->assertEquals(true, isset($resp->data[0]));
        $this->assertEquals(true, isset($resp->data[0]->name));
        $this->assertEquals(true, isset($resp->data[0]->description));
    }

    /** @test */
    public function can_return_all_channels()
    {
        $ti = $this->mockApiSetup();

        $jwt = $this->getJwtTokenFor($ti->client);

        $resp = $this->callWithResponse('GET', route('channel.all'), [], $jwt, $ti->device->uid);

        $this->assertEquals(true, (count($resp->data) > 0 ));
        $this->assertEquals(true, isset($resp->data[0]->user));
        $this->assertEquals(true, isset($resp->data[0]->description));
        $this->assertEquals(true, isset($resp->data[0]->subscriptions));
        $this->assertEquals(true, isset($resp->data[0]->videos));
        $this->assertEquals(true, isset($resp->data[0]->uid));
        $this->assertEquals(true, isset($resp->data[0]->subscribed));
        $this->assertEquals(true, isset($resp->data[0]->thumbnail));
        $this->assertEquals(true, isset($resp->data[0]->categories));
    }

    /** @test */
    public function it_can_return_a_channel()
    {
        $ti = $this->mockApiSetup();
        
        // subscribe the device to the channel
        $ti->device->subscriptions()->attach([$ti->channel->id]);

        $jwt = $this->getJwtTokenFor($ti->client);  

        $resp = $this->callWithResponse('GET', route('channel.single', $ti->channel->uid), [], $jwt, $ti->device->uid);
        
        $this->assertEquals(true, isset($resp->data->user));
        $this->assertEquals(true, isset($resp->data->subscriptions));
        $this->assertEquals(true, isset($resp->data->description));
        $this->assertEquals(true, isset($resp->data->videos));
        $this->assertEquals(true, isset($resp->data->uid));
        $this->assertEquals(true, isset($resp->data->subscribed));
        $this->assertEquals(true, isset($resp->data->thumbnail));
        $this->assertEquals(true, isset($resp->data->categories));

    }
}
