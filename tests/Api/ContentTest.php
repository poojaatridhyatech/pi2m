<?php

namespace Tests\Feature;

use App\Ad;
use Generate;
use App\Push;
use App\View;
use App\User;
use App\Device;
use App\Content;
use App\Category;
use App\ViewType;
use Tests\TestCase;
use App\Subscription;
use App\Events\PushContent;
use Laravel\Passport\Passport;
use App\Routines\TrendingContent;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ContentTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function client_can_request_to_start_a_push_for_contents()
    {
        $ti = $this->mockApiSetup();
        $jwt = $this->getJwtTokenFor($ti->client);

        $uids = [];
        $uids = [$ti->content->uid];

        $payload = [
            'uids'   =>  $uids,
        ];

        $ti->device->subscriptions()->attach($ti->channel->id, ['subscribed' => true]);

        $resp = $this->callWithResponse('POST', route('content.request'), $payload, $jwt, $ti->device->uid);

        $this->assertEquals($resp->success, true);
        $this->assertEquals($resp->data->queued, count($uids));
    }

    /** @test */
    public function it_can_push_trending_videos()
    {
        $ti = $this->mockApiSetup();

        // add a like
        $ti->device->opinions()->attach($ti->content->id, [
            'like'  =>  true
        ]);

        $jwt = $this->getJwtTokenFor($ti->client);

        $payload = [
            'qty'   =>  1
        ];

        $resp = $this->callWithResponse('POST', route('content.download_trending'), $payload, $jwt, $ti->device->uid);

        $this->assertEquals($resp->success, true);
        $this->assertEquals($resp->data->queued, $payload['qty']);
    }

    /** @test */
    public function it_can_return_a_list_of_trending_videos()
    {
        $ti = $this->mockApiSetup();

        // add a like
        $ti->device->opinions()->attach($ti->content->id, [
            'like'  =>  true
        ]);

        $jwt = $this->getJwtTokenFor($ti->client);

        $resp = $this->callWithResponse('GET', route('content.trending'), [], $jwt, $ti->device->uid);

        foreach ($resp->data as $res) {
            $this->assertEquals($res->uid, $ti->content->uid);
        }

    }

    /** @test */
    public function it_can_create_a_list_of_trending_videos()
    {
        $ti = $this->mockApiSetup();

        // add a like
        $ti->device->opinions()->attach($ti->content->id, [
            'like'  =>  true
        ]);


        $routine = new TrendingContent($ti->client, $ti->device);
        $trending = $routine->get();

        foreach ($trending as $t) {
            $this->assertEquals($t->id, $ti->content->id);
        }
    }

    /** @test */
    public function it_can_push_recommended_videos_to_a_device()
    {
        $ti = $this->mockApiSetup();

        // add a like
        $ti->device->opinions()->attach($ti->content->id, [
            'like'  =>  true
        ]);

        // content that should be recommended
        $toBeRecommended = Content::create([
            'uid'   =>  Generate::noDashUid(),
            'title' =>  'recommend',
            'description' =>  'Lorem ipsum dolor',
            'resource_uri'  =>  'test',
            'user_id'   =>  $ti->user->id,
        ]);

        $toBeRecommended->channels()->attach([$ti->channel->id]);

        // add a category
        $category = Category::create([
            'name'  =>  'sports',
            'description'   =>  'sprts',
            'user_id'   =>  $ti->user->id,
            'uid'   =>  Generate::noDashUid(),
            'asset_path'    =>  '/asd',
        ]);
        $ti->content->categories()->attach($category->id);
        $toBeRecommended->categories()->attach($category->id);

        // add a view
        $view = View::create([
            'device_id'     =>  $ti->device->id,
            'content_id'    =>  $ti->content->id,
            'watched'       =>  null,
            'view_type_id'  =>  ViewType::finished()->id,
        ]);

        $ad = Ad::create([
            'uid'   =>  Generate::noDashUid(),
            'title' =>  'ad title',
            'description'   =>  'desc',
            'expires_at'    =>  '0000-00-00 00:00:00',
            'asset_path'    =>  '/test',
            'user_id'       =>  $ti->user->id,
            'resource_uri'  =>  '/test',
        ]);

        $toBeRecommended->ads()->attach([$ad->id]);

        $jwt = $this->getJwtTokenFor($ti->client);
        
        $payload = [
            'qty'   =>  1
        ];

        $resp = $this->callWithResponse('POST', route('content.download_recommended'), $payload, $jwt, $ti->device->uid);

        $this->assertEquals($resp->success, true);
        $this->assertEquals($resp->data->queued, $payload['qty']);
    }

    /** @test */
    public function it_can_return_recommended_videos()
    {
        $ti = $this->mockApiSetup();

        // add a like
        $ti->device->opinions()->attach($ti->content->id, [
            'like'  =>  true
        ]);

        // content that should be recommended
        $toBeRecommended = Content::create([
            'uid'   =>  Generate::noDashUid(),
            'title' =>  'recommend',
            'description' =>  'Lorem ipsum dolor',
            'resource_uri'  =>  'test',
            'user_id'   =>  $ti->user->id,
        ]);

        $toBeRecommended->channels()->attach([$ti->channel->id]);

        // add a category
        $category = Category::create([
            'name'  =>  'sports',
            'description'   =>  'sprts',
            'user_id'   =>  $ti->user->id,
            'uid'   =>  Generate::noDashUid(),
            'asset_path'    =>  '/asd',
        ]);
        $ti->content->categories()->attach($category->id);
        $toBeRecommended->categories()->attach($category->id);

        // add a view
        $view = View::create([
            'device_id'     =>  $ti->device->id,
            'content_id'    =>  $ti->content->id,
            'watched'       =>  null,
            'view_type_id'  =>  ViewType::finished()->id,
        ]);

        $jwt = $this->getJwtTokenFor($ti->client);
        
        $resp = $this->callWithResponse('GET', route('content.recommended'), [], $jwt, $ti->device->uid);

        foreach ($resp->data as $res) {
            $this->assertEquals($res->uid, $toBeRecommended->uid);
        }

    }

    /** @test */
    public function it_will_return_information_about_multiple_videos_at_once()
    {
        $device = Device::create([
            'uid'   =>  '50ecded6-191c-4f1d-889b-0999f9c90935'
        ]);
        $user = User::first();
        $client = \App\Client::create([
            'uid'   =>  \Generate::noDashUid(),
            'name'   =>  'tst client',
            'description'   =>  'description',
            'user_id'   =>  $user->id,
            'secret'    =>  'my_secret',
        ]);
        $jwt = $this->getJwtTokenFor($client);

        $user = User::first();
        $content = $this->mockContent($user);
        $content->channels()->attach([\App\Channel::first()->id]);

        $payload = [
            'uids'  =>  [
                $content->uid
            ]
        ];

        $resp = $this->callWithResponse('POST', route('content.about_multiple'), $payload, $jwt, $device->uid);
        
        $this->assertEquals(true, isset($resp->data));
        $this->assertEquals(true, isset($resp->data[0]->expires_at));
        $this->assertEquals(true, isset($resp->data[0]->likes));
        $this->assertEquals(true, isset($resp->data[0]->dislikes));
        $this->assertEquals(true, isset($resp->data[0]->categories));
        $this->assertEquals(true, isset($resp->data[0]->views));
        $this->assertEquals(true, isset($resp->data[0]->bookmarked));
        $this->assertEquals(true, isset($resp->data[0]->duration));
        $this->assertEquals(true, isset($resp->data[0]->watch_later));
        $this->assertEquals(true, isset($resp->data[0]->thumbnail));
        $this->assertEquals(true, is_array($resp->data[0]->channels));
    }

    /** @test */
    public function it_will_return_content_information()
    {
        $device = Device::create([
            'uid'   =>  '50ecded6-191c-4f1d-889b-0999f9c90935'
        ]);
        $user = User::first();
        $client = \App\Client::create([
            'uid'   =>  \Generate::noDashUid(),
            'name'   =>  'tst client',
            'description'   =>  'description',
            'user_id'   =>  $user->id,
            'secret'    =>  'my_secret',
        ]);
        $jwt = $this->getJwtTokenFor($client);

        $user = User::first();
        $content = $this->mockContent($user);
        $content->channels()->attach([\App\Channel::first()->id]);

        $resp = $this->callWithResponse('GET', route('content.about', $content->uid), [], $jwt, $device->uid);

        $this->assertEquals(true, isset($resp->data));
        $this->assertEquals(true, isset($resp->data->expires_at));
        $this->assertEquals(true, isset($resp->data->likes));
        $this->assertEquals(true, isset($resp->data->dislikes));
        $this->assertEquals(true, isset($resp->data->categories));
        $this->assertEquals(true, isset($resp->data->views));
        $this->assertEquals(true, isset($resp->data->bookmarked));
        $this->assertEquals(true, isset($resp->data->duration));
        $this->assertEquals(true, isset($resp->data->watch_later));
        $this->assertEquals(true, isset($resp->data->thumbnail));
        $this->assertEquals(true, is_array($resp->data->channels));
    }

    /** @test */
    public function it_will_store_push_information()
    {
        $deviceUid = '50ecded6-191c-4f1d-889b-0999f9c90935';
        $oneSignalDeviceId = '50ecded6-191c-4f1d-889b-0999f9c90935';
        $user = User::first();
        $content = $this->mockContent($user);

        $device = Device::create([
            'uid'   =>  '50ecded6-191c-4f1d-889b-0999f9c90935'
        ]);

        $client = \App\Client::create([
            'uid'   =>  \Generate::noDashUid(),
            'name'   =>  'tst client',
            'description'   =>  'description',
            'user_id'   =>  $user->id,
            'secret'    =>  'my_secret',
        ]);
        $jwt = $this->getJwtTokenFor($client);

        $payload = [
            'send_to'   =>  $oneSignalDeviceId
        ];

        $p = Push::create([
            'device_id' =>  $device->id,
            'content_id'    =>  $content->id
        ]);

        $resp = $this->callWithResponse('POST', route('content.downloaded', $content->uid), $payload, $jwt, $deviceUid);
        $this->assertEquals($resp->success, true);

        // make sure that the push has been stored
        $device = \App\Device::where('uid', $deviceUid)->first(); // created device

        $push = \App\Push::where('content_id', $content->id)->where('device_id', $device->id)->first();

        $this->assertNotNull($push);
        $this->assertEquals($push->pushed, true);
    }

    /** @test */
    public function it_will_store_info_if_content_has_been_deleted()
    {
        $deviceUid = '50ecded6-191c-4f1d-889b-0999f9c90935';
        $user = User::first();
        $content = $this->mockContent($user);

        $device = Device::create([
            'uid'   =>  '50ecded6-191c-4f1d-889b-0999f9c90935'
        ]);

        $client = \App\Client::create([
            'uid'   =>  \Generate::noDashUid(),
            'name'   =>  'tst client',
            'description'   =>  'description',
            'user_id'   =>  $user->id,
            'secret'    =>  'my_secret',
        ]);
        $jwt = $this->getJwtTokenFor($client);

        $p = Push::create([
            'device_id' =>  $device->id,
            'content_id'    =>  $content->id
        ]);

        // download content first
        $resp = $this->callWithResponse('POST', route('content.downloaded', $content->uid), [
            'send_to'   =>  $deviceUid
        ], $jwt, $deviceUid);

        $resp = $this->callWithResponse('POST', route('content.deleted', $content->uid), [], $jwt, $deviceUid);

        $this->assertEquals($resp->success, true);

        // make sure that the push has been stored
        $device = \App\Device::where('uid', $deviceUid)->first(); // created device

        $push = \App\Push::where('content_id', $content->id)->where('device_id', $device->id)->first();

        $this->assertEquals(true, $push->deleted);
    }
}