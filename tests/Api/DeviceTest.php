<?php

namespace Tests\Feature;

use App\Device;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DeviceTest extends TestCase
{
    use DatabaseTransactions;
    
    /** @test */
    public function it_can_save_device_settings()
    {
        $ti = $this->mockApiSetup();

        $payload = [
            'settings'  =>  [
                'frequency'  =>  5
            ]
        ];

        $jwt = $this->getJwtTokenFor($ti->client);

        $resp = $this->callWithResponse('POST', route('device.settings'), $payload, $jwt, $ti->device->uid);
        $this->assertEquals($resp->success, true);

        $dev = Device::find($ti->device->id);
        $this->assertEquals($dev->settings, json_encode($payload['settings']));
    }
}
