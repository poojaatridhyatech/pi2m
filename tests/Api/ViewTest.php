<?php

namespace Tests\Feature;

use App\View;
use App\User;
use App\Device;
use App\Content;
use App\ViewType;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ViewTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function video_can_be_marked_as_finished_watching()
    {
        $ti = $this->mockApiSetup();
        $jwt = $this->getJwtTokenFor($ti->client);

        $viewType = ViewType::finished();

        $payload = [
            'type'  =>  $viewType->name
        ];

        $resp = $this->callWithResponse('POST', route('content.post_view', $ti->content->uid), $payload, $jwt, $ti->device->uid);

        $this->assertEquals($resp->success, true);

        // check if view has been stored
        $view = View::where('device_id', $ti->device->id)
                ->where('content_id', $ti->content->id)
                ->where('view_type_id', $viewType->id)
                ->first();

        $this->assertNotNull($view);

    }

    /** @test */
    public function video_can_be_marked_as_watch_later()
    {
        $ti = $this->mockApiSetup();
        $jwt = $this->getJwtTokenFor($ti->client);

        $viewType = ViewType::watchLater();

        $payload = [
            'type'  =>  $viewType->name,
        ];

        $resp = $this->callWithResponse('POST', route('content.post_view', $ti->content->uid), $payload, $jwt, $ti->device->uid);

        $this->assertEquals($resp->success, true);

        // check if view has been stored
        $view = View::where('device_id', $ti->device->id)
                ->where('content_id', $ti->content->id)
                ->where('view_type_id', $viewType->id)
                ->first();

        $this->assertNotNull($view);
    }
}
