<?php

namespace Tests\Feature;

use App\Bookmark;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BookmarkTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_can_bookmark_a_video()
    {
        $ti = $this->mockApiSetup();
        $jwt = $this->getJwtTokenFor($ti->client);

        $resp = $this->callWithResponse('POST', route('content.bookmark', $ti->content->uid), [], $jwt, $ti->device->uid);

        $this->assertEquals($resp->success, true);

        // check if bookmark has been stored
        $bookmark = Bookmark::where('device_id', $ti->device->id)
                ->where('content_id', $ti->content->id)
                ->first();

        $this->assertNotNull($bookmark);
    }

    /** @test */
    public function it_can_unbookmark_a_video()
    {
        $ti = $this->mockApiSetup();

        $jwt = $this->getJwtTokenFor($ti->client);

        // create a bookmark
        $ti->device->bookmarks()->attach($ti->content->id);

        $resp = $this->callWithResponse('POST', route('content.bookmark', $ti->content->uid), [], $jwt, $ti->device->uid);

        $this->assertEquals($resp->success, true);

        // check if bookmark has been stored
        $bookmark = Bookmark::where('device_id', $ti->device->id)
                ->where('content_id', $ti->content->id)
                ->first();

        $this->assertNull($bookmark);
    }
}
