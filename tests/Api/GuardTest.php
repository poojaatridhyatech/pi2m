<?php

namespace Tests\Feature;

use Generate;
use App\User;
use App\Client;
use App\Device;
use App\Channel;
use Tests\TestCase;
use App\Subscription;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GuardTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_will_not_subscribe_to_an_unsubscribed_channel()
    {
        $ti = $this->mockApiSetup();

        $jwt = $this->getJwtTokenFor($ti->client);

        $unsubscribedFromChannel = Channel::create([
            'label' =>  'notsubed',
            'name'  =>  'notsubed',
            'description'   =>  'notsubed',
            'asset_path'    =>  'notsubed',
            'uid'   =>  Generate::noDashUid(),
            'user_id'   =>  $ti->user->id,
        ]);
        $ti->device->subscriptions()->attach($unsubscribedFromChannel, ['subscribed' => false]);

        $ti->client->channels()->attach([$unsubscribedFromChannel->id]);

        $resp = $this->callWithResponse('GET', route('content.about', $ti->content->uid), [], $jwt, $ti->device->uid);

        $subscription = Subscription::where('channel_id', $unsubscribedFromChannel->id)->where('device_id', $ti->device->id)->first();

        $this->assertEquals(false, $subscription->subscribed);
    }

    /** @test */
    public function it_will_subscribe_a_user_to_a_channel_when_its_the_first_call()
    {
        $ti = $this->mockApiSetup();

        $jwt = $this->getJwtTokenFor($ti->client);

        $resp = $this->callWithResponse('GET', route('content.about', $ti->content->uid), [], $jwt, $ti->device->uid);

        $subscriptions = Subscription::all();
        $this->assertEquals(1, count($subscriptions));
    }

    /** @test */
    public function it_will_subscribe_to_a_never_subscribed_channel()
    {
        $ti = $this->mockApiSetup();

        $jwt = $this->getJwtTokenFor($ti->client);

        $notSubscribedToChannel = Channel::create([
            'label' =>  'notsubed',
            'name'  =>  'notsubed',
            'description'   =>  'notsubed',
            'asset_path'    =>  'notsubed',
            'uid'   =>  Generate::noDashUid(),
            'user_id'   =>  $ti->user->id,
        ]);
        $ti->device->subscriptions()->attach($ti->channel->id);

        $ti->client->channels()->attach([$notSubscribedToChannel->id]);

        $resp = $this->callWithResponse('GET', route('content.about', $ti->content->uid), [], $jwt, $ti->device->uid);

        $subscriptions = Subscription::all();

        $this->assertEquals(2, count($subscriptions));
    }

    /** @test */
    public function it_will_throw_an_error_if_the_jwt_token_is_not_valid()
    {
        $content = $this->mockContent(User::first());
        $device = Device::first();
        
        $jwt = 'Bearer fake_token';

        $resp = $this->callWithResponse('GET', route('content.about', $content->uid), [], $jwt, $device->uid);

        $this->assertEquals($resp->error, trans('gate.not_valid_jwt'));
    }

    /** @test */
    public function it_will_throw_an_error_if_the_auth_header_is_not_sent()
    {
        $content = $this->mockContent(User::first());
        $device = Device::first();

        $jwt = null;

        $resp = $this->callWithResponse('GET', route('content.about', $content->uid), [], $jwt, $device->uid);
        $this->assertEquals($resp->error, trans('gate.no_auth_header_found'));
    }

    /** @test */
    public function it_willhandle_onesignal_uid_appropriately()
    {
        $ti = $this->mockApiSetup(User::first());
        
        $device = Device::create([
            'uid'   =>  Generate::uid(),
            'onesignal_uid'    =>  Generate::uid(),
        ]);

        $device->onesignal_uid = Device::first()->onesignal_uid;

        $client = Client::create([
            'uid'   =>  Generate::noDashUid(),
            'name'   =>  'test client',
            'description'   =>  'description',
            'user_id'   =>  User::first()->id,
            'secret'    =>  'clients_secret_____' . rand(1900,20000),
            'onesignal_app_id'  =>  'eed077b2-fec1-49ed-aae1-8149855ae434---',
            'onesignal_api_key' =>  'YTlhOTU5MzctMzQyNy00NjcyLWIwNmYtYmRjMjBlYWRmZTU5----',
        ]);

        $ti->content->channels()->attach([$ti->channel->id]);

        $jwt = $this->getJwtTokenFor($client);

        $resp = $this->callWithResponse('GET', route('content.about', $ti->content->uid), [], $jwt, $device->uid);

        $this->assertEquals(true, isset($resp->data) );
    }
}
