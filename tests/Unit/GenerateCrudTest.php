<?php

namespace Tests\Unit;

use Generate;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GenerateCrudTest extends TestCase
{
    /** @test */
    public function it_can_create_a_create_instance()
    {
        $name = 'newone';
        $g = Generate::crudInstance($name, 'c');

        $this->assertEquals(true, file_exists(app_path() . '/Routines/Crud/Create/' . ucfirst(strtolower($name)) . '.php'));

        unlink(app_path() . '/Routines/Crud/Create/' . ucfirst(strtolower($name)) . '.php');
    }

    /** @test */
    public function it_can_create_a_update_instance()
    {
        $name = 'newone';
        $g = Generate::crudInstance($name, 'u');

        $this->assertEquals(true, file_exists(app_path() . '/Routines/Crud/Update/' . ucfirst(strtolower($name)) . '.php'));

        unlink(app_path() . '/Routines/Crud/Update/' . ucfirst(strtolower($name)) . '.php');
    }

    /** @test */
    public function it_can_create_a_delete_instance()
    {
        $name = 'newone';
        $g = Generate::crudInstance($name, 'd');

        $this->assertEquals(true, file_exists(app_path() . '/Routines/Crud/Delete/' . ucfirst(strtolower($name)) . '.php'));

        unlink(app_path() . '/Routines/Crud/Delete/' . ucfirst(strtolower($name)) . '.php');
    }
}
