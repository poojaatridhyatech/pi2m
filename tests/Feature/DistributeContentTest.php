<?php

namespace Tests\Feature;

use App\Ad;
use Generate;
use App\Push;
use App\User;
use App\Device;
use App\Content;
use App\Bookmark;
use Carbon\Carbon;
use Tests\TestCase;
use App\Routines\PushContent;
use App\Routines\SchedulePush;
use App\Events\PushContent as PushContentEvent;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DistributeContentTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_can_monitor_the_distribution()
    {
        $ti = $this->mockApiSetup();

        // $type = [
        //     'type'  =>  'banner',
        //     'postition' =>  'top',
        //     'skippable' =>  true,
        // ];
        // // create an Ad
        // $ad = Ad::create([
        //     'uid'   =>  Generate::noDashUid(),
        //     'title' =>  'ad title',
        //     'description'   =>  'desc',
        //     'expires_at'    =>  '0000-00-00 00:00:00',
        //     'asset_path'    =>  '/test',
        //     'user_id'       =>  $ti->content->user->id,
        //     'resource_uri'  =>  '/test',
        //     'type'          =>  json_encode($type),
        // ]);

        // $ti->content->ads()->attach([$ad->id]);


        $routine = new \App\Routines\Monitor\PushContentCron;
        
        $routine->pushImmediately();
    }

    /** @test */
    public function content_can_be_distributed_immediately()
    {
        $ti = $this->mockApiSetup();

        // $when = Carbon::now()->addDay();
        $when = null;

        // 1. Schedule a push with  at = null, so it's sent immediately.
        $routine = new SchedulePush($ti->content, $ti->device, 'asdasd send');
        $routine->at($when);

        $unscheduledPush = $routine->getPush();

        $this->assertEquals($unscheduledPush->push_at, $when);

        // 2. Execute the mechanics running cronjob that will push the videos.
        
        event(new PushContentEvent($ti->content, $ti->device, $unscheduledPush));

    }

    /** @test */
    public function it_will_not_send_a_pushes_that_have_already_been_executed()
    {
        $ti = $this->mockApiSetup();

        $notToPush = Push::create([
            'device_id'     =>  $ti->device->id,
            'content_id'    =>  $ti->content->id,
            'pushed'        =>  true,   // set to true, which means it's been executed
            'push_at'       =>  '2010-01-01 00:00:01'
        ]);

        // $when = Carbon::now()->addDay();
        $when = null;

        $routine = new SchedulePush($ti->content, $ti->device, '$Title send');
        $routine->at($when);

        // 2. Execute the mechanics running cronjob that will push the videos.
        $push = new Push;
        $scheduled = $push->scheduled();
        
        $this->assertEquals( count($scheduled), 0);
    }
}
