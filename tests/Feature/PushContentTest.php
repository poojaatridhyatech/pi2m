<?php

namespace Tests\Feature;

use Generate;
use App\Ad;
use App\Push;
use App\User;
use App\Device;
use App\Content;
use App\Bookmark;
use Carbon\Carbon;
use Tests\TestCase;
use App\Routines\PushContent;
use App\Events\PushContent as PushContentEvent;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PushContentTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_can_recall_push()
    {
        $ti = $this->mockApiSetup();
        $ti->device->subscriptions()->attach($ti->channel->id, ['subscribed' => true]);

        $user = $this->be($ti->user);

        $payload = [
            'content_uid'       =>  $ti->content->uid,
            'onesignal_uid'     =>  '',
            'channels'          =>  [$ti->content->channels[0]->uid],
            'expires_at_date'   =>  '2020-02-24T12:19:01.862Z',
            'notification'      =>  'required',
        ];

        $resp = $this->call('POST', route('content.push_to_devices'), $payload);
        $resp = json_decode($resp->getContent());

        $this->assertEquals($resp->queued, 1);

        $push = Push::where('device_id', $ti->device->id)->where('content_id', $ti->content->id)->latest()->first();

        $recallPayload = [
            'content_uid'       =>  $push->content->uid,
            'channel_uid'       =>  $push->channel->uid,
        ];

        $resp = $this->call('POST', route('content.recall_push'), $recallPayload);
        $resp = json_decode($resp->getContent());

        // check push status
        $push = Push::where('device_id', $ti->device->id)->where('content_id', $ti->content->id)->latest()->first();
        $this->assertEquals($push->deleted, 1);
    }

    /** @test */
    public function it_can_schedule_push_to_channels()
    {
        $ti = $this->mockApiSetup();

        $ti->device->subscriptions()->attach($ti->channel->id, ['subscribed' => true]);

        $user = $this->be($ti->user);

        $payload = [
            'content_uid'       =>  $ti->content->uid,
            'onesignal_uid'     =>  '',
            'channels'     =>  [$ti->content->channels[0]->uid],
            'expires_at_date'   =>  '2020-02-24T12:19:01.862Z',
            'notification'      =>  'required',
            'push_at'           =>  \Carbon\Carbon::now()->subDays(1),
        ];

        $resp = $this->call('POST', route('content.push_to_devices'), $payload);
        $resp = json_decode($resp->getContent());

        $this->assertEquals($resp->queued, 1);

        // check if the push is there
        $push = Push::where('device_id', $ti->device->id)->where('content_id', $ti->content->id)->latest()->first();
        $content = Content::find($ti->content->id);

        $this->assertEquals(false, $push->pushed);
        $this->assertEquals('2020-02-24 12:19:01', $content->expires_at);
        $this->assertEquals($payload['push_at'], $push->push_at);


        // check if the cron logic is working as expected
        // find the scheduled pushes
        $scheduled = Push::where('pushed', false)->where('push_at', '<', date('Y-m-d H:i:s'))->get();
        foreach ($scheduled as $push) {
            event( new PushContentEvent($push->content, $push->device, $push) );
        }

        // make sure all are pushed
        $scheduled = Push::where('pushed', false)->where('push_at', '<', date('Y-m-d H:i:s'))->get();
        $this->assertEquals(0, count($scheduled));
    }

    /** @test */
    public function it_can_push_from_cms_to_channels()
    {
        $ti = $this->mockApiSetup();

        $ti->device->subscriptions()->attach($ti->channel->id, ['subscribed' => true]);

        $user = $this->be($ti->user);

        $payload = [
            'content_uid'       =>  $ti->content->uid,
            'onesignal_uid'     =>  '',
            'channels'     =>  [$ti->content->channels[0]->uid],
            'expires_at_date'   =>  '2020-02-24T12:19:01.862Z',
            'notification'      =>  'required',
        ];

        $resp = $this->call('POST', route('content.push_to_devices'), $payload);
        $resp = json_decode($resp->getContent());


        $this->assertEquals($resp->queued, 1);

        // check if the push is there
        $push = Push::where('device_id', $ti->device->id)->where('content_id', $ti->content->id)->first();
        $content = Content::find($ti->content->id);

        $this->assertEquals(true, $push->pushed);
        $this->assertEquals(null, $push->push_at);
        $this->assertEquals('2020-02-24 12:19:01', $content->expires_at);
        $this->assertEquals(null, $content->push_at);

        // make sure nothing is scheduled
        $scheduled = Push::where('pushed', false)->where('push_at', '<', date('Y-m-d H:i:s'))->get();
        $this->assertEquals(0, count($scheduled));
    }

    /** @test */
    public function it_can_push_from_cms_to_device()
    {
        $ti = $this->mockApiSetup();
        $user = $this->be($ti->user);

        $payload = [
            'content_uid'       =>  $ti->content->uid,
            'onesignal_uid'     =>  $ti->device->onesignal_uid,
            'expires_at_date'   =>  '2020-02-24T12:19:01.862Z',
            'notification'      =>  'required',
        ];

        $resp = $this->call('POST', route('content.push_to_devices'), $payload);
        $resp = json_decode( $resp->getContent() );

        $this->assertEquals($resp->queued, 1);

        // check if the push is there
        $push = Push::where('device_id', $ti->device->id)->where('content_id', $ti->content->id)->first();
        $content = Content::find($ti->content->id);

        $this->assertEquals(true, $push->pushed);
        $this->assertEquals(null, $push->push_at);
        $this->assertEquals('2020-02-24 12:19:01', $content->expires_at);
        
    }

    /** @test */
    public function it_can_push_with_expiry_time()
    {
        $ti = $this->mockApiSetup();
        $user = $this->be($ti->user);

        $payload = [
            'content_uid'       =>  $ti->content->uid,
            'onesignal_uid'     =>  $ti->device->onesignal_uid,
            // 'expires_at_date'   =>  '2020-02-24T12:19:01.862Z',
            'expiry_time'   =>  2,
            'expiry_time_value' =>  'minutes',
            'notification'      =>  'required',
        ];

        $resp = $this->call('POST', route('content.push_to_devices'), $payload);
        $resp = json_decode( $resp->getContent() );

        $this->assertEquals($resp->queued, 1);

        // check if the push is there
        $push = Push::where('device_id', $ti->device->id)->where('content_id', $ti->content->id)->first();
        $content = Content::find($ti->content->id);

        $this->assertEquals(true, $push->pushed);
        $this->assertEquals(null, $push->push_at);

        $now = new Carbon();
        $expireat = $now->addMinutes($payload['expiry_time']);
        
        $this->assertEquals($expireat, $content->expires_at);
    }
}
