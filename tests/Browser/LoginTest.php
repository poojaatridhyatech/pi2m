<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $pass = 'letmein';
        $user = User::where('email', 'imants@eskimo.uk.com')->first();
        // update the password just in case
        $user->password = bcrypt($pass);
        $user->save();

        $this->browse(function (Browser $browser) use ($user, $pass) {
            $browser->visit('/');
            $browser->click('#loginBtn')->assertSee('E-Mail Address');

            // fill in the email and password field
            $browser->value('#email', $user->email);
            $browser->value('#password', $pass);

            $browser->click('#submitBtn')->assertRouteIs('content.page')->assertSee('Videos');

        });
    }
}
