<?php

use App\Role;

Route::get('wewewe', function() {
    $device = \App\Device::where('onesignal_uid', '91b0a8a8-c799-4c6c-a248-07ac77b68863')->first();
    // foreach( $device->clients as $client) {
    //     $client->devices()->detach($device->id);
    // }

    dd($device->clients()->sync([17]));
});


Route::get('/', [
    'uses'  =>  'PublicController@index',
    'as'    =>  'public.index'
]);

Route::get('/activate', [
    'uses'  =>  'UserController@getActivate',
    'as'    =>  'activate'
]);
Route::post('/activate', [
    'uses'  =>  'UserController@postActivate',
    'as'    =>  'post.activate'
]);

Route::post('/reset', [
    'uses'  =>  'UserController@postReset',
    'as'    =>  'post.reset'
]);


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->middleware('auth', \App\Http\Middleware\CheckDeveloper::class);

Route::post('request-demo', [
    'as'  =>  'request_demo',
    'uses'  =>  'MailController@requestDemo',
]);


Route::group(['middleware' => 'auth', 'prefix' => 'me'], function() {

    Route::group(['prefix' => 'dashboard'], function() {

        Route::get('summary', 'HomeController@dashboardContentSummary')
            ->name('dashboard.content.summary');

        Route::get('videos', 'HomeController@dashboardContentVideos')
            ->name('dashboard.content.videos');

        Route::post('content-charts-data', 'HomeController@contentChartsData')
            ->name('dashboard.charts-data.videos');

        Route::post('ad-charts-data', 'HomeController@adChartsData')
            ->name('dashboard.charts-data.ads');

        Route::get('ad_summary', 'HomeController@dashboardAdSummary')
            ->name('dashboard.ad.summary');

        Route::get('reindex', 'HomeController@reindexPage')
            ->name('dashboard.reindex-page');

        Route::post('reindex', 'HomeController@reindexRun')
            ->name('dashboard.reindex');
    });

    Route::post('notifications', 'HomeController@notifications')
        ->middleware('role:' . Role::ROLE_ADMIN)
        ->name('notifications');

    Route::group(['prefix' => 'ads'], function() {

        Route::get('/', 'AdController@index')->name('ads.index');

        Route::get('list', 'AdController@listing')->name('ads.list');

        Route::get('create', 'AdController@create')->name('ads.create');

        Route::post('/', 'AdController@store')->name('ads.store');

        Route::get('{ad}', 'AdController@show')->name('ads.show');

        Route::get('{ad}/json', 'AdController@json')->name('ads.json');

        Route::get('{ad}/categories', 'AdController@categories')->name('ads.categories');

        Route::get('{ad}/edit', 'AdController@edit')->name('ads.edit');

        Route::put('{ad}', 'AdController@update')->name('ads.update');

        Route::delete('{ad}', 'AdController@destroy')->name('ads.destroy');

        Route::post('destroy-multi', 'AdController@destroyMulti')->name('ads.destroy-multi');

        Route::post('search', 'AdController@postSearch')
            ->name('ads.search');

        Route::get('info/{id}', 'AdController@getInfo')
            ->name('ads.info');
    });

    Route::group(['prefix' => 'categories'], function() {
        Route::get('/list', [
            'uses'  =>  'CategoryController@getList',
            'as'    =>  'categories.list'
        ]);
        Route::get('/', [
            'uses'  =>  'CategoryController@getAll',
            'as'    =>  'categories.page'
        ]);
        Route::get('/{uid?}/edit', [
            'uses'  =>  'CategoryController@getEdit',
            'as'    =>  'categories.edit_page'
        ]);
        Route::post('/{uid?}/edit', [
            'uses'  =>  'CategoryController@post',
            'as'    =>  'categories.edit'
        ]);
        Route::post('/', [
            'uses'  =>  'CategoryController@store',
            'as'    =>  'categories.store'
        ]);
        Route::post('/delete', [
           'uses'  =>  'CategoryController@delete',
           'as'    =>  'categories.delete'
        ]);
        Route::post('/dissociate/{cat}/{type}', [
            'uses'  =>  'CategoryController@dissociate',
            'as'    =>  'categories.dissociate'
         ]);
        Route::get('/{uid}/videos', [
            'uses'  =>  'CategoryController@viewVideos',
            'as'    =>  'categories.videos'
        ]);
        Route::get('/{uid}/ads', [
            'uses'  =>  'CategoryController@viewAds',
            'as'    =>  'categories.ads'
        ]);
        Route::post('/search', [
            'uses'  =>  'CategoryController@postSearch',
            'as'    =>  'categories.search'
        ]);
        Route::get('/info/{id}', [
            'uses'  =>  'CategoryController@getInfo',
            'as'    =>  'categories.info'
        ]);
    });
    Route::group(['prefix' => 'videos'], function() {
        Route::post('search', [
            'uses'  =>  'ContentController@postSearchByUid',
            'as'    =>  'content.search'
        ]);
        Route::post('search2', [
            'uses'  =>  'ContentController@postSearch2',
            'as'    =>  'content.search2'
        ]);
        Route::get('/', [
            'uses'  =>  'ContentController@getAll',
            'as'    =>  'content.page'
        ]);
        Route::get('/search', [
            'uses'  =>  'ContentController@getSearch',
            'as'    =>  'content.searched'
        ]);
        Route::post('/push', [
            'uses'  =>  'PushController@postPushToDevices',
            'as'    =>  'content.push_to_devices'
        ]);
        Route::post('/recall', [
            'uses'  =>  'PushController@postRecallPush',
            'as'    =>  'content.recall_push'
        ]);
        Route::get('/{uid?}', [
            'uses'  =>  'ContentController@getEdit',
            'as'    =>  'content.edit_page'
        ]);
        Route::post('/remove', [
            'uses'  =>  'ContentController@removeContent',
            'as'    =>  'content.remove'
        ]);
        Route::post('/{uid?}', [
            'uses'  =>  'ContentController@postContent',
            'as'    =>  'content.upload'
        ]);
        Route::get('/{uid?}/push', [
            'uses'  =>  'PushController@getPushContent',
            'as'    =>  'content.manage_push'
        ]);
        Route::get('/{uid}/push-to-channel', [
            'uses'  =>  'ContentController@getPushToChannel',
            'as'    =>  'content.push_to_channel'
        ]);
        Route::post('/{uid}/push', [
            'uses'  =>  'PushController@postPushContent',
            'as'    =>  'content.push'
        ]);
        Route::get('/{uid}/download', [
            'uses'  =>  'ContentController@downloadSingle',
            'as'    =>  'content.download'
        ]);
        Route::get('/info/{id}', [
            'uses'  =>  'ContentController@getInfo',
            'as'    =>  'content.info'
        ]);

    });

    Route::group(['prefix' => 'users'], function() {

        Route::get('/', 'UserController@index')
            ->name('users.index');

        Route::get('list', 'UserController@listing')
            ->name('users.list');

        Route::get('create', 'UserController@create')
            ->name('users.create');

        Route::post('/', 'UserController@store')
            ->name('users.store');

        //Route::get('{user}', 'UserController@show')->name('users.show');

        Route::get('{user}/json', 'UserController@user')
            ->name('users.user');

        Route::get('{user}/edit', 'UserController@edit')
            ->name('users.edit');

        Route::put('{user}', 'UserController@update')
            ->name('users.update');

        Route::delete('{user}', 'UserController@destroy')
            ->name('users.destroy');

        Route::post('destroy-multi', 'UserController@destroyMulti')
            ->name('users.destroy-multi');

        Route::post('change-password', 'UserController@postChangePassword')
            ->name('users.change_password');

        Route::post('search', 'UserController@postSearch')
            ->name('users.search');

        Route::get('info/{id}', 'UserController@getUserInfo')
            ->name('users.info');
    });

    Route::group(['prefix' => 'channels'], function() {
        Route::get('/', [
            'uses'  =>  'ChannelController@getAll',
            'as'    =>  'channels.page'
        ]);
        Route::get('/{uid?}/edit', [
            'uses'  =>  'ChannelController@getEdit',
            'as'    =>  'channels.edit_page'
        ]);
        Route::post('/{uid?}/edit', [
            'uses'  =>  'ChannelController@post',
            'as'    =>  'channels.edit'
        ]);
        Route::post('/delete', [
            'uses'  =>  'ChannelController@delete',
            'as'    =>  'channels.delete'
        ]);
        Route::post('/dissociate/{cat}/{type}', [
            'uses'  =>  'ChannelController@dissociate',
            'as'    =>  'channels.dissociate'
         ]);
        Route::get('/{uid}/contents', [
            'uses'  =>  'ChannelController@viewVideos', 
            'as'    =>  'channels.contents'
        ]);
        Route::get('/{uid}/apps', [
            'uses'  =>  'ChannelController@viewApps', 
            'as'    =>  'channels.apps'
        ]);

        Route::post('/search', [
            'uses'  =>  'ChannelController@postSearch',
            'as'    =>  'channels.search'
        ]);
        Route::get('/info/{id}', [
            'uses'  =>  'ChannelController@getInfo',
            'as'    =>  'channels.info'
        ]);

    });

    Route::group(['prefix' => 'clients'], function() {
        Route::get('/', [
            'uses'  =>  'ClientController@getAll',
            'as'    =>  'clients.page',
        ]);
        Route::get('/{uid?}', [
            'uses'  =>  'ClientController@getEdit',
            'as'    =>  'clients.edit_page'
        ]);
        Route::post('/{uid?}', [
            'uses'  =>  'ClientController@post',
            'as'    =>  'clients.edit'
        ]);
        Route::post('/{uid}/delete', [
            'uses'  =>  'ClientController@delete',
            'as'    =>  'clients.delete'
        ]);
        Route::get('token/{uid}', [
            'uses'  =>  'ClientController@getToken',
            'as'    =>  'token'
        ]);

    });

    Route::group(['prefix' => 'devices'], function() {
        Route::get('/', [
            'uses'  =>  'DeviceController@getAll',
            'as'    =>  'devices.page',
        ]);
        Route::get('/{uid?}', [
            'uses'  =>  'DeviceController@getEdit',
            'as'    =>  'devices.edit_page'
        ]);
        Route::post('/{uid?}', [
            'uses'  =>  'DeviceController@post',
            'as'    =>  'devices.edit'
        ]);
        Route::post('/{uid}/delete', [
            'uses'  =>  'DeviceController@delete',
            'as'    =>  'devices.delete'
        ]);

    });

    Route::group(['prefix' => 'account'], function() {
        Route::get('/', [
            'uses'  =>  'AccountController@index',
            'as'    =>  'account.page'
        ]);
        Route::post('/edit', [
            'uses'  =>  'AccountController@update',
            'as'    =>  'account.update'
        ]);
        Route::post('/change-password', [
            'uses'  =>  'AccountController@postChangePassword',
            'as'    =>  'account.change_password'
        ]);
        Route::post('/destroy', [
            'uses'  =>  'AccountController@destroy',
            'as'    =>  'account.destroy'
        ]);
        //Route::delete('{user}', 'UserController@destroy')->name('users.destroy');

    });

});