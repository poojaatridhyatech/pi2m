<?php

use Illuminate\Http\Request;

Route::group(['prefix' => 'v1', 'middleware' => ['apigate']], function() {
    Route::group(['prefix' => 'me'], function () {
        Route::get('wishlist', [
            'uses'  =>  '\App\Http\Controllers\Api\AdController@getWishes',
            'as'    =>  'ads.wishes'
        ]);
    });
    Route::group(['prefix' => 'auth'], function() {
        Route::post('{type}', [
            'uses'  =>  'AuthController@postAuth',
            'as'    =>  'auth.login'
        ]);
    });
    Route::group(['prefix' => 'videos'], function() {
        Route::post('/', [
            'uses'  =>  '\App\Http\Controllers\Api\ContentController@postAbout',
            'as'    =>  'content.about_multiple'
        ]);
        Route::post('/request', [
            'uses'  =>  '\App\Http\Controllers\Api\ContentController@postRequestContent',
            'as'    =>  'content.request'
        ]);
        Route::post('/search', [
            'uses'  =>  '\App\Http\Controllers\Api\ContentController@search',
            'as'    =>  'content.search'
        ]);
        Route::get('/recommended', [
            'uses'  =>  '\App\Http\Controllers\Api\ContentController@getRecommended',
            'as'    =>  'content.recommended'
        ]);
        Route::post('/recommended', [
            'uses'  =>  '\App\Http\Controllers\Api\ContentController@postRecommended',
            'as'    =>  'content.download_recommended'
        ]);
        Route::get('/trending', [
            'uses'  =>  '\App\Http\Controllers\Api\ContentController@getTrending',
            'as'    =>  'content.trending'
        ]);
        Route::post('/trending', [
            'uses'  =>  '\App\Http\Controllers\Api\ContentController@postTrending',
            'as'    =>  'content.download_trending'
        ]);
        Route::get('/{uid}', [
            'uses'  =>  '\App\Http\Controllers\Api\ContentController@getSingle',
            'as'    =>  'content.about'
        ]);
        Route::post('/{uid}/downloaded', [
            'uses'  =>  '\App\Http\Controllers\Api\ContentController@downloaded',
            'as'    =>  'content.downloaded'
        ]);
        Route::post('/{uid}/deleted', [
            'uses'  =>  '\App\Http\Controllers\Api\ContentController@deleted',
            'as'    =>  'content.deleted'
        ]);
        Route::post('/{uid}/stats', [
            'uses'  =>  '\App\Http\Controllers\Api\StatController@post',
            'as'    =>  'stats.post'
        ]);
        Route::post('/{uid}/opinion', [
            'uses'  =>  '\App\Http\Controllers\Api\ContentController@postOpinion',
            'as'    =>  'content.opinion',
        ]);
        Route::post('/{uid}/bookmark', [
            'uses'  =>  '\App\Http\Controllers\Api\ContentController@postBookmark',
            'as'    =>  'content.bookmark',
        ]);
        Route::post('/{uid}/view', [
            'uses'  =>  '\App\Http\Controllers\Api\ContentController@postView',
            'as'    =>  'content.post_view',
        ]);
        Route::group(['prefix' => '/{uid}/ads'], function () {
            Route::get('/', [
                'uses'  =>  '\App\Http\Controllers\Api\AdController@getPerVideo',
                'as'    =>  'ads.per_video'
            ]);
            Route::post('/{aduid}/wish', [
                'uses'  =>  '\App\Http\Controllers\Api\AdController@postWishlist',
                'as'    =>  'ads.wish'
            ]);
        });
    });
    Route::group(['prefix' => 'channels'], function() {
        Route::get('/', [
            'uses'  =>  '\App\Http\Controllers\Api\ChannelController@getAll',
            'as'    =>  'channel.all'
        ]);
        Route::get('/categories', [
            'uses'  =>  '\App\Http\Controllers\Api\ChannelController@getCategories',
            'as'    =>  'channel.categories'
        ]);
        Route::post('{uid}/stop', [
            'uses'  =>  '\App\Http\Controllers\Api\ChannelController@postStop',
            'as'    =>  'channel.stop'
        ]);
        Route::post('/{uid}/subscribe', [
            'uses'  =>  '\App\Http\Controllers\Api\ChannelController@postSubscribe',
            'as'    =>  'channel.subscribe'
        ]);
        Route::get('/{uid}', [
            'uses'  =>  '\App\Http\Controllers\Api\ChannelController@getSingle',
            'as'    =>  'channel.single'
        ]);
    });
    Route::group(['prefix' => 'device'], function() {
        Route::post('/settings', [
            'uses'  =>  '\App\Http\Controllers\Api\DeviceController@postSettings',
            'as'    =>  'device.settings'
        ]);
    });
    Route::group(['prefix' => 'ads'], function() {
        Route::post('/{ads_uid}/click', [
            'uses'  =>  '\App\Http\Controllers\Api\AdController@postclick',
            'as'    =>  'ads.click'
        ]);
    });

});
