<?php

namespace App\Providers;

use App\Events\ModelCreated;
use App\Events\ModelDeleted;
use App\Events\ModelUpdated;
use App\Listeners\ElasticsearchModelCreated;
use App\Listeners\ElasticsearchModelDeleted;
use App\Listeners\ElasticsearchModelUpdated;
use App\Listeners\ElasticsearchUpdateViewCounts;
use App\Listeners\LogActionModelCreated;
use App\Listeners\LogActionModelDeleted;
use App\Listeners\LogActionModelUpdated;
use App\Listeners\ElasticsearchUpdateOpinionCounts;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
        'App\Events\LogHttpRequest' => [
            'App\Listeners\LogHttpRequestListener',
        ],
        'App\Events\PushContent' => [
            'App\Listeners\PushContentListener',
        ],
        'App\Events\RemoveContent' => [
            'App\Listeners\RemoveContentListener',
        ],
        'App\Events\RecallPush' => [
            'App\Listeners\RecallPushListener',
        ],
        ModelCreated::class => [
            LogActionModelCreated::class,
            ElasticsearchModelCreated::class,
            ElasticsearchUpdateViewCounts::class,
            ElasticsearchUpdateOpinionCounts::class,
        ],
        ModelDeleted::class => [
            LogActionModelDeleted::class,
            ElasticsearchModelDeleted::class,
        ],
        ModelUpdated::class => [
            LogActionModelUpdated::class,
            ElasticsearchModelUpdated::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
