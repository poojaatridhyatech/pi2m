<?php

namespace App;

use App\Content;

class Notification extends AbstractModel
{
    protected $fillable = [
        'subject',
        'body',
        'content_id',
    ];

    public function content()
    {
        return $this->hasOne(Content::class);
    }
}
