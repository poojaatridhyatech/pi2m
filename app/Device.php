<?php

namespace App;

use App\Stat;
use App\Push;
use App\View;
use App\Client;
use App\Device;
use App\Channel;
use App\Content;
use App\Subscription;

class Device extends AbstractModel
{
    protected $fillable = [
        'uid',
        'name',
        'info',
        'onesignal_uid',
        'settings',
    ];

    public function wishes()
    {
        return $this->belongsToMany(Ad::class, 'wishlists', 'device_id', 'ad_id');
    }

    public function bookmarks()
    {
        return $this->belongsToMany(Content::class, 'bookmarks', 'device_id', 'content_id');
    }

    public function clients()
    {
        return $this->belongsToMany(Client::class, 'client_devices', 'device_id', 'client_id');
    }

    public function bookmarked(Content $content)
    {
        $bookmark = $this->bookmarks()->where('content_id', $content->id)->first();
        if ($bookmark) return true;
        
        return false;
    }

    public function pushes()
    {
        return $this->hasMany(Push::class);
    }

    public function contents()
    { 
        return $this->belongsToMany(Content::class, 'pushes', 'device_id', 'content_id');
    }

    public function subscriptions()
    {
        return $this->belongsToMany(Channel::class, 'subscriptions', 'device_id', 'channel_id');
    }

    public function subscribedTo(Channel $channel)
    {
        $subscription = $this->subscriptions()->where('channel_id', $channel->id)->where('subscribed', true)->first();
        if ($subscription) return true;

        return false;
    }

    public function subscribeTo(Channel $channel)
    {
        $subscription = Subscription::where('channel_id', $channel->id)->where('device_id', $this->id)->first();
        if ($subscription) {
            $subscription->subscribed = true;
            $subscription->save();
        } else {
            $this->subscriptions()->attach($channel->id, ['subscribed' => true]);
        }

        return true;
    }

    public function unsubscribeFrom(Channel $channel)
    {
        $subscription = Subscription::where('channel_id', $channel->id)->where('device_id', $this->id)->first();
        $subscription->subscribed = false;
        $subscription->save();

        return true;
    }

    public function likes($uid = null)
    {
        if ($uid) {
            return $this->opinions()->where('like', true)->where('uid', $uid)->first();
        }

        return $this->opinions()->where('like', true)->get();
    }

    public function dislikes($uid = null)
    {
        if ($uid) {
            return $this->opinions()->where('like', false)->where('uid', $uid)->first();
        }

        return $this->opinions()->where('like', false)->get();
    }

    public function opinions()
    {
        return $this->belongsToMany(Content::class, 'opinions', 'device_id', 'content_id');
    }

    public function views()
    {
        return $this->hasMany(View::class, 'device_id');
    }

    public function stats()
    {
        return $this->hasMany(Stat::class);
    }

    public function reccomendations(Client $client)
    {
        // get all the liked videos
        dd( $client->channels );
    }
}
