<?php

namespace App;

use App\Device;
use App\Content;

class Stat extends AbstractModel
{
    protected $fillable = [
        'device_id',
        'content_id',
        'channel_id',
        'data',
    ];

    public function device()
    {
        return $this->hasOne(Device::class);
    }

    public function content()
    {
        return $this->hasOne(Content::class);
    }
}
