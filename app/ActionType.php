<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActionType extends Model
{
    const ACTION_CREATED = 'created';
    const ACTION_UPDATED = 'updated';
    const ACTION_UPLOADED = 'uploaded';

    public $timestamps = false;
}
