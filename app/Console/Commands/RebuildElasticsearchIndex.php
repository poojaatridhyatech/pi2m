<?php

namespace App\Console\Commands;

use App\AbstractModel;
use App\Ad;
use App\AdClick;
use App\Category;
use App\Channel;
use App\Content;
use App\Jobs\ElasticDocumentCreate;
use App\Jobs\ElasticDocumentDelete;
use App\Jobs\ElasticDocumentUpdate;
use App\Opinion;
use App\Push;
use App\Subscription;
use App\View;
use App\Wishlist;
use DB;
use Elasticsearch\Client;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Log;
use Plastic;
use Storage;

class RebuildElasticsearchIndex extends Command
{
    /**
     * Created entity action case
     */
    const REINDEXING_STASH_ACTION_CREATED = 1;

    /**
     * Updated entity action case
     */
    const REINDEXING_STASH_ACTION_UPDATED = 2;

    /**
     * Deleted entity action case
     */
    const REINDEXING_STASH_ACTION_DELETED = 3;

    /**
     * Name of the file that is used as a mark saying that re-indexing is in progress right now or not (true if the file
     * exists, false otherwise).
     */
    const REINDEXING_IN_PROGRESS_MARK_FILE = 'es_reindex_in_progress';

    /**
     * {@inheritdoc}
     */
    protected $signature = 'es-index:rebuild';

    /**
     * {@inheritdoc}
     */
    protected $description = '!!!STAY AWAY IF YOU DO NOT KNOW WHAT IS THIS EXACTLY!!! Rebuild the default Elasticsearch index from scratch.';

    /**
     * Elasticsearch client wrapper.
     *
     * @var Client
     */
    protected $client;

    /**
     * @var string
     */
    protected $defaultIndex;

    /**
     * @var string
     */
    protected $indexA;

    /**
     * @var string
     */
    protected $indexB;

    /**
     * @var string
     */
    protected $oldIndex;

    /**
     * @var string
     */
    protected $currentIndex;

    /**
     * @var string
     */
    protected $indexAlias;

    /**
     * @var string
     */
    protected $documentType = '_doc';

    /**
     * @var array
     */
    protected $availableTypes = [
        'ads' => Ad::class,
        'ad_clicks' => AdClick::class,
        'categories' => Category::class,
        'channels' => Channel::class,
        'contents' => Content::class,
        'opinions' => Opinion::class,
        'pushes' => Push::class,
        'subscriptions' => Subscription::class,
        'views' => View::class,
        'wishlists' => Wishlist::class,
    ];

    /**
     * @var int
     */
    protected $bulkLimit = 1000;

    /**
     * {@inheritdoc}
     */
    public function __construct()
    {
        $this->client = Plastic::getClient();
        $this->defaultIndex = Plastic::getDefaultIndex();
        $this->indexA = $this->defaultIndex.'_a';
        $this->indexB = $this->defaultIndex.'_b';
        $this->indexAlias = env('ELASTICSEARCH_INDEX_ALIAS', '');

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        if (!$this->indexAlias) {
            $this->error('You must set the index alias in your .env file first (like ELASTICSEARCH_INDEX_ALIAS=vpp).');
            return;
        }

        try {
            set_time_limit(0);

            Storage::disk('local')->put(self::REINDEXING_IN_PROGRESS_MARK_FILE, '1');

            $this->checkIndexAndAlias();

            $this->line('Mapping.');
            $this->map();

            foreach ($this->availableTypes as $type => $class) {
                $this->saveRecords($type, $class);
            }

            $this->switchIndexAlias();

            $this->processStash();

            $this->line('All done.');
        } catch (Exception $e) {
            Storage::disk('local')->delete(self::REINDEXING_IN_PROGRESS_MARK_FILE);
            Log::info($e);
            $this->error('Something went wrong! Check the log file.');
        }
    }

    /**
     * Saves the info about the created model into ES reindexing stash table if necessary, so that it could be synced
     * properly during the re-indexing process.
     *
     * @param AbstractModel $model
     * @param int $action
     */
    public static function stash(AbstractModel $model, $action)
    {
        if (Storage::disk('local')->exists(self::REINDEXING_IN_PROGRESS_MARK_FILE)) {
            $entityClassName = get_class($model);
            $recordExists = DB::table('es_reindex_stash')->where([
                ['entity_class_name', $entityClassName],
                ['entity_id', $model->id],
                ['action', $action],
            ])->exists();

            if (!$recordExists) {
                DB::table('es_reindex_stash')->insert([
                    'entity_class_name' => $entityClassName,
                    'entity_id' => $model->id,
                    'action' => $action,
                ]);
            }
        }
    }

    protected function checkIndexAndAlias()
    {
        $possibleIndices = [$this->defaultIndex, $this->indexA, $this->indexB];

        $noIndex = true;
        $isDefaultIndex = false;

        foreach ($possibleIndices as $index) {
            if ($this->client->indices()->exists(['index' => $index])) {
                $noIndex = false;
                if ($index === $this->defaultIndex) {
                    $isDefaultIndex = true;
                }
            }
        }

        if ($noIndex) {
            $this->client->indices()->create(['index'=> $this->indexA]);
            $this->client->indices()->putAlias([
                'index' => $this->indexA,
                'name' => $this->indexAlias,
            ]);
        } else {
            if ($isDefaultIndex) {
                $params = [
                    'index' => $this->defaultIndex,
                    'name' => $this->indexAlias,
                ];
                $indexAliasExists = $this->client->indices()->existsAlias($params);

                if (!$indexAliasExists) {
                    $this->client->indices()->putAlias($params);
                }
            }

            $this->switchIndex();
        }
    }

    protected function switchIndex()
    {
        $aliasInfo = $this->client->indices()->getAlias([
            'index' => '*',
            'name' => $this->indexAlias,
        ]);

        if (isset($aliasInfo[$this->defaultIndex])) {
            $this->oldIndex = $this->defaultIndex;
            $this->currentIndex = $this->indexA;
        } elseif (isset($aliasInfo[$this->indexA])) {
            $this->oldIndex = $this->indexA;
            $this->currentIndex = $this->indexB;
        } else {
            $this->oldIndex = $this->indexB;
            $this->currentIndex = $this->indexA;
        }

        // Try to delete the index in case if it already exists for some reason.
        try {
            $this->client->indices()->delete(['index'=> $this->currentIndex]);
        } catch (Exception $e) {
            // Do nothing.
        }

        // Create new index.
        $this->client->indices()->create(['index'=> $this->currentIndex]);

        session(['elasticsearch_current_index' => $this->currentIndex]);
    }

    /**
     * Create the mapping for the index.
     */
    protected function map()
    {
        $params = [
            'index' => $this->currentIndex,
            'type' => $this->documentType,
            'body' => [
                $this->documentType => [
                    '_source' => [
                        'enabled' => true,
                    ],
                    'properties' => [
                        'type' => [
                            'type' => 'keyword',
                        ],
                        'ad_id' => [
                            'type' => 'integer',
                        ],
                        'asset_path' => [
                            'type' => 'text',
                            'index' => false,
                        ],
                        'categories' => [
                            'type' => 'nested',
                            'properties' => [
                                'id' => [
                                    'type' => 'integer',
                                ],
                                'uid' => [
                                    'type' => 'keyword',
                                ],
                                'content_id' => [
                                    'type' => 'integer',
                                ],
                                'ad_id' => [
                                    'type' => 'integer',
                                ],
                            ],
                        ],
                        'channels' => [
                            'type' => 'nested',
                            'properties' => [
                                'id' => [
                                    'type' => 'integer',
                                ],
                                'uid' => [
                                    'type' => 'keyword',
                                ],
                                'content_id' => [
                                    'type' => 'integer',
                                ],
                            ],
                        ],
                        'channel_id' => [
                            'type' => 'integer',
                        ],
                        'channel_uid' => [
                            'type' => 'keyword',
                        ],
                        'click_type' => [
                            'type' => 'integer',
                        ],
                        'client_id' => [
                            'type' => 'integer',
                        ],
                        'content_id' => [
                            'type' => 'integer',
                        ],
                        'created_at' => [
                            'type' => 'date',
                            'format' => 'yyyy-MM-dd HH:mm:ss',
                        ],
                        'deleted' => [
                            'type' => 'integer',
                        ],
                        'description' => [
                            'type' => 'text',
                        ],
                        'device_id' => [
                            'type' => 'integer',
                        ],
                        'duration' => [
                            'type' => 'integer',
                        ],
                        'id' => [
                            'type' => 'keyword',
                        ],
                        'label' => [
                            'type' => 'text',
                            'fielddata' => true,
                            'fields' => ['keyword' => ['type' => 'keyword']],
                        ],
                        'like' => [
                            'type' => 'integer',
                        ],
                        'likes_count' => [
                            'type' => 'integer',
                        ],
                        'name' => [
                            'type' => 'text',
                            'fielddata' => true,
                            'fields' => ['keyword' => ['type' => 'keyword']],
                        ],
                        'original_id' => [
                            'type' => 'integer',
                        ],
                        'pushed' => [
                            'type' => 'integer',
                        ],
                        'settings' => [
                            'type' => 'nested',
                            'properties' => [
                                'type' => [
                                    'type' => 'keyword',
                                ],
                                'position' => [
                                    'type' => 'keyword',
                                ],
                                'dismissible' => [
                                    'type' => 'integer',
                                ],
                            ],
                        ],
                        'subscribed' => [
                            'type' => 'integer',
                        ],
                        'title' => [
                            'type' => 'text',
                            'fielddata' => true,
                            'fields' => ['keyword' => ['type' => 'keyword']],
                        ],
                        'uid' => [
                            'type' => 'keyword',
                        ],
                        'updated_at' => [
                            'type' => 'date',
                            'format' => 'yyyy-MM-dd HH:mm:ss',
                        ],
                        'user_id' => [
                            'type' => 'integer',
                        ],
                        'view_type_id' => [
                            'type' => 'integer',
                        ],
                        'views_count' => [
                            'type' => 'integer',
                        ],
                    ],
                ],
            ],
        ];

        $this->client->indices()->putMapping($params);
    }

    /**
     * @param string $type
     * @param string $class
     */
    protected function saveRecords($type, $class)
    {
        $recordsCount = $class::count();

        if ($recordsCount) {
            $this->line('Rebuilding index for "' . $type . '" type.');
            $bar = $this->output->createProgressBar($recordsCount);

            for ($i = 0; $i <= $recordsCount; $i += $this->bulkLimit) {
                $records = $class::allForElasticsearch($this->bulkLimit, $i);
                $batchCount = count($records);

                if ($batchCount) {
                    foreach ($records as $record) {
                        // Make sure the record is saved into the new index.
                        $record->documentIndex = $this->currentIndex;
                    }

                    $this->bulkSave($records);
                    $bar->advance($batchCount);
                }
            }

            $bar->finish();
            $this->line('');
        }
    }

    /**
     * Bulk save a collection Models.
     *
     * @param array|Collection $collection
     *
     * @return mixed
     */
    protected function bulkSave($collection = [])
    {
        $params = [];

        foreach ($collection as $item) {
            $docData = $item->getDocumentData();

            $params['body'][] = [
                'index' => [
                    '_id' => $docData['id'],
                    '_type' => $this->documentType,
                    '_index' => $this->currentIndex,
                ],
            ];
            $params['body'][] = $docData;
        }

        return $this->client->bulk($params);
    }

    protected function switchIndexAlias()
    {
        $this->client->indices()->updateAliases([
            'body' => [
                'actions' => [
                    [
                        'remove' => ['index' => $this->oldIndex, 'alias' => $this->indexAlias]
                    ],
                    [
                        'add' => ['index' => $this->currentIndex, 'alias' => $this->indexAlias]
                    ],
                ],
            ],
        ]);

        Storage::disk('local')->delete(self::REINDEXING_IN_PROGRESS_MARK_FILE);

        // Delete the old index if it exists.
        try {
            $this->client->indices()->delete(['index'=> $this->oldIndex]);
        } catch (Exception $e) {
            // Do nothing.
        }
    }

    /**
     * Syncs ES index with the models that have been created, updated or deleted during the re-indexing process.
     */
    protected function processStash()
    {
        $recordsCount = DB::table('es_reindex_stash')->count();

        if ($recordsCount) {
            $this->line('Syncing the records from stash');
            $bar = $this->output->createProgressBar($recordsCount);

            for ($i = 0; $i <= $recordsCount; $i += $this->bulkLimit) {
                $records = DB::table('es_reindex_stash')->limit($this->bulkLimit)->offset($i)->get();
                $batchCount = count($records);

                if ($batchCount) {
                    foreach ($records as $record) {
                        $class = $record->entity_class_name;
                        $model = $class::find($record->entity_id);
                        $queue = 'elastic';

                        if ($model) {
                            if ($record->action === self::REINDEXING_STASH_ACTION_CREATED) {
                                dispatch((new ElasticDocumentCreate($model))->onQueue($queue));
                            } elseif ($record->action === self::REINDEXING_STASH_ACTION_UPDATED) {
                                dispatch((new ElasticDocumentUpdate($model))->onQueue($queue));
                            }
                        } else {
                            if ($record->action === self::REINDEXING_STASH_ACTION_DELETED) {
                                dispatch((new ElasticDocumentDelete($class, $record->entity_id))->onQueue($queue));
                            }
                        }
                    }

                    $bar->advance($batchCount);
                }
            }

            DB::table('es_reindex_stash')->truncate();

            $bar->finish();
            $this->line('');
        }
    }
}
