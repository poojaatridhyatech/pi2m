<?php

namespace App\Console\Commands;

use Generate;
use Illuminate\Console\Command;

class GenerateCrud extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'snique:crud {name} {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Snique create a CRUD instance.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Generate::crudInstance($this->argument('name'), $this->argument('type'));

        $this->info("Created!");
    }
}
