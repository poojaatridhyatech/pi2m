<?php

namespace App\Console\Commands;

use Mail;
use App\Push;
use App\Content;
use App\Mail\PublishingContent;
use Illuminate\Console\Command;
use App\Events\PushContent as PushContentEvent;


class PushContentCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'snique:push_content';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pushes content that is ready to be published.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $scheduled = Push::where('pushed', false)->where('push_at', '<', date('Y-m-d H:i:s'))->get();
        // foreach ($scheduled as $push) {
        //     if ($push->device->onesignal_uid) {
        //         event(new PushContentEvent($push->content, $push->device, $push));
        //     }
        // }

        // if (count($scheduled) > 0)
        // {
        //     // Mail::to('imants@eskimo.uk.com')->send(new PublishingContent($scheduled));
        // }
        
        // echo count($scheduled) . ' videos pushed.';
    }
}
