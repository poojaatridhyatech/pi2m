<?php

namespace App\Routines;

use DB;
use App\Client;
use App\Content;
use App\ViewType;
use Carbon\Carbon;

class TrendingContent
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function get($count = 3)
    {
        $toTake = ($count) ? ($count > 3 || $count < 0) ? 3 : $count : 3;
        $ret = [];
        $contents = $this->getAvailableContents();

        $trending = DB::table('contents')
                ->leftJoin('opinions', 'contents.id', '=', 'opinions.content_id')
                ->leftJoin('views', 'contents.id', '=', 'views.content_id')
                ->select(
                    DB::raw('contents.id')
                )
                ->selectRaw('count(opinions.content_id)+count(views.content_id) as total')
                ->where('opinions.like', '=', 1)
                ->whereIn('contents.id', $contents)
                ->take($toTake)
                ->orderBy('total', 'desc')
                ->get();
            
        foreach ($trending as $trend) {
            $ret[] = $trend->id;
        }

        return Content::whereIn('id', $ret)->get();
    }

    public function getAvailableContents()
    {
        $justIds = [];
        $contentIds = [];
        $interactedWith = [];
        $channels = $this->client->channels;

        foreach ($channels as $channel) {
            $set = $channel->contents;
            foreach ($set as $one) {
                array_push($justIds, $one->id);
                $contentIds = [
                    'content' => $one,
                    'likes' => $one->opinions()->where('like', 1)->whereBetween('opinions.created_at', [Carbon::now()->subWeeks(1), Carbon::now()])->count(),
                    'views'  =>  $one->totalViews(),
                ];
            }
        }

        return $justIds;
    }
}
