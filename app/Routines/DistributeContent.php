<?php

namespace App\Routines;

use Log;
use Generate;
use Exception;
use App\Device;
use App\Client;
use App\Channel;
use App\Content;
use App\Routines\CanPush;
use App\Events\PushContent;

class DistributeContent
{
    protected $content;

    protected $device;
    protected $push;
    
    private $status;

    public function __construct(Content $content)
    {
        $this->content = $content;
    }

    public function toDevice(Device $device)
    {
        try {

            event(new PushContent($this->content, $device));

            $this->queued();

        } catch (Exception $e) {

            $this->failed($e);

        }

        return $this;
    }

    public function toChannel(Channel $channel)
    {
        try {
            $subscribedDevices = $channel->subscribers()
                                         ->where('subscriptions.channel_id', $channel->id)
                                         ->where('subscriptions.subscribed', true)
                                         ->get();


            foreach ($subscribedDevices as $device) {
                event( new PushContent($this->content, $device) );

                $this->queued();
            }

        } catch (Exception $e) {

            $this->failed($e);

        }

        return $this;
    }

    protected function notSending()
    {
        $this->status = 'not_sending';

        return $this;
    }

    protected function queued()
    {
        $this->status = 'queued';

        return $this;
    }

    protected function failed($e)
    {
        $this->status = 'failed';

        Log::error('Content couldnt be pushed, because: ' . Generate::usefulExceptionMessage($e));

        return $this;
    }

    public function status()
    {
        return $this->status;
    }
}
