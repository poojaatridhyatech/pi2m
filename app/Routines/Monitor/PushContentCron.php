<?php

namespace App\Routines\Monitor;

use Log;
use Mail;
use Generate;
use App\User;
use Exception;
use App\Client;
use App\Device;
use App\Content;
use App\Mail\ErrorMail;
use App\Events\PushContent;
use App\Routines\SchedulePush;

class PushContentCron
{
    public function __construct()
    {
        $this->logAction('Constructed.');

        $this->setContent();
        $this->setDevice();
    }

    public function pushImmediately()
    {
        try {
            $this->logAction('Sending immediately.');

            $when = null;
            $routine = new SchedulePush($this->content, $this->device, 'Notif');
            $resp = $routine->at(null); // send immediately

            $push = $resp->getPush();
            event(new PushContent($this->content, $this->device, $push));

            $this->logAction('Push queued');

            return $push;

        } catch (Exception $e) {

            $errorId = Generate::noDashUid();

            Mail::to(['imants@eskimo.uk.com', 'victoria@eskimo.uk.com'])->send(new ErrorMail( 'Cron failed ('. $errorId .') <br> ' . Generate::usefulExceptionMessage($e) ) );

            Log::error('PUSH CRON FAILED! Error UUID: ' . $errorId . ' - ' . Generate::usefulExceptionMessage($e) );

        }
    }

    public function logAction($action)
    {
        Log::info('PUSH_MONITOR: ' . $action);
    }

    public function setContent()
    {
        $this->content = Content::first();

        if ( ! $this->content ) {
            $this->content = Content::create([
                'uid'   =>  Generate::noDashUid(),
                'title' =>  'monitor',
                'description' =>  'Monitoring content instance',
                'resource_uri'  =>  'https://s3-eu-west-1.amazonaws.com/snique.files/3q5ovoVpQVG1U87qlsnt_SampleVideo_1280x720_1mb.mp4',
                'categories'  =>  'animals',
                'user_id'   =>  User::first()->id,
            ]);
        }

        return $this;
    }

    public function setDevice()
    {
        $client = Client::where('onesignal_app_id', 'eed077b2-fec1-49ed-aae1-8149855ae434')->first();
        $this->device = Device::where('onesignal_uid', '91b0a8a8-c799-4c6c-a248-07ac77b68863')->first();

        if ( ! $this->device ) {
            $this->device = Device::create([
                'uid'   =>  Generate::noDashUid(),
                'name'  =>  'monitor device',
                'info'  =>  '',
                'onesignal_uid' =>  '91b0a8a8-c799-4c6c-a248-07ac77b68863',
                'settings'  =>  '',
            ]);
        }

        $client->devices()->attach([$this->device->id]);

        $this->logAction('Using Device with the onesignal uid: ' . $this->device->onesignal_uid);

        return $this;
    }
}
