<?php

namespace App\Routines\Auth;

use App\User;
use Exception;
use GuzzleHttp\Client;

/**
 * Class FacebookRoutine
 * @package Stikit\Routines\Auth\Social
 */
class Facebook
{
    /**
     * @var array
     */
    protected $fields = [
        'id',
        'name',
        'email'
    ];

    /**
     * @var Customer
     */
    public $customer;

    /**
     * @var string
     */
    protected $token;

    public function __construct() {}

    /**
     * @return $this
     */
    public function attempt()
    {
        $http = new Client;
        $response = $http->get(
            'https://graph.facebook.com/v2.5/me?fields='.implode(',', $this->fields).'&access_token='.$this->token
        );

        $this->user = json_decode((string) $response->getBody());

        return $this;
    }

    /**
     * @return Customer
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $token
     * @return $this
     */
    public function setToken($token)
    {
        $this->token = (string) $token;

        return $this;
    }
}
