<?php

namespace App\Routines\Crud\Update;

use Auth;
use Generate;
use App\Device as Model;
use Illuminate\Http\Request;

class Device
{
    public function __construct(Model $device, Request $request)
    {
        $device->name = $request->name;
        $device->onesignal_uid = $request->onesignal_uid;
        $device->save();

        //$device->clients()->sync($request->clients);
    }
}
