<?php

namespace App\Routines\Crud\Update;

use App\Mail\UserCreated;
use App\Role;
use App\User as Model;
use DB;
use Generate;
use Illuminate\Http\Request;

class User
{
    public function __construct(Model $user, Request $request)
    {
        DB::transaction(function () use ($user, $request) {
            $user->name = $request->name;
            $user->email = $request->email;
            $user->status = $request->status ? 1 : 0;
            $user->save();

            $roleIds = $request->roles;
            $userRole = Role::where('name', Role::ROLE_USER)->first();
            if ($roleIds) {
                if (!in_array($userRole->id, $roleIds)) {
                    array_push($roleIds, $userRole->id);
                }
            } else {
                $roleIds = [$userRole->id];
            }

            $user->roles()->sync($roleIds);
        });
    }
}
