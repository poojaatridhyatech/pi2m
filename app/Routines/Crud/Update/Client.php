<?php

namespace App\Routines\Crud\Update;

use Auth;
use Generate;
use App\Client as Model;
use Illuminate\Http\Request;

class Client
{
    public function __construct(Model $client, Request $request)
    {
        $client->name = $request->name;
        $client->description = $request->description;
        $client->onesignal_app_id = $request->onesignal_app_id;
        $client->onesignal_api_key = $request->onesignal_api_key;
        $client->save();

        $client->channels()->sync($request->channels);
    }
}
