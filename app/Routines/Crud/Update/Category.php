<?php

namespace App\Routines\Crud\Update;

use Auth;
use Generate;
use App\Category as Model;
use Illuminate\Http\Request;

class Category
{
    public function __construct(Model $model, Request $request)
    {
        $model->name = $request->name;
        $model->description = $request->description;
        if ($request->asset_path && strlen($request->asset_path) > 1) {
            $model->asset_path = $request->asset_path;
        }
        $model->save();

        // attach videos
        $model->contents()->sync($request->videos);
    }
}
