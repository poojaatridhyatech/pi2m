<?php

namespace App\Routines\Crud\Update;

use App\Ad as Model;
use App\Events\ModelUpdated;
use DB;
use Illuminate\Http\Request;

class Ad
{
    public function __construct(Model $ad, Request $request)
    {
        DB::transaction(function () use ($ad, $request) {
            $type = [
                'type' => $request->type,
                'position' => $request->position,
                'dismissible' => $request->dismissible ? 1 : 0,
            ];

            $ad->title = $request->title;
            $ad->description = $request->description;
            $ad->url = $request->url;
            $ad->expires_at = $request->expires_at;
            $ad->asset_path = $request->asset_path;
            $ad->resource_uri = $request->resource_uri;
            $ad->price = (double) $request->price;
            $ad->currency = $request->currency;
            $ad->type = json_encode($type);
            $ad->save();

            $ad->categories()->sync($request->categories);

            event(new ModelUpdated($ad));
        });
    }
}
