<?php

namespace App\Routines\Crud\Update;

use Auth;
use Extract;
use Generate;
use App\Channel as Model;
use Illuminate\Http\Request;
use App\Routines\Crud\Traits\NeedsAttachements;

class Channel
{
    use NeedsAttachements;

    public $channel;

    public function __construct(Model $channel, Request $request)
    {
        $this->channel = $channel;
        $this->channel->label = $request->label;
        $this->channel->description = $request->description;
        $this->channel->name = $request->name;
        if ($request->asset_path && strlen($request->asset_path) > 1) {
            $this->channel->asset_path = $request->asset_path;
        }
        $this->channel->save();

        // attach clients
        $this->channel->clients()->sync($request->apps);

        // attach videos
        $this->channel->contents()->sync($request->videos);
    }

    public function get()
    {
        return $this->channel;
    }
}

            