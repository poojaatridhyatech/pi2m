<?php

namespace App\Routines\Crud;

class Attach
{
    protected $what;

    public function __construct($to, $what, $model, $data)
    {
        $this->what = $what;
        $uids = $this->extractIds($data);

        $ids = $model->whereIn('uid', $uids)->get();

        // $cl
// str_singular
        dd($ids);
    }

    public function extractIds($data)
    {
        $ids = [];
        // handle attaching to clients
        foreach ($data as $key => $field) {
            if (starts_with($key, $this->what)) {
                $exp = explode('_', $key);
                $uid = $exp[1];
                $ids[] = $uid;
                // $client = Client::where('uid', $uid)->first();
                // $ids[] = $client->id;
            }
            // $channel->clients()->sync($ids);
        }

        return $ids;
    }
}
