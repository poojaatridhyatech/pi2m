<?php

namespace App\Routines\Crud\Delete;

use Auth;
use Generate;
use App\Client as Model;
use Illuminate\Http\Request;

class Client
{
    public function __construct(Model $client)
    {
        // detach all channels
        // foreach ($client->channels as $channel) {
        //     $channel->clients()->detach([$client->id]);
        // }

        $client->channels()->detach();
        $client->devices()->detach();

        // delete the client
        $client->delete();

    }
}

            