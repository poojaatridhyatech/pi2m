<?php

namespace App\Routines\Crud\Delete;

use App\Category as Model;

class Category
{
    public function __construct(Model $category)
    {

        $category->contents()->detach();

        $category->delete();
    }
}