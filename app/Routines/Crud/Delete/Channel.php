<?php

namespace App\Routines\Crud\Delete;

use Auth;
use Generate;
use App\Channel as Model;
use Illuminate\Http\Request;

class Channel
{
    public function __construct(Model $channel)
    {
        // detach contents
        $channel->contents()->detach();

        // detach clients
        $channel->clients()->detach();

        // detach subscriptions
        $channel->subscribers()->detach();

        // detach subscriptions
        foreach ($channel->pushes as $push) {
            $push->delete();
        }


        $channel->delete();

    }
}

            