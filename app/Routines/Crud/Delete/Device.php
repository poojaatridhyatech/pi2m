<?php

namespace App\Routines\Crud\Delete;

use Auth;
use Generate;
use App\Device as Model;
use Illuminate\Http\Request;

class Device
{
    public function __construct(Model $device)
    {
        $device->clients()->detach();
        $device->subscriptions()->detach();
        $device->opinions()->detach();
        $device->wishes()->detach();
        foreach ($device->views as $view) {
            $view->delete();
        }

        // detach subscriptions
        foreach ($device->pushes as $push) {
            $push->delete();
        }


        // delete the device
        $device->delete();

    }
}

            