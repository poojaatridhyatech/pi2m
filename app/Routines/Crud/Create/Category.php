<?php

namespace App\Routines\Crud\Create;

use DB;
use Auth;
use Generate;
use App\Category as Model;
use Illuminate\Http\Request;

class Category
{
    public $category;

    public function __construct(Request $request)
    {
        $this->category = Model::create([
            'uid'   =>  Generate::noDashUid(),
            'name' =>  $request->name,
            'description'   =>  $request->description,
            'asset_path'    =>  isset($request->asset_path)? $request->asset_path : '',
            'user_id'   =>  Auth::user()->id,
        ]);

        // attach videos
        $this->category->contents()->sync($request->videos);
    }

}
