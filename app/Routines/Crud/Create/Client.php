<?php

namespace App\Routines\Crud\Create;

use Auth;
use Generate;
use App\Client as Model;
use Illuminate\Http\Request;

class Client
{
    public function __construct(Request $request)
    {
        $client = Model::create([
            'uid'   =>  Generate::noDashUid(),
            'name' => $request->name,
            'description' => $request->description,
            'onesignal_app_id' => $request->onesignal_app_id,
            'onesignal_api_key' => $request->onesignal_api_key,
            'secret'    =>  Generate::secret(),
            'user_id'   =>  Auth::user()->id,
            'onesignal_app_id'  =>  $request->onesignal_app_id,
            'onesignal_api_key' =>  $request->onesignal_api_key,
        ]);

        $client->channels()->sync($request->channels);
    }
}
