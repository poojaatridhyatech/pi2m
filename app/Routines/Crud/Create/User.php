<?php

namespace App\Routines\Crud\Create;

use App\Client;
use App\Mail\UserCreated;
use App\Role;
use App\User as Model;
use DB;
use Generate;
use Illuminate\Http\Request;
use Mail;

class User
{
    public function __construct(Request $request)
    {
        DB::transaction(function () use ($request) {
            $user = Model::create([
                'uid' => Generate::noDashUid(),
                'name' => $request->name,
                'email' => $request->email,
                'password' => '',
                'status' => $request->status ? 1 : 0,
            ]);

            $roleIds = $request->roles;
            $userRole = Role::where('name', Role::ROLE_USER)->first();
            if ($roleIds) {
                if (!in_array($userRole->id, $roleIds)) {
                    array_push($roleIds, $userRole->id);
                }
            } else {
                $roleIds = [$userRole->id];
            }

            $user->roles()->sync($roleIds);

            Client::create([
                'uid' => Generate::noDashUid(),
                'name' => 'default',
                'description' => 'Default client',
                'secret' => Generate::secret(),
                'user_id' => $user->id,
            ]);

            Mail::to($request->email)->send(new UserCreated($user));
        });
    }
}
