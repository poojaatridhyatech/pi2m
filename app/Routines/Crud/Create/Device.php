<?php

namespace App\Routines\Crud\Create;

use Auth;
use Generate;
use App\Device as Model;
use Illuminate\Http\Request;

class Device
{
    public function __construct(Request $request)
    {
        $device = Model::create([
            'uid'   =>  Generate::noDashUid(),
            'name' => $request->name,
            'onesignal_uid' => $request->onesignal_uid,
        ]);

        //$device->clients()->sync($request->clients);
    }
}
