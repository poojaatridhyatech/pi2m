<?php

namespace App\Routines\Crud\Create;

use App\Events\ModelCreated;
use Auth;
use App\Ad as Model;
use DB;
use Generate;
use Illuminate\Http\Request;

class Ad
{
    public function __construct(Request $request)
    {
        DB::transaction(function () use ($request) {
            $type = [
                'type' => $request->type,
                'position' => $request->position,
                'dismissible' => $request->dismissible ? 1 : 0,
            ];

            $ad = Model::create([
                'uid' => Generate::noDashUid(),
                'title' => $request->title,
                'description' => $request->description,
                'url' => $request->url,
                'asset_path' => $request->asset_path,
                'expires_at' => $request->expires_at,
                'user_id' => Auth::user()->id,
                'resource_uri' => $request->resource_uri,
                'price' => (double) $request->price,
                'currency' => $request->currency,
                'type' => json_encode($type),
            ]);

            $ad->categories()->sync($request->categories);

            event(new ModelCreated($ad));
        });
    }
}
