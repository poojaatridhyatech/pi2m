<?php

namespace App\Routines\Crud\Create;

use Auth;
use Extract;
use Generate;
use App\Channel as Model;
use Illuminate\Http\Request;

class Channel
{
    public $channel;

    public function __construct(Request $request)
    {
        $this->channel = Model::create([
            'uid' =>  Generate::noDashUid(),
            'user_id' =>  Auth::user()->id,
            'name' =>  $request->name,
            'label' =>  $request->label,
            'description' =>  $request->description,
            'asset_path' =>  $request->asset_path? $request->asset_path : '',
        ]);
        
        // attach clients
        $this->channel->clients()->sync($request->apps);

        // attach videos
        $this->channel->contents()->sync($request->videos);
    }

    public function get()
    {
        return $this->channel;
    }
}

            