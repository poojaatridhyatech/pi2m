<?php

namespace App\Routines;

use App\Push;
use App\Device;
use App\Content;

class SchedulePush
{
    protected $push;

    public function __construct(Content $content, Device $device, $message = null)
    {
        $this->device  = $device;
        $this->content = $content;
        $this->message = ($message) ? str_replace('$Title', $this->content->title, $message) : $this->content->title . ' received';
        
        $this->createPush();
    }

    protected function createPush()
    {
        $this->push = Push::create([
            'device_id'     =>  $this->device->id,
            'content_id'    =>  $this->content->id,
            'pushed'        =>  false,
            'notification'  =>  $this->message,
        ]);

        return $this;
    }

    public function at($time)
    {
        if ($time) {
            $this->push->push_at = $time;
            $this->push->save();
        }

        return $this;
    }

    public function getPush()
    {
        return $this->push;
    }
}
