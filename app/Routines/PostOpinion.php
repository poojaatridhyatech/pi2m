<?php

namespace App\Routines;

use App\Device;
use App\Content;
use App\Opinion;

class PostOpinion
{
    public function __construct(Device $device, Content $content, $like)
    {
        $this->device   = $device;
        $this->content  = $content;
        $this->like     = $like;

        if ($this->like === true) {
            $this->likeContent();
        } else {
            $this->dislikeContent();
        }
    }

    public function likeContent()
    {
        // check if device already likes the content
        if ($this->device->likes($this->content->uid)) {
            $this->device->opinions()->detach($this->content->id);

            return;
        }

        $this->device->opinions()->attach($this->content->id, [
            'like'  =>  $this->like
        ]);

        return true;
    }

    public function dislikeContent()
    {
        // check if device already dislikes the content
        if ($this->device->dislikes($this->content->uid)) {
            $this->device->opinions()->detach($this->content->id);

            return;
        }

        $this->device->opinions()->attach($this->content->id, [
            'like'  =>  $this->like
        ]);

        return true;
    }
}