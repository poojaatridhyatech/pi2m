<?php

namespace App\Routines;

use DB;
use App\Device;
use App\Client;
use App\Content;
use App\ViewType;

class RecommendContent
{
    protected $device;
    protected $client;

    public function __construct(Client $client, Device $device)
    {
        $this->device = $device;
        $this->client = $client;
    }

    public function get($count = 3)
    {
        $toTake = ($count) ? ($count > 3 || $count < 0) ? 3 : $count : 3;

        $likedCategories = [];
        $likedContent = [];
        foreach ( $this->device->likes() as $like ) {
            array_push($likedContent, $like->id);

            foreach ($like->categories()->pluck('category_id')->toArray() as $id) {
                array_push($likedCategories, $id);
            }
        }

        $contents = $this->getAvailableContents();

        $toRecommend = DB::table('contents')
                   ->join('content_categories', 'contents.id', '=', 'content_categories.content_id')
                   ->join('categories', 'categories.id', '=', 'content_categories.category_id')
                   ->select('contents.id')
                   ->whereNotIn('contents.id', $likedContent)
                   ->take($toTake)
                   ->get()
                   ->pluck('id')->toArray();

        return Content::whereIn('id', $toRecommend)->get();
    }

    public function getAvailableContents()
    {
        $contentIds = [];
        $interactedWith = [];
        $channels = $this->client->channels;

        foreach ($channels as $channel) {
            $set = $channel->contents;
            foreach ($set as $one) {
                $contentIds[] = $one;
            }
        }

        return $contentIds;
    }
}