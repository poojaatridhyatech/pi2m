<?php

namespace App;

use App\Events\ModelCreated;
use App\Events\ModelDeleted;
use App\Events\ModelUpdated;
use App\User;
use App\Ad;
use App\Channel;
use App\Content;
use DB;
use Illuminate\Support\Collection;
use Sleimanx2\Plastic\Searchable;

class Category extends AbstractModel
{
//    use Searchable;

    public $syncDocument = false;

    public $elasticDocumentType = 'category';

    public $logAction = true;

    protected $fillable = [
        'name',
        'description',
        'user_id',
        'uid',
        'asset_path',
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => ModelCreated::class,
        'updated' => ModelUpdated::class,
        'deleted' => ModelDeleted::class,
    ];

    public function contents()
    {
        return $this->belongsToMany(Content::class, 'content_categories', 'category_id', 'content_id');
    }

    public function ads()
    {
        return $this->belongsToMany(Ad::class, 'ad_category', 'category_id', 'ad_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * Build the document data for storing in Elasticsearch.
     *
     * @return array
     */
    public function buildDocument()
    {
        return [
            'type' => $this->elasticDocumentType,
            'id' => $this->elasticDocumentType . '-' . $this->id,
            'uid' => $this->uid,
            'name' => $this->name,
            'description' => $this->description,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }

    /**
     * Process the model before syncing with the Elasticsearch index.
     */
    public function prepareForElasticsearch()
    {
        $this->type = $this->elasticDocumentType;
    }

    /**
     * Returns the data required for ES index rebuild.
     *
     * @param int $limit
     * @param int $offset
     * @return Collection
     */
    public static function allForElasticsearch($limit = 0, $offset = 0)
    {
        return self::limit($limit)
            ->offset($offset)
            ->get([
                DB::raw('CONCAT("category") AS type'),
                'id',
                'uid',
                'name',
                'description',
                'user_id',
                'created_at',
                'updated_at',
            ]);
    }
}
