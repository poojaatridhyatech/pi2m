<?php

namespace App;

class ClientChannel extends AbstractModel
{
    protected $fillable = [
        'client_id',
        'channel_id',
    ];
}
