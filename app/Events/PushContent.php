<?php

namespace App\Events;

use App\Push;
use App\Client;
use App\Device;
use App\Content;
use App\Channel as ChannelModel;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;


class PushContent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $content;
    public $device;
    public $push;
    public $successfullySentTo = [];
    public $failedToSend = [];

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Content $content, Device $device, $push = null, $client = null)
    {
        $this->device = $device;
        $this->client = $client;
        $this->content = $content;
        $this->push = $push;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('content-distribution');
    }
}