<?php

namespace App;

use App\Http\Controllers\ContentController;
use Auth;
use App\Ad;
use App\Events\ModelCreated;
use App\Events\ModelDeleted;
use App\Events\ModelUpdated;
use App\Tag;
use App\User;
use App\View;
use App\Device;
use App\Channel;
use App\ViewType;
use DB;
use Illuminate\Support\Collection;
use Laravel\Scout\Searchable;

class Content extends AbstractModel
{
    use \Sleimanx2\Plastic\Searchable;

    public $syncDocument = false;

    public $elasticDocumentType = 'content';

    public $logAction = true;

    protected $fillable = [
        'uid',
        'title',
        'resource_uri',
        'user_id',
        'extension',
        'description',
        'mime',
        'publish_at',
        'unpublish_at',
        'parent_id',
        'duration',
        'asset_path',
        'expires_at',
    ];

    protected $hidden = [
    //    'id',
        'pivot'
    ];

    public $dates = [
        'updated_at'
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     * @see ContentController for ModelCreated and ModelUpdated events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'deleted' => ModelDeleted::class,
    ];

    public function searchableAs()
    {
        return 'snique_content';
    }

    public function ads_array()
    {
        //return $this->belongsToMany(Ad::class, 'content_ads', 'content_id', 'ad_id');

        $query = Ad::distinct()
                    ->select('ads.*')
                    ->join('ad_category', 'ad_category.ad_id', '=', 'ads.id')
                    ->join('content_categories', 'ad_category.category_id', '=', 'content_categories.category_id')
                    ->where('content_categories.content_id', $this->id );

        $ads_array = $query->get();

        return $ads_array;
        
    }

    public function devices()
    {
        return $this->belongsToMany(Device::class, 'pushes', 'content_id', 'device_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'content_categories', 'content_id', 'category_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'content_tags', 'content_id', 'tag_id');
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return [
            'title' =>  $this->title,
            'description' =>  $this->description,
        ];
    }

    public function opinions()
    {
        return $this->belongsToMany(Device::class, 'opinions', 'content_id', 'device_id');
    }

    public function likes()
    {
        return $this->opinions()->where('like', 1)->get();
    }

    public function channels()
    {
        return $this->belongsToMany(Channel::class, 'channel_contents', 'content_id', 'channel_id');
    }

    public function views()
    {
        return $this->hasMany(View::class, 'content_id');
    }

    public function totalViews()
    {
        $viewed = $this->views()->where('view_type_id', ViewType::finished()->id)->get();

        return count($viewed);
    }

    public function bookmarks()
    {
        return $this->belongsToMany(Device::class, 'bookmarks', 'content_id', 'device_id');
    }

    public function watchLater(Device $device)
    {
        $view = View::create([
            'device_id'     =>  $device->id,
            'content_id'    =>  $this->id,
            'watched'       =>  null,
            'view_type_id'  =>  ViewType::watchLater()->id,
        ]);
        
        return $view;
    }

    public function watched(Device $device)
    {
        $view = View::create([
            'device_id'     =>  $device->id,
            'content_id'    =>  $this->id,
            'watched'       =>  null,
            'view_type_id'  =>  ViewType::finished()->id,
        ]);
        
        return $view;
    }

    public function wantsToWatchLater(Device $device)
    {
        $view = $this->views()
                     ->where('view_type_id', ViewType::watchLater()->id)
                     ->where('device_id', $device->id)
                     ->first();
        
        if ($view) return true;

        return false;
    }

    public function readyToPublish()
    {
        return $this->where('publish_at', '<', date('Y-m-d H:i:s'))->get();
    }

    public function isExplicit()
    {
        $res = $this->tags()->where('name', 'explicit')->first();
        if ($res) {
            return true;
        }

        return false;
    }

    public function canPush()
    {
        if (Auth::user()->id === $this->user_id) {
            return true;
        }

        return false;
    }

    /**
     * Build the document data for storing in Elasticsearch.
     *
     * @return array
     */
    public function buildDocument()
    {
        return [
            'type' => $this->elasticDocumentType,
            'id' => $this->elasticDocumentType . '-' . $this->id,
            'original_id' => $this->id,
            'uid' => $this->uid,
            'title' => $this->title,
            'description' => $this->description,
            'asset_path' => $this->asset_path,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
            'channels' => $this->channels,
            'categories' => $this->categories,
        ];
    }

    /**
     * Process the model before syncing with the Elasticsearch index.
     */
    public function prepareForElasticsearch()
    {
        $this->type = $this->elasticDocumentType;
        $this->channels = $this->getRelatedChannels();
        $this->categories = $this->getRelatedCategories();
    }

    /**
     * Returns the data required for ES index rebuild.
     *
     * @param int $limit
     * @param int $offset
     * @return Collection
     */
    public static function allForElasticsearch($limit = 0, $offset = 0)
    {
        $contents = self::limit($limit)
            ->offset($offset)
            ->get([
                DB::raw('CONCAT("content") AS type'),
                'id',
                'uid',
                'title',
                'description',
                'asset_path',
                'user_id',
                'created_at',
                'updated_at',
            ]);

        if (count($contents)) {
            $channels = self::getChannelsData();
            $categories = self::getCategoriesData();

            foreach ($contents as $content) {
                $content->channels = (!empty($channels[$content->id])) ? $channels[$content->id] : [];
                $content->categories = (!empty($categories[$content->id])) ? $categories[$content->id] : [];
            }
        }

        return $contents;
    }

    public static function getChannelsData()
    {
        $data = [];
        $result = DB::table('channel_contents AS cc')
            ->join('channels AS c', 'cc.channel_id', '=', 'c.id')
            ->get(['c.id', 'c.uid', 'cc.content_id']);

        if (count($result)) {
            $collection = collect($result);
            $data = $collection->groupBy('content_id')->all();
        }

        return $data;
    }

    public static function getCategoriesData()
    {
        $data = [];
        $result = DB::table('content_categories AS cc')
            ->join('categories AS c', 'cc.category_id', '=', 'c.id')
            ->get(['c.id', 'c.uid', 'cc.content_id']);

        if (count($result)) {
            $collection = collect($result);
            $data = $collection->groupBy('content_id')->all();
        }

        return $data;
    }

    protected function getRelatedChannels()
    {
        return DB::table('channel_contents AS cc')
            ->join('channels AS c', 'cc.channel_id', '=', 'c.id')
            ->where('cc.content_id', $this->id)
            ->get(['c.id', 'c.uid', 'cc.content_id']);
    }

    public function getRelatedCategories()
    {
        return DB::table('content_categories AS cc')
            ->join('categories AS c', 'cc.category_id', '=', 'c.id')
            ->where('cc.content_id', $this->id)
            ->get(['c.id', 'c.uid', 'cc.content_id']);
    }
}
