<?php

namespace App\Transformers;

class Transformer
{
    /**
     * Transform multiple objects
     *
     * @param  array $collection
     * @return array
     */
    public function transformMultiple($collection)
    {
        $transformed = [];
        if ($collection)
        {
            foreach ($collection as $single) {
                $transformed[] = $this->transform($single);
            }
        }

        return $transformed;
    }
}