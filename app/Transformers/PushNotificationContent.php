<?php

namespace App\Transformers;

use App\Device;

class PushNotificationContent extends Transformer
{
    public function transform($raw, Device $device)
    {
        $t = new \stdClass();
        $t->uid         = (string) $raw->uid;
        $t->title       = (string) $raw->title;
        $t->thumbnail   = $raw->asset_path;
        $t->channels    = $this->transformChannels($raw, $device);
        $t->expires_at  = (string) $raw->expires_at;
        $t->duration    = (int) $raw->duration;
        $t->likes       = (int) $raw->opinions()->where('like', true)->count();
        $t->dislikes    = (int) $raw->opinions()->where('like', false)->count();
        $t->views       = (int) $raw->totalViews();
        $t->watch_later = (bool) $raw->wantsToWatchLater($device);
        
        return $t;
    }

    public function transformChannels($raw, $device)
    {
        $ret = [];
        foreach ($raw->channels as $ch) {
            $ret[] = $ch->uid;
        }

        return $ret;
    }
}
