<?php

namespace App\Transformers;

use App\Device;
use App\Channel as Model;
use App\Transformers\Category as CategoryTransformer;

class Channel extends Transformer
{
    public function transform(Model $raw, Device $device)
    {
        $categoryTransformer = new CategoryTransformer;

        $t = new \stdClass();
        $t->uid             = (string) $raw->uid;
        $t->label           = (string) $raw->label;
        $t->name            = (string) $raw->name;
        $t->description     = (string) $raw->description;
        $t->thumbnail       = (string) $raw->asset_path;
        $t->user            = $raw->user;
        $t->subscriptions   = (int) $raw->subscribers()->where('subscribed', true)->count();
        $t->videos          = (int) $raw->contents()->count();
        $t->subscribed      = (bool) $device->subscribedTo($raw);
        $t->categories      = $categoryTransformer->transformMultiple($raw->categories);

        return $t;
    }
}