<?php

namespace App\Transformers;

use App\Device;
use App\Category as Model;

class Category extends Transformer
{
    public function transform(Model $raw)
    {
        $t = new \stdClass();
        $t->uid             = (string) $raw->uid;
        $t->asset_path      = (string) $raw->asset_path;
        $t->name            = (string) $raw->name;
        $t->description     = (string) $raw->description;

        return $t;
    }
}
