<?php

namespace App\Transformers;

use Storage;
use App\Device;
use App\Transformers\Channel as ChannelTransformer;

class Content extends Transformer
{
    public function transform($raw, Device $device)
    {
        $t                  = new \stdClass();
        $t->uid             = (string) $raw->uid;
        $t->download_uri    = Storage::disk('s3')->url('content/'. $t->uid.'.' . $raw->extension);
        $t->title           = (string) $raw->title;
        $t->description     = (string) $raw->description;
        $t->thumbnail       = $raw->asset_path;
        $t->categories      = $raw->categories;
        $t->user            = $raw->user;
        $t->expires_at      = (string) $raw->expires_at;
        $t->duration        = (int) $raw->duration;
        $t->likes           = (int) $raw->opinions()->where('like', true)->count();
        $t->dislikes        = (int) $raw->opinions()->where('like', false)->count();
        $t->views           = (int) $raw->totalViews();
        $t->bookmarked      = (bool) $device->bookmarked($raw);
        $t->watch_later     = (bool) $raw->wantsToWatchLater($device);
        $t->channels        = $this->transformChannels($raw, $device);
        $t->explicit        = $raw->isExplicit();

        return $t;
    }

    public function transformChannels($raw, $device)
    {
        $ret = [];
        $ct = new ChannelTransformer;
        foreach ( $raw->channels as $ch ) {
            $ret[] = $ct->transform($ch, $device);
        }

        return $ret;
    }
}