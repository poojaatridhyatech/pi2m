<?php

namespace App\Transformers;

use App\Ad as Model;

class Ad extends Transformer
{
    public function transform(Model $raw)
    {
        $t = new \stdClass();
        $t->uid             = (string) $raw->uid;
        $t->asset_path      = (string) $raw->asset_path;
        $t->name            = (string) $raw->name;
        $t->description     = (string) $raw->description;
        $t->download_uri    = (string) $raw->resource_uri;
        $t->expires_at      = (string) $raw->expires_at;
        $t->price           = $raw->price;
        $t->options         = json_decode($raw->type);

        return $t;
    }
}
