<?php

namespace App;

use App\Content;

class Tag extends AbstractModel
{
    protected $fillable = [
        'name'
    ];

    public function contents()
    {
        return $this->belongsToMany(Content::class, 'content_tags', 'tag_id', 'content_id');
    }
}
