<?php

namespace App;

use App\Events\ModelCreated;
use App\Events\ModelDeleted;
use App\Events\ModelUpdated;
use App\User;
use App\Client;
use App\Category;
use App\Content;
use App\Push;
use DB;
use Illuminate\Support\Collection;
use Sleimanx2\Plastic\Searchable;

class Channel extends AbstractModel
{
//    use Searchable;

    public $syncDocument = false;

    public $elasticDocumentType = 'channel';

    public $logAction = true;

    protected $fillable = [
        'label',
        'name',
        'description',
        'asset_path',
        'uid',
        'user_id',
    ];

    protected $hidden = [
        'pivot'
    ];

    public function contentStatus(Content $content)
    {
        $push = Push::where('channel_id', $this->id)->where('content_id', $content->id)->latest()->first();
        if ( ! $push ) {
            return 'unpushed';
        }

        if ( strtotime(date('Y-m-d H:i:s')) < strtotime($push->push_at) ) {
            return 'queued';
        }

        return 'pushed';
    }

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => ModelCreated::class,
        'updated' => ModelUpdated::class,
        'deleted' => ModelDeleted::class,
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function pushes()
    {
        return $this->hasMany(Push::class);
    }

    public function subscribers()
    {
        return $this->belongsToMany(Device::class, 'subscriptions', 'channel_id', 'device_id');
    }

    public function clients()
    {
        return $this->belongsToMany(Client::class, 'client_channels', 'channel_id', 'client_id');
    }

    public function contents()
    {
        return $this->belongsToMany(Content::class, 'channel_contents', 'channel_id', 'content_id');
    }

    public function userOwns(User $user)
    {
        if ($this->user_id === $user->id) {
            return true;
        }

        return false;
    }

    /**
     * Build the document data for storing in Elasticsearch.
     *
     * @return array
     */
    public function buildDocument()
    {
        return [
            'type' => $this->elasticDocumentType,
            'id' => $this->elasticDocumentType . '-' . $this->id,
            'original_id' => $this->id,
            'uid' => $this->uid,
            'label' => $this->label,
            'name' => $this->name,
            'description' => $this->description,
            'asset_path' => $this->asset_path,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }

    /**
     * Process the model before syncing with the Elasticsearch index.
     */
    public function prepareForElasticsearch()
    {
        $this->type = $this->elasticDocumentType;
    }

    /**
     * Returns the data required for ES index rebuild.
     *
     * @param int $limit
     * @param int $offset
     * @return Collection
     */
    public static function allForElasticsearch($limit = 0, $offset = 0)
    {
        return self::limit($limit)
            ->offset($offset)
            ->get([
                DB::raw('CONCAT("channel") AS type'),
                'id',
                'uid',
                'label',
                'name',
                'description',
                'asset_path',
                'user_id',
                'created_at',
                'updated_at',
            ]);
    }
}
