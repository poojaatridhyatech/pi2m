<?php

namespace App;

use App\Events\ModelCreated;
use App\Events\ModelDeleted;
use App\Events\ModelUpdated;
use DB;
use Illuminate\Support\Collection;
use Sleimanx2\Plastic\Searchable;

class Subscription extends AbstractModel
{
//    use Searchable;

    const SUBSCRIBED = 1;
    const UNSUBSCRIBED = 0;

    public $syncDocument = false;

    public $elasticDocumentType = 'subscription';

    protected $fillable = [
        'channel_id',
        'device_id',
        'subscribed',
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => ModelCreated::class,
        'updated' => ModelUpdated::class,
        'deleted' => ModelDeleted::class,
    ];

    public function channel()
    {
        return $this->hasOne(Channel::class, 'id', 'channel_id');
    }

    /**
     * Build the document data for storing in Elasticsearch.
     *
     * @return array
     */
    public function buildDocument()
    {
        return [
            'type' => $this->elasticDocumentType,
            'id' => $this->elasticDocumentType . '-' . $this->id,
            'device_id' => $this->device_id,
            'channel_id' => $this->channel_id,
            'user_id' => $this->user_id,
            'subscribed' => $this->subscribed,
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
            'channel_uid' => $this->channel_uid,
        ];
    }

    /**
     * Process the model before syncing with the Elasticsearch index.
     */
    public function prepareForElasticsearch()
    {
        $this->type = $this->elasticDocumentType;
        $this->channel_uid = $this->channel->uid;
        $this->user_id = $this->channel->user_id;
    }

    /**
     * Returns the data required for ES index rebuild.
     *
     * @param int $limit
     * @param int $offset
     * @return Collection
     */
    public static function allForElasticsearch($limit = 0, $offset = 0)
    {
        return self::join('channels AS c', 'subscriptions.channel_id', '=', 'c.id')
            ->limit($limit)
            ->offset($offset)
            ->get([
                DB::raw('CONCAT("subscription") AS type'),
                'subscriptions.id',
                'subscriptions.device_id',
                'subscriptions.channel_id',
                'subscriptions.subscribed',
                'subscriptions.created_at',
                'subscriptions.updated_at',
                'c.uid AS channel_uid',
                'c.user_id',
            ]);
    }
}
