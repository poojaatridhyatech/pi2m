<?php

namespace App\Listeners;

use Log;
use Generate;
use GuzzleHttp\Client;
use App\Events\RecallPush;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecallPushListener implements ShouldQueue
{
    const API_URL = 'https://onesignal.com/api/v1';
    const ENDPOINT_NOTIFICATIONS = '/notifications';

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Client $http)
    {
        $this->http = $http;
    }

    /**
     * Handle the event.
     *
     * @param  RecallPush  $event
     * @return void
     */
    public function handle(RecallPush $event)
    {
        try {
            $request['headers']['Content-Type']  = 'application/json';

            foreach ($event->pushes as $push) {
                \Log::info($push->device->onesignal_uid);
                $client = $push->device->clients[0];

                $OSAPIKey = $client->onesignal_api_key;
                $OSAPPId = $client->onesignal_app_id;
                $request['headers']['Authorization'] = 'Basic ' . $OSAPIKey;
                $request['body'] = json_encode([
                        'content_available'     =>  true, // @IMPORTANT this parameter ensures that this is a silent notification
                        'app_id'                =>  $OSAPPId,
                        'include_player_ids'    =>  [$push->device->onesignal_uid],
                            'data'              =>  [
                                'content_uid' => $push->content->uid,
                                'channel_uid' => $push->channel->uid,
                        ]
                ]);

                $successfulPush = false;
                if (env('APP_ENV') !== 'testing') {
                    $resp = $this->http->post(self::API_URL . self::ENDPOINT_NOTIFICATIONS, $request);
                    if ($resp->getStatusCode() !== 200) {
                        Log::error('ERROR-WHEN-RECALLING. Didnt receive 200 from OneSignal.');
                    } else {
                        $successfulPush = true;
                    }
                } else {
                    $successfulPush = true;
                }

                if ($successfulPush) {
                    Log::info('RECALL-NOTIFICATION-SENT to "' . $push->device->uid . '" (OS_UID: '. $push->device->onesignal_uid .')');

                    $push->deleted = true;
                    $push->save();
                }

            }

            return true;

        } catch (Exception $e) {
            $this->failedToSend[] = $this->event->device;
            Log::error('ERROR-WHEN-RECALLING. ' . Generate::usefulExceptionMessage($e));
        }
    }
}
