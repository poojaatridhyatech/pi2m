<?php

namespace App\Listeners;

use App\Events\RemoveContent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RemoveContentListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RemoveContent  $event
     * @return void
     */
    public function handle(RemoveContent $event)
    {
        //
    }
}
