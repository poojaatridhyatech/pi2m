<?php

namespace App\Listeners;

use App\Events\LogHttpRequest;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogHttpRequestListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LogHttpRequest  $event
     * @return void
     */
    public function handle(LogHttpRequest $event)
    {
        //
    }
}
