<?php

namespace App\Listeners;

use App\Console\Commands\RebuildElasticsearchIndex;
use App\Events\ModelDeleted;
use App\Jobs\ElasticDocumentDelete;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sleimanx2\Plastic\Searchable;

class ElasticsearchModelDeleted
{
    /**
     * Handle the event.
     *
     * @param ModelDeleted $event
     */
    public function handle(ModelDeleted $event)
    {
        $entityClassName = get_class($event->model);
        $traits = class_uses_recursive($entityClassName);

        if (isset($traits[Searchable::class])) {
            RebuildElasticsearchIndex::stash($event->model, RebuildElasticsearchIndex::REINDEXING_STASH_ACTION_DELETED);
            dispatch((new ElasticDocumentDelete($entityClassName, $event->model->id))->onQueue('elastic'));
        }
    }
}
