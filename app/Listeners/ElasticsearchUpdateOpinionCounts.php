<?php

namespace App\Listeners;

use App\Events\ModelCreated;
use App\Jobs\ElasticUpdateVideoChannels;
use App\Opinion;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ElasticsearchUpdateOpinionCounts
{
    /**
     * Handle the event.
     *
     * @param ModelCreated $event
     */
    public function handle(ModelCreated $event)
    {
        if (get_class($event->model) === Opinion::class && $event->model->like) {
            dispatch((new ElasticUpdateVideoChannels($event->model->content))->onQueue('elastic'));
        }
    }
}
