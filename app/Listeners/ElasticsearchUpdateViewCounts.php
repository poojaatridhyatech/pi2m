<?php

namespace App\Listeners;

use App\Events\ModelCreated;
use App\Jobs\ElasticUpdateVideoChannels;
use App\View;
use App\ViewType;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ElasticsearchUpdateViewCounts
{
    /**
     * Handle the event.
     *
     * @param ModelCreated $event
     */
    public function handle(ModelCreated $event)
    {
        if (get_class($event->model) === View::class && $event->model->type->name === ViewType::TYPE_FINISHED) {
            dispatch((new ElasticUpdateVideoChannels($event->model->content))->onQueue('elastic'));
        }
    }
}
