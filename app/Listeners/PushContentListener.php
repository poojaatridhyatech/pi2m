<?php

namespace App\Listeners;

use Log;
use Storage;
use App\Push;
use Generate;
use Exception;
use OneSignal;
use App\Device;
use App\Content;
use GuzzleHttp\Client;
use App\Events\PushContent;
use App\Services\Notification;
use App\Routines\TrendingContent;
use Illuminate\Queue\InteractsWithQueue;
use App\Transformers\Ad as AdTransformer;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Transformers\PushNotificationContent as ContentTransformer;


class PushContentListener implements ShouldQueue
{
    protected $event; 
    public $failedToSend = [];
    public $successfullySentTo = [];

    const API_URL = 'https://onesignal.com/api/v1';
    const ENDPOINT_NOTIFICATIONS = '/notifications';

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        // 
    }

    /**
     * Handle the event.
     *
     * @param  PushContent  $event
     * @return void
     */
    public function handle(PushContent $event)
    {
        $this->event = $event;
        $this->http = new Client;

        $this->push();
    }

    public function push()
    {
        $adTransformer = new AdTransformer;
        $transformer   = new ContentTransformer;

        try {
            if (isset($this->event->client))
            {
                $trendingContent = [];
                $trending = new TrendingContent($this->event->client, $this->event->device);
                $trending = $routine->get();
                foreach ($trending as $content) {
                    $trendingContent[] = $transformer->transform($content, $this->event->device);
                }
            }

            $client = $this->event->device->clients[0];

            $OSAPIKey = $client->onesignal_api_key;
            $OSAPPId = $client->onesignal_app_id;
            $ads = $adTransformer->transformMultiple($this->event->content->ads_array());
            $request['headers']['Content-Type']  = 'application/json';
            $request['headers']['Authorization'] = 'Basic ' . $this->event->device->clients[0]->onesignal_api_key;
            $request['body'] = json_encode([
                'content_available'     =>  true,   // @IMPORTANT this parameter ensures that this is a silent notification
                'app_id'                =>  $this->event->device->clients[0]->onesignal_app_id,
                'include_player_ids'    =>  [$this->event->device->onesignal_uid],
                    'data'              =>  [
                        'video_url' => $this->event->content->resource_uri,
                        'content'   => $transformer->transform($this->event->content, $this->event->device),
                        'ad'        => $ads,
                        'trending'  =>  (isset($trendingContent)) ? $trendingContent : [],
                ]
            ]);

// \Log::info($request['body']);
            $successfulPush = false;
            if (env('APP_ENV') !== 'testing') {
                $resp = $this->http->post(self::API_URL . self::ENDPOINT_NOTIFICATIONS, $request);
                if ($resp->getStatusCode() !== 200) {
                    Log::error('ERROR-WHEN-PUSHING. Didnt receive 200 from OneSignal.');
                } else {
                    $successfulPush = true;
                }
            } else {
                $successfulPush = true;
            }

            if ($successfulPush === true) {
                Log::info('PULL-NOTIFICATION-SENT to "' . $this->event->device->uid . '" (OS_UID: '. $this->event->device->onesignal_uid .') with the message ' . $this->event->push->notification);

                $sentAd = ( count($ads) > 0 ) ? $ads[0] : null;
                // push has been sent
                if ($this->event->push) {
                    $push = $this->event->push;
                    $push->pushed = true;
                    $push->ad_id = ($sentAd) ? $sentAd->id : null;
                    $push->save();
                } else {
                    Push::create([
                        'device_id' =>  $this->event->device->id,
                        'content_id'    =>  $this->event->content->id,
                        'ad_id'    =>  ($sentAd) ? $sentAd->id : null,
                    ]);

                }
            }

            return true;

        } catch(Exception $e) {
            $this->failedToSend[] = $this->event->device;
            Log::error('ERROR-WHEN-PUSHING. ' . Generate::usefulExceptionMessage($e));
            if ( isset($this->event->device->uid) ) {
                Log::error('ERROR-WHEN-PUSHING. Device ' . $this->event->device->uid);
            }
            
        }

        return true;
    }
}
