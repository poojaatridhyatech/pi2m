<?php

namespace App\Listeners;

use App\Console\Commands\RebuildElasticsearchIndex;
use App\Events\ModelCreated;
use App\Jobs\ElasticDocumentCreate;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sleimanx2\Plastic\Searchable;

class ElasticsearchModelCreated
{
    /**
     * Handle the event.
     *
     * @param ModelCreated $event
     */
    public function handle(ModelCreated $event)
    {
        $traits = class_uses_recursive(get_class($event->model));

        if (isset($traits[Searchable::class])) {
            RebuildElasticsearchIndex::stash($event->model, RebuildElasticsearchIndex::REINDEXING_STASH_ACTION_CREATED);
            dispatch((new ElasticDocumentCreate($event->model))->onQueue('elastic'));
        }
    }
}
