<?php

namespace App\Listeners;

use App\Action;
use App\ActionType;
use App\Events\ModelUpdated;
use Auth;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogActionModelUpdated
{
    public $user;

    /**
     * Create the event listener.
     */
    public function __construct()
    {
        $this->user = Auth::user();
    }

    /**
     * Handle the event.
     *
     * @param ModelUpdated $event
     */
    public function handle(ModelUpdated $event)
    {
        if (property_exists($event->model, 'logAction') && $event->model->logAction) {
            $actionType = ActionType::where('name', ActionType::ACTION_UPDATED)->first();

            if ($this->user && $actionType) {
                Action::create([
                    'user_id' => $this->user->id,
                    'action_type_id' => $actionType->id,
                    'entity_class_name' => get_class($event->model),
                    'entity_id' => $event->model->id,
                ]);
            }
        }
    }
}
