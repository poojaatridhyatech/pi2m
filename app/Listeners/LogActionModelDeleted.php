<?php

namespace App\Listeners;

use App\Action;
use App\Events\ModelDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogActionModelDeleted
{
    /**
     * Handle the event.
     *
     * @param  ModelDeleted  $event
     * @return void
     */
    public function handle(ModelDeleted $event)
    {
        if (property_exists($event->model, 'logAction') && $event->model->logAction) {
            Action::where([
                ['entity_class_name', get_class($event->model)],
                ['entity_id', $event->model->id],
            ])->delete();
        }
    }
}
