<?php

namespace App\Listeners;

use App\Console\Commands\RebuildElasticsearchIndex;
use App\Events\ModelUpdated;
use App\Jobs\ElasticDocumentUpdate;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sleimanx2\Plastic\Searchable;

class ElasticsearchModelUpdated
{
    /**
     * Handle the event.
     *
     * @param ModelUpdated $event
     */
    public function handle(ModelUpdated $event)
    {
        $traits = class_uses_recursive(get_class($event->model));

        if (isset($traits[Searchable::class])) {
            RebuildElasticsearchIndex::stash($event->model, RebuildElasticsearchIndex::REINDEXING_STASH_ACTION_UPDATED);
            dispatch((new ElasticDocumentUpdate($event->model))->onQueue('elastic'));
        }
    }
}
