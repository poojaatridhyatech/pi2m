<?php

namespace App\Mail;

use Generate;
use App\User;
use App\Activation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserCreated extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Your new Pushit2Mobile account details')->view('emails.user_created')->with([
            'link' => Generate::activationLink( Activation::new($this->user) ) . '&activate=true',
            'user' => $this->user,
        ]);
    }
}
