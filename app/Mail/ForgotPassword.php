<?php

namespace App\Mail;

use Generate;
use App\User;
use App\Activation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class ForgotPassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Your Pushit2Mobile password reset token')->view('emails.reset_password')->with([
            'link' => Generate::activationLink(Activation::new($this->user)),
            'user' => $this->user,
        ]);

    }
}
