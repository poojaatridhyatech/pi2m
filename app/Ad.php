<?php

namespace App;

use App\Events\ModelDeleted;
use DB;
use Illuminate\Support\Collection;
use Sleimanx2\Plastic\Searchable;

class Ad extends AbstractModel
{
//    use Searchable;

    const TYPE_BANNER = 'banner';
    const TYPE_VIDEO = 'video';
    const POSITION_TOP = 'top';
    const POSITION_BOTTOM = 'bottom';

    public $syncDocument = false;

    public $elasticDocumentType = 'ad';

    public $logAction = true;

    protected $fillable = [
        'uid',
        'title',
        'description',
        'url',
        'expires_at',
        'asset_path',
        'user_id',
        'resource_uri',
        'price',
        'currency',
        'type',
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'deleted' => ModelDeleted::class,
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'uid';
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'ad_category', 'ad_id', 'category_id');
    }

    public function devices()
    {
        return $this->belongsToMany(Device::class, 'wishlists', 'ad_id', 'device_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * Build the document data for storing in Elasticsearch.
     *
     * @return array
     */
    public function buildDocument()
    {
        return [
            'type' => $this->elasticDocumentType,
            'id' => $this->elasticDocumentType . '-' . $this->id,
            'original_id' => $this->id,
            'uid' => $this->uid,
            'title' => $this->title,
            'description' => $this->description,
            'user_id' => $this->user_id,
            'settings' => $this->settings,
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
            'categories' => $this->categories,
        ];
    }

    /**
     * Process the model before syncing with the Elasticsearch index.
     */
    public function prepareForElasticsearch()
    {
        $this->settings = json_decode($this->type);
        $this->type = $this->elasticDocumentType;
        $this->categories = $this->getRelatedCategories();
    }

    /**
     * Returns the data required for ES index rebuild.
     *
     * @param int $limit
     * @param int $offset
     * @return Collection
     */
    public static function allForElasticsearch($limit = 0, $offset = 0)
    {
        $ads = self::limit($limit)
            ->offset($offset)
            ->get([
                DB::raw('CONCAT("ad") AS type'),
                'id',
                'uid',
                'title',
                'description',
                'user_id',
                'type AS settings',
                'created_at',
                'updated_at',
            ]);

        if (count($ads)) {
            $categories = self::getCategoriesData();

            foreach ($ads as $ad) {
                $ad->settings = json_decode($ad->settings);
                $ad->categories = (!empty($categories[$ad->id])) ? $categories[$ad->id] : [];
            }
        }

        return $ads;
    }

    public static function getCategoriesData()
    {
        $data = [];
        $result = DB::table('ad_category AS ac')
            ->join('categories AS c', 'ac.category_id', '=', 'c.id')
            ->get(['c.id', 'c.uid', 'ac.ad_id']);

        if (count($result)) {
            $collection = collect($result);
            $data = $collection->groupBy('ad_id')->all();
        }

        return $data;
    }

    public function getRelatedCategories()
    {
        return DB::table('ad_category AS ac')
            ->join('categories AS c', 'ac.category_id', '=', 'c.id')
            ->where('ac.ad_id', $this->id)
            ->get(['c.id', 'c.uid', 'ac.ad_id']);
    }
}
