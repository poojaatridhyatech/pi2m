<?php

namespace App;

class AdClickType extends AbstractModel
{
    const TYPE_SKIP = 'skip';
    const TYPE_OPEN = 'open';
    const TYPE_CLICK = 'click';

    protected $fillable = [
        'key',
        'name',
    ];
}
