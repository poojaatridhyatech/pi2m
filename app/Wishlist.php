<?php

namespace App;

use App\Events\ModelCreated;
use App\Events\ModelDeleted;
use DB;
use Illuminate\Support\Collection;
use Sleimanx2\Plastic\Searchable;

class Wishlist extends AbstractModel
{
//    use Searchable;

    public $syncDocument = false;

    public $elasticDocumentType = 'wishlist';

    protected $fillable = [
        'device_id',
        'ad_id',
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => ModelCreated::class,
        'deleted' => ModelDeleted::class,
    ];

    /**
     * Build the document data for storing in Elasticsearch.
     *
     * @return array
     */
    public function buildDocument()
    {
        return [
            'type' => $this->elasticDocumentType,
            'id' => $this->elasticDocumentType . '-' . $this->id,
            'original_id' => $this->id,
            'device_id' => $this->device_id,
            'ad_id' => $this->ad_id,
            'created_at' => $this->created_at->toDateTimeString(),
        ];
    }

    /**
     * Process the model before syncing with the Elasticsearch index.
     */
    public function prepareForElasticsearch()
    {
        $this->type = $this->elasticDocumentType;
    }

    /**
     * Returns the data required for ES index rebuild.
     *
     * @param int $limit
     * @param int $offset
     * @return Collection
     */
    public static function allForElasticsearch($limit = 0, $offset = 0)
    {
        return self::limit($limit)
            ->offset($offset)
            ->get([
                DB::raw('CONCAT("wishlist") AS type'),
                'id',
                'device_id',
                'ad_id',
                'created_at',
            ]);
    }
}
