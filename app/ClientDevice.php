<?php

namespace App;

class ClientDevice extends AbstractModel
{
    protected $fillable = [
        'client_id',
        'device_id',
    ];
}