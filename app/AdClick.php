<?php

namespace App;

use App\Ad;
use App\Device;
use App\Client;
use App\AdClickType;
use App\Events\ModelCreated;
use App\Events\ModelDeleted;
use DB;
use Illuminate\Support\Collection;
use Sleimanx2\Plastic\Searchable;

class AdClick extends AbstractModel
{
//    use Searchable;

    public $syncDocument = false;

    public $elasticDocumentType = 'ad_click';

    protected $fillable = [
        'ad_id',
        'client_id',
        'device_id',
        'click_type'
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => ModelCreated::class,
        'deleted' => ModelDeleted::class,
    ];

    public function ad()
    {
        return $this->hasOne(Ad::class, 'id', 'ad_id');
    }

    public function device()
    {
        return $this->hasOne(Device::class, 'id', 'device_id');
    }

    public function client()
    {
        return $this->hasOne(Client::class, 'id', 'client_id');
    }

    public function clicktype()
    {
        return $this->hasOne(AdClickType::class, 'id', 'click_type');
    }

    /**
     * Build the document data for storing in Elasticsearch.
     *
     * @return array
     */
    public function buildDocument()
    {
        return [
            'type' => $this->elasticDocumentType,
            'id' => $this->elasticDocumentType . '-' . $this->id,
            'original_id' => $this->id,
            'ad_id' => $this->ad_id,
            'client_id' => $this->client_id,
            'device_id' => $this->device_id,
            'click_type' => $this->click_type,
            'created_at' => $this->created_at->toDateTimeString(),
        ];
    }

    /**
     * Process the model before syncing with the Elasticsearch index.
     */
    public function prepareForElasticsearch()
    {
        $this->type = $this->elasticDocumentType;
    }

    /**
     * Returns the data required for ES index rebuild.
     *
     * @param int $limit
     * @param int $offset
     * @return Collection
     */
    public static function allForElasticsearch($limit = 0, $offset = 0)
    {
        return self::limit($limit)
            ->offset($offset)
            ->get([
                DB::raw('CONCAT("ad_click") AS type'),
                'id',
                'ad_id',
                'client_id',
                'device_id',
                'click_type',
                'created_at',
            ]);
    }
}
