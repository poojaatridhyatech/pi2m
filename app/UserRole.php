<?php

namespace App;

class UserRole extends AbstractModel
{
    protected $fillable = [
        'user_id',
        'role_id',
    ];
}
