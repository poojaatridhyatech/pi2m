<?php

namespace App;

use App\Ad;
use App\Role;
use App\Client;
use App\Content;
use App\Channel;
use App\Category;
use App\Activation;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'uid', 'status'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token', 
        //'id',
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'uid';
    }

    public function categories()
    {
        return $this->hasMany(Category::class, 'user_id', 'id');
    }

    public function ads()
    {
        return $this->hasMany(Ad::class, 'user_id', 'id');
    }

    public function activations()
    {
        return $this->hasMany(Activation::class, 'user_id', 'id');
    }

    public function isAdmin()
    {
        return $this->hasRole(Role::admin());
    }

    public function contents()
    {
        return $this->hasMany(Content::class);
    }

    public function channels()
    {
        return $this->hasMany(Channel::class);
    }

    public function clients()
    {
        return $this->hasMany(Client::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_roles', 'user_id', 'role_id');
    }

    public function hasRole(Role $role)
    {
        foreach ($this->roles as $assignedRole) {
            if ($assignedRole->name === $role->name) {
                return true;
            }
        }

        return false;
    }

    public function allChannels()
    {
        $channels = [];

        // gather all channels that the user owns
        foreach ($this->channels as $channel) {
            $channel->owns = true;

            $channels[] = $channel;
            
        }

        return $channels;
    }
}
