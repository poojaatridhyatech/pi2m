<?php

namespace App;

class Role extends AbstractModel
{
    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';

    protected $fillable = [
        'name',
        'label',
    ];

    public function scopeUser($query)
    {
        return $query->where('name', 'user')->first();
    }

    public function scopeAdmin($query)
    {
        return $query->where('name', 'admin')->first();
    }

    public function users()
    {
        return $this->belongsToMany(Role::class, 'user_roles', 'role_id', 'user_id');
    }
}
