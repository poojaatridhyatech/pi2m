<?php

namespace App\Repos;

abstract class Repo
{
    protected $model;

    public function __construct(string $model)
    {
        $this->model = new $model;
    }
}