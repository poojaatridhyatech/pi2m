<?php

namespace App\Repos;

use App\Action as Model;
use App\Ad;
use App\Category;
use App\Channel;
use App\Client;
use App\Content;
use Carbon\Carbon;

class ActionRepo extends Repo
{
    public function __construct()
    {
        parent::__construct(Model::class);
    }

    /**
     * Reference list of the entities for which the actions are being recorded.
     *
     * @var array
     */
    protected $entities = [
        Ad::class => 'ad',
        Category::class => 'category',
        Channel::class => 'channel',
        Client::class => 'app',
        Content::class => 'video',
    ];

    public function loadActions(int $tzOffset)
    {
        $query = $this->model->where('actions.created_at', '>=', Carbon::now()->startOfDay()->subDay())
            ->join('users AS u', 'actions.user_id', '=', 'u.id')
            ->join('action_types AS at', 'actions.action_type_id', '=', 'at.id')
            ->leftJoin('ads AS a', function ($join) {
                $join->on('actions.entity_id', '=', 'a.id')->where('actions.entity_class_name', Ad::class);
            })
            ->leftJoin('categories AS c', function ($join) {
                $join->on('actions.entity_id', '=', 'c.id')->where('actions.entity_class_name', Category::class);
            })
            ->leftJoin('channels AS ch', function ($join) {
                $join->on('actions.entity_id', '=', 'ch.id')->where('actions.entity_class_name', Channel::class);
            })
            ->leftJoin('clients AS cl', function ($join) {
                $join->on('actions.entity_id', '=', 'cl.id')->where('actions.entity_class_name', Client::class);
            })
            ->leftJoin('contents AS ct', function ($join) {
                $join->on('actions.entity_id', '=', 'ct.id')->where('actions.entity_class_name', Content::class);
            })
            ->orderBy('actions.created_at', 'desc');

        $actions = $query->get([
            'at.name AS action',
            'u.name AS username',
            'actions.entity_class_name',
            'actions.created_at',

            'a.uid AS ad_uid',
            'a.title AS a_title',

            'c.uid AS category_uid',
            'c.name AS c_title',

            'ch.uid AS channel_uid',
            'ch.label AS ch_title',

            'cl.uid AS client_uid',
            'cl.name AS cl_title',

            'ct.uid AS video_uid',
            'ct.title AS ct_title',
        ])->toArray();

        $result = [];

        if ($actions) {
            foreach ($actions as $action) {
                if ($action['ad_uid']) {
                    $action['url'] = route('ads.index');
                    $action['title'] = $action['a_title'];
                }

                if ($action['category_uid']) {
                    $action['url'] = route('categories.videos', [$action['category_uid']]);
                    $action['title'] = $action['c_title'];
                }

                if ($action['channel_uid']) {
                    $action['url'] = route('channels.contents', [$action['channel_uid']]);
                    $action['title'] = $action['ch_title'];
                }

                if ($action['client_uid']) {
                    $action['url'] = route('clients.edit_page', [$action['client_uid']]);
                    $action['title'] = $action['cl_title'];
                }

                if ($action['video_uid']) {
                    $action['url'] = route('content.edit_page', [$action['video_uid']]);
                    $action['title'] = $action['ct_title'];
                }

                $createdAt = Carbon::createFromFormat('Y-m-d H:i:s', $action['created_at'])->addMinutes($tzOffset);

                $action['time'] = $createdAt->format('g:i A');
                $action['action'] = trans('misc.action-types.' . $action['action']);
                $action['entity'] = trans('misc.entities.' . $this->entities[$action['entity_class_name']]);

                if ($createdAt->isToday()) {
                    $result['today'][] = $action;
                } elseif ($createdAt->isYesterday()) {
                    $result['yesterday'][] = $action;
                } else {
                    $result[$createdAt->diffForHumans()][] = $action;
                }
            }
        }

        return $result;
    }
}
