<?php

namespace App\Repos;

use App\Ad;
use App\AdClickType;
use App\Channel;
use App\Content;
use App\Opinion;
use App\Push;
use App\Subscription;
use App\View;
use App\ViewType;
use Carbon\Carbon;
use DB;
use Elasticsearch\Client;
use Illuminate\Database\Eloquent\Collection;
use Plastic;

class DashboardRepo
{
    const INCOMING_DATE_FORMAT = 'Y-m-d';

    /**
     * Elasticsearch client wrapper.
     *
     * @var Client
     */
    protected $elasticsearchClient;

    /**
     * Elasticsearch index name.
     *
     * @var string
     */
    protected $index;

    /**
     * "From date" filter.
     *
     * @var string
     */
    protected $from;

    /**
     * "To date" filter.
     *
     * @var string
     */
    protected $to;

    /**
     * Holds the date filters to be applied to the Elasticsearch range queries.
     *
     * @var array
     */
    protected $queryRange;

    /**
     * Holds the date limits for the Elasticsearch queries.
     * @see DashboardRepo::setExtendedBoundsForType()
     *
     * @var array
     */
    protected $extendedBounds;

    /**
     * Time interval used in Elasticsearch queries (like "day", "month", etc.).
     *
     * @var string
     */
    protected $queryInterval;

    /**
     * User ID filter.
     *
     * @var int
     */
    protected $userIdFilter;

    /**
     * Channel ID filter.
     *
     * @var int
     */
    protected $channelIdFilter;

    /**
     * Content (video) ID filter.
     *
     * @var int
     */
    protected $contentIdFilter;

    /**
     * Content (video) IDs filter.
     *
     * @var array
     */
    protected $contentIdsFilter;

    /**
     * Ad ID filter.
     *
     * @var int
     */
    protected $adIdFilter;

    /**
     * Ad type filter.
     *
     * @var int
     */
    protected $adTypeFilter;

    /**
     * Ad IDs filter.
     *
     * @var array
     */
    protected $adIdsFilter;

    /**
     * Content (video) ID filter.
     *
     * @var int
     */
    protected $categoryIdsFilter;

    /**
     * Timezone offset in minutes as provided by the user's browser.
     *
     * @var int
     */
    protected $tzOffsetInMins;

    /**
     * Timezone offset in "+01:00" format to be used in Elasticearch range queries.
     *
     * @var string
     */
    protected $tzOffsetHm;

    public function __construct()
    {
        $this->elasticsearchClient = Plastic::getClient();
        $this->setIndex();
    }

    protected function setIndex()
    {
        $defaultIndex = Plastic::getDefaultIndex();
        $indexAlias = env('ELASTICSEARCH_INDEX_ALIAS', '');

        if ($indexAlias) {
            $aliasInfo = $this->elasticsearchClient->indices()->getAlias([
                'index' => '*',
                'name' => $indexAlias,
            ]);

            $this->index = ($aliasInfo) ? current(array_keys($aliasInfo)) : $defaultIndex;
        } else {
            $this->index = $defaultIndex;
        }
    }

    /**
     * Load the analytics data for the Videos tab.
     *
     * @param array $filters - Filters in the formats as they were set to be used internally.
     * @return array
     */
    public function loadVideosData(array $filters)
    {
        $filters = $this->setFilters($filters);

        $this->setExtendedBoundsForType($filters['from'], $filters['to']);
        $this->setQueryRange($filters['from'], $filters['to']);

        if ($this->categoryIdsFilter) {
            $this->contentIdsFilter = $this->getVideoIdsByFieldFilter('categories', 'categories.id', $this->categoryIdsFilter);

            if (!$this->contentIdsFilter) {
                // Selected categories have no content in them, so no need to look further.
                return null;
            }
        } else {
            if ($this->channelIdFilter) {
                $this->contentIdsFilter = $this->getVideoIdsByFieldFilter('channels', 'channels.id', [$this->channelIdFilter]);

                if (!$this->contentIdsFilter) {
                    // Selected channel has no content in it, so no need to look further.
                    return null;
                }
            }
        }

        $data = [
            'timeUnit' => $this->queryInterval,
        ];

        if ($this->contentIdFilter) {
            $data['videoPopularity'] = $this->videoPopularityHistogram();
            $data['uniqueViews'] = $this->uniqueViewsHistogram();
            $data['viewerEngagement'] = $this->viewerEngagementData();
            $data['avgPlayRate'] = $this->avgPlayRate('dark');
            $data['avgViewRate'] = $this->avgViewRate('dark');
            $data['viewTime'] = $this->viewTimeHistogram();
        } elseif ($this->channelIdFilter) {
            $data['channelPopularity'] = $this->channelPopularityHistogram();
            $data['subscriptions'] = $this->subscriptionsData();
            $data['pushedVideos'] = $this->pushedVideosHistogram();
        } else {
            if ($filters['selectedContentType'] === 'channel') {
                $data['popularChannels'] = $this->popularChannels();
                $data['subscriptions'] = $this->subscriptionsData();
                $data['pushedVideos'] = $this->pushedVideosHistogram();
            } else {
                $data['popularVideos'] = $this->popularVideos();
                $data['viewerEngagement'] = $this->viewerEngagementData();
                $data['avgPlayRate'] = $this->avgPlayRate('light');
                $data['avgViewRate'] = $this->avgViewRate('light');
                $data['viewTime'] = $this->viewTimeHistogram();
            }
        }

        return $data;
    }

    /**
     * Load the analytics data for the Advertising tab.
     *
     * @param array $filters - Filters in the formats as they were set to be used internally.
     * @return array
     */
    public function loadAdsData(array $filters)
    {
        $filters = $this->setFilters($filters);

        $this->setExtendedBoundsForType($filters['from'], $filters['to']);
        $this->setQueryRange($filters['from'], $filters['to']);

        if ($this->categoryIdsFilter) {
            $this->contentIdsFilter = $this->getVideoIdsByFieldFilter(
                'categories',
                'categories.id',
                $this->categoryIdsFilter,
                false
            );
        } else {
            if ($categoryIds = $this->getCategoryIdsFromAds()) {
                $this->contentIdsFilter = $this->getVideoIdsByFieldFilter(
                    'categories',
                    'categories.id',
                    $categoryIds,
                    false
                );
            }
        }

        if (!$this->contentIdsFilter) {
            // No associated videos found, so no need to look further.
            return null;
        }

        $this->adIdsFilter = ($this->adIdFilter) ? [$this->adIdFilter] : $this->getAdIds();

        $data = [
            'timeUnit' => $this->queryInterval,
        ];

        if ($this->adIdFilter) {
            $data['consumerEngagement'] = $this->consumerEngagementData();
        } else {
            if ($filters['selectedSearchType'] === 'cat') {
                $data['popularAds'] = $this->popularAds();
                $data['consumerEngagement'] = $this->consumerEngagementData();
            } else {
                if ($filters['selectedAdType'] === Ad::TYPE_BANNER) {
                    $data['popularVideos'] = [
                        'title' => __('misc.title.banner-ads'),
                        'icon' => 'icon-banner',
                    ];
                } elseif ($filters['selectedAdType'] === Ad::TYPE_VIDEO) {
                    $data['popularVideos'] = [
                        'title' => __('misc.title.video-ads'),
                        'icon' => 'icon-videos',
                    ];
                } else {
                    $data['popularVideos'] = [
                        'title' => __('misc.title.ads'),
                        'icon' => 'icon-megaphone',
                    ];
                }

                $popularVideos = $this->popularVideos();
                $data['popularVideos']['list'] = $popularVideos['list'] ?? [];
                $data['popularVideos']['total'] = count($this->adIdsFilter);
                $data['pushedAds'] = $this->pushedVideosHistogram(true);
                $data['consumerEngagement'] = $this->consumerEngagementData();
            }
        }

        return $data;
    }

    /**
     * Set all the filters to be used in the queries.
     *
     * @param array $filters - Filters provided in the request from the browser.
     * @return array
     */
    protected function setFilters(array $filters)
    {
        $this->from = $filters['dateFrom'];
        $this->to = $filters['dateTo'];
        $this->tzOffsetInMins = $filters['tzOffset'];
        $this->userIdFilter = (isset($filters['selectedUser']) && $filters['selectedUser'])
            ? $filters['selectedUser']
            : null;
        $this->channelIdFilter = (isset($filters['selectedChannel']) && $filters['selectedChannel'])
            ? $filters['selectedChannel']
            : null;
        $this->contentIdFilter = (isset($filters['selectedVideo']) && $filters['selectedVideo'])
            ? $filters['selectedVideo']
            : null;
        $this->adIdFilter = (isset($filters['selectedAd']) && $filters['selectedAd'])
            ? $filters['selectedAd']
            : null;
        $this->adTypeFilter =
            (isset($filters['selectedAdType']) && in_array($filters['selectedAdType'], [Ad::TYPE_BANNER, Ad::TYPE_VIDEO]))
            ? $filters['selectedAdType']
            : null;
        $this->categoryIdsFilter = (isset($filters['selectedCategories']) && $filters['selectedCategories'])
            ? $filters['selectedCategories']
            : null;

        $filters['from'] = $this->convertDateFilterStringForQuery($filters['dateFrom']);
        $filters['to'] = $this->convertDateFilterStringForQuery($filters['dateTo']);

        $this->setTimezoneHmOffset($filters['tzOffset']);
        $this->setQueryInterval();

        return $filters;
    }

    /**
     * Set timezone offset filter in "+01:00" format (for the range queries).
     *
     * @param int $tzOffset - Timezone offset as provided by the user's browser.
     */
    protected function setTimezoneHmOffset($tzOffset)
    {
        $hm = date('H:i', mktime(0, abs($tzOffset)));
        // We need to negate the sign here, because timezone offset coming from the browser looks like -360 when we need
        // it to be +06:00 and vice versa.
        $this->tzOffsetHm = ($tzOffset < 0) ? '+' . $hm : '-' . $hm;
    }

    /**
     * Convert the date into the format to be used in the queries to Elasticsearch.
     *
     * @param string $date - Date string as it comes in the request from the browser.
     * @return string|static
     */
    protected function convertDateFilterStringForQuery($date)
    {
        if ($date) {
            $date = Carbon::createFromFormat(self::INCOMING_DATE_FORMAT, $date);

            return $date->toDateString();
        }

        return $date;
    }

    /**
     * Set query interval for Elasticsearch queries based on from-to filter range.
     */
    protected function setQueryInterval()
    {
        if ($this->from && $this->to) {
            $from = Carbon::createFromFormat(self::INCOMING_DATE_FORMAT, $this->from);
            $to = Carbon::createFromFormat(self::INCOMING_DATE_FORMAT, $this->to);
            $diffInDays = $to->diffInDays($from);

            if ($diffInDays < 3) {
                $this->queryInterval = 'hour';
            } elseif ($diffInDays >= 3 && $diffInDays <= 60) {
                $this->queryInterval = 'day';
            } elseif ($diffInDays > 60 && $diffInDays <= 180) {
                $this->queryInterval = 'week';
            } else {
                $this->queryInterval = 'month';
            }
        } else {
            $this->queryInterval = 'day';
        }
    }

    /**
     * Set the date filters to be applied to the Elasticsearch range queries.
     *
     * @param string $from - Date filter converted for the Elasticsearch query.
     * @param string $to - Date filter converted for the Elasticsearch query.
     */
    protected function setQueryRange($from, $to)
    {
        $from = Carbon::createFromFormat(self::INCOMING_DATE_FORMAT, $from);
        $to = Carbon::createFromFormat(self::INCOMING_DATE_FORMAT, $to);
        $diffInDays = $to->diffInDays($from);
        $this->queryRange = [
            'from' => $from->startOfDay()->toDateTimeString(),
            'to' => $to->endOfDay()->toDateTimeString(),
            'prevFrom' => $from->subDays($diffInDays + 1)->startOfDay()->toDateTimeString(),
            'prevTo' => $to->subDays($diffInDays + 1)->endOfDay()->toDateTimeString(),
        ];
    }

    /**
     * Sets the date limits that are applied to the Elasticsearch queries as boundaries so that we get the data only
     * within those limits (NB: those are not filters).
     * @see https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-histogram-aggregation.html#search-aggregations-bucket-histogram-aggregation-extended-bounds
     *
     * @param $from
     * @param $to
     */
    protected function setExtendedBoundsForType($from, $to)
    {
        $this->extendedBounds = [
            'min' => Carbon::createFromFormat(self::INCOMING_DATE_FORMAT, $from)->startOfDay()->timestamp * 1000,
            'max' => Carbon::createFromFormat(self::INCOMING_DATE_FORMAT, $to)->endOfDay()->timestamp * 1000,
        ];
    }

    protected function viewerEngagementData()
    {
        $views = $this->viewsHistogram();
        $pushes = $this->pushesHistogram();
        $likes = $this->likesHistogram();

        return [
            'views' => [
                'isHorizontal' => false,
                'displayLegend' => true,
                'title' => __('misc.title.view-count'),
                'total' => $views['total'],
                'data' => [
                    'labels' => isset($views['labels']) ? $views['labels'] : [],
                    'datasets' => [
                        [
                            'label' => 'Stopped',
                            'backgroundColor' => '#e880a4',
                            'data' => isset($views['stopped']) ? $views['stopped']['values'] : [],
                        ],
                        [
                            'label' => 'Watched',
                            'backgroundColor' => '#a31a5c',
                            'data' => isset($views['finished']) ? $views['finished']['values'] : [],
                        ],
                        [
                            'label' => 'Opened',
                            'backgroundColor' => '#d30f54',
                            'data' => isset($views['started']) ? $views['started']['values'] : [],
                        ],
                    ],
                ],
            ],
            'pushes' => [
                'isHorizontal' => false,
                'displayLegend' => true,
                'title' => __('misc.title.pushes'),
                'total' => $pushes['total'],
                'data' => [
                    'labels' => isset($pushes['labels']) ? $pushes['labels'] : [],
                    'datasets' => [
                        [
                            'label' => 'Delivered Pushes',
                            'backgroundColor' => '#d30f54',
                            'data' => isset($pushes['successful']) ? $pushes['successful']['values'] : [],
                        ],
                        [
                            'label' => 'All Pushes',
                            'backgroundColor' => '#a31a5c',
                            'data' => isset($pushes['all']) ? $pushes['all']['values'] : [],
                        ],
                    ],
                ],
            ],
            'likes' => [
                'isHorizontal' => false,
                'displayLegend' => true,
                'title' => __('misc.title.likes'),
                'total' => $likes['total'],
                'data' => [
                    'labels' => isset($likes['labels']) ? $likes['labels'] : [],
                    'datasets' => [
                        [
                            'label' => 'Disliked',
                            'backgroundColor' => '#d30f54',
                            'data' => isset($likes['disliked']) ? $likes['disliked']['values'] : [],
                        ],
                        [
                            'label' => 'Liked',
                            'backgroundColor' => '#a31a5c',
                            'data' => isset($likes['liked']) ? $likes['liked']['values'] : [],
                        ],
                    ],
                ],
            ],
        ];
    }

    protected function consumerEngagementData()
    {
        $views = $this->viewsHistogram(true);
        $pushes = $this->pushesHistogram();
        $viewsStarted = $this->viewStartedHistogram();
        $clicksThrough = $this->adClicksThroughHistogram();
        $adSaves = $this->wishlistsHistogram();

        return [
            'impressions' => [
                'isHorizontal' => false,
                'displayLegend' => false,
                'title' => __('misc.title.impressions'),
                'total' => $views['total'],
                'data' => [
                    'labels' => isset($views['labels']) ? $views['labels'] : [],
                    'datasets' => [
                        [
                            'label' => __('misc.title.impressions'),
                            'backgroundColor' => '#a31a5c',
                            'data' => isset($views['values']) ? $views['values'] : [],
                        ],
                    ],
                ],
            ],
            'ctr' => [
                'isHorizontal' => false,
                'displayLegend' => true,
                'title' => __('misc.title.ctr'),
                'totalRatio' => $viewsStarted['total']
                    ? round($clicksThrough['total'] / $viewsStarted['total'], 2)
                    : null,
                'data' => [
                    'labels' => isset($viewsStarted['labels']) ? $viewsStarted['labels'] : [],
                    'datasets' => [
                        [
                            'label' => __('misc.title.views'),
                            'backgroundColor' => '#d30f54',
                            'data' => isset($viewsStarted['values']) ? $viewsStarted['values'] : [],
                        ],
                        [
                            'label' => __('misc.title.clicks'),
                            'backgroundColor' => '#a31a5c',
                            'data' => isset($clicksThrough['values']) ? $clicksThrough['values'] : [],
                        ],
                    ],
                ],
            ],
            'adSaves' => [
                'isHorizontal' => false,
                'displayLegend' => false,
                'title' => __('misc.title.ad-saves'),
                'total' => $adSaves['total'],
                'data' => [
                    'labels' => isset($adSaves['labels']) ? $adSaves['labels'] : [],
                    'datasets' => [
                        [
                            'label' => __('misc.title.ad-saves'),
                            'backgroundColor' => '#a31a5c',
                            'data' => isset($adSaves['values']) ? $adSaves['values'] : [],
                        ],
                    ],
                ],
            ],
            'pushes' => [
                'isHorizontal' => false,
                'displayLegend' => false,
                'title' => __('misc.title.pushes'),
                'total' => $pushes['total'],
                'data' => [
                    'labels' => isset($pushes['labels']) ? $pushes['labels'] : [],
                    'datasets' => [
                        [
                            'label' => __('misc.title.pushes'),
                            'backgroundColor' => '#a31a5c',
                            'data' => isset($pushes['all']) ? $pushes['all']['values'] : [],
                        ],
                    ],
                ],
            ],
        ];
    }

    protected function subscriptionsData()
    {
        $subs = $this->subsHistogram();

        return [
            'isHorizontal' => false,
            'chartId' => 'subscriptions',
            'data' => [
                'labels' => isset($subs['labels']) ? $subs['labels'] : [],
                'datasets' => [
                    [
                        'label' => __('misc.label.unsubbed'),
                        'backgroundColor' => '#d30f54',
                        'data' => isset($subs['unsubscribed']) ? $subs['unsubscribed']['values'] : [],
                    ],
                    [
                        'label' => __('misc.label.subbed'),
                        'backgroundColor' => '#a31a5c',
                        'data' => isset($subs['subscribed']) ? $subs['subscribed']['values'] : [],
                    ],
                ],
            ],
            'summary' => [
                'title' => __('misc.title.total-subs'),
                'total' => $subs['total'],
            ],
        ];
    }

    protected function popularChannels()
    {
        $data = [
//            'title' => __('misc.title.channels'),
//            'icon' => 'icon-channels',
//            'total' => $this->contentTypeCount(Channel::get()),
        ];
        $type = 'subscription';
        $params = [
            'index' => $this->index,
            'body'  => [
                'size'  => 0,
                'query' => [
                    'bool' => [
                        'filter'  => [
                            'match' => [
                                'type' => $type,
                            ],
                        ],
                        'must' => [
                            [
                                'range' => [
                                    'created_at' => [
                                        'gte' => $this->queryRange['from'],
                                        'lte' => $this->queryRange['to'],
                                        'time_zone' => $this->tzOffsetHm,
                                        'format' => 'yyyy-MM-dd HH:mm:ss',
                                    ],
                                ],
                            ],
                            [
                                'term' => [
                                    'subscribed' => Subscription::SUBSCRIBED,
                                ],
                            ],
                        ],
                    ],
                ],
                'aggs' => [
                    'subs' => [
                        'terms' => [
                            'field' => 'channel_id',
                        ],
                        'aggs' => [
                            'count' => [
                                'value_count' => [
                                    'field' => 'id',
                                ],
                            ],
                            'popularity_sort' => [
                                'bucket_sort' => [
                                    'sort' => [
                                        'count' => ['order' => 'desc'],
                                    ],
                                    'size' => 3,
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $response = $this->elasticsearchClient->search($params);
        $ids = [];
        $subs = [];

        foreach ($response['aggregations']['subs']['buckets'] as $bucket) {
            $ids[] = $bucket['key'];
            $subs[$bucket['key']] = $bucket['count']['value'];
        }

        if ($ids) {
            $channels = $this->getChannelsByIds($ids);
            $ranks = $this->channelsRanking($this->queryRange['prevFrom'], $this->queryRange['prevTo']);

            foreach ($ids as $id) {
                $channel = $channels[$id];
                $channel['subs'] = $subs[$id];

                if ($ranks && isset($ranks[$id])) {
                    $channel['prevPosition'] = $ranks[$id] + 1;
                }

                $data['list'][] = $channel;
            }
        }

        return $data;
    }

    protected function channelPopularityHistogram()
    {
        $data = [];
        $type = 'subscription';
        $params = [
            'index' => $this->index,
            'body'  => [
                'size'  => 0,
                'query' => [
                    'bool' => [
                        'filter'  => [
                            'match' => [
                                'type' => $type,
                            ],
                        ],
                        'must' => [
                            [
                                'range' => [
                                    'created_at' => [
                                        'gte' => $this->queryRange['from'],
                                        'lte' => $this->queryRange['to'],
                                        'time_zone' => $this->tzOffsetHm,
                                        'format' => 'yyyy-MM-dd HH:mm:ss',
                                    ],
                                ],
                            ],
                            [
                                'term' => [
                                    'subscribed' => Subscription::SUBSCRIBED,
                                ],
                            ],
                            [
                                'term' => [
                                    'channel_id' => $this->channelIdFilter,
                                ],
                            ],
                        ],
                    ],
                ],
                'aggs' => [
                    'subs' => [
                        'date_histogram' => [
                            'field' => 'created_at',
                            'interval' => $this->queryInterval,
                            'time_zone' => $this->tzOffsetHm,
                            'extended_bounds' => [
                                'min' => $this->extendedBounds['min'],
                                'max' => $this->extendedBounds['max'],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $response = $this->elasticsearchClient->search($params);

        foreach ($response['aggregations']['subs']['buckets'] as $bucket) {
            $data['labels'][] = $bucket['key'];
            $data['values'][] = $bucket['doc_count'];
        }

        $output = [
            'title' => __('misc.title.popularity'),
            'data' => [
                'labels' => isset($data['labels']) ? $data['labels'] : [],
                'datasets' => [
                    [
                        'data' => isset($data['values']) ? $data['values'] : [],
                    ],
                ],
            ],
        ];

        if ($ranksCurrent = $this->channelsRanking($this->queryRange['from'], $this->queryRange['to'])) {
            if (isset($ranksCurrent[$this->channelIdFilter])) {
                $output['position'] = $ranksCurrent[$this->channelIdFilter] + 1;
            }
        }
        if ($ranksPrevious = $this->channelsRanking($this->queryRange['prevFrom'], $this->queryRange['prevTo'])) {
            if (isset($ranksPrevious[$this->channelIdFilter])) {
                $output['prevPosition'] = $ranksPrevious[$this->channelIdFilter] + 1;
            }
        }

        return $output;
    }

    protected function channelsRanking(string $from, string $to)
    {
        $type = 'subscription';
        $params = [
            'index' => $this->index,
            'body'  => [
                'size'  => 0,
                'query' => [
                    'bool' => [
                        'filter'  => [
                            'match' => [
                                'type' => $type,
                            ],
                        ],
                        'must' => [
                            [
                                'range' => [
                                    'created_at' => [
                                        'gte' => $from,
                                        'lte' => $to,
                                        'time_zone' => $this->tzOffsetHm,
                                        'format' => 'yyyy-MM-dd HH:mm:ss',
                                    ],
                                ],
                            ],
                            [
                                'term' => [
                                    'subscribed' => Subscription::SUBSCRIBED,
                                ],
                            ],
                        ],
                    ],
                ],
                'aggs' => [
                    'subs' => [
                        'terms' => [
                            'field' => 'channel_id',
                        ],
                        'aggs' => [
                            'count' => [
                                'value_count' => [
                                    'field' => 'id',
                                ],
                            ],
                            'popularity_sort' => [
                                'bucket_sort' => [
                                    'sort' => [
                                        'count' => ['order' => 'desc'],
                                    ],
                                    'size' => 10000,
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $response = $this->elasticsearchClient->search($params);
        $ids = [];

        foreach ($response['aggregations']['subs']['buckets'] as $bucket) {
            $ids[] = $bucket['key'];
        }

        if ($ids) {
            return array_flip($ids);
        }

        return null;
    }

    protected function popularVideos()
    {
        $data = [
//            'title' => __('misc.title.videos'),
//            'icon' => 'icon-videos',
//            'total' => $this->contentIdsFilter ? count($this->contentIdsFilter) : $this->contentTypeCount(Content::get()),
        ];
        $viewFinished = ViewType::where('name', ViewType::TYPE_FINISHED)->value('id');

        $params = [
            'index' => $this->index,
            'body'  => [
                'size'  => 0,
                'query' => [
                    'bool' => [
                        'filter'  => [
                            'terms' => [
                                'type' => ['view', 'opinion'],
                            ],
                        ],
                        'must' => [
                            [
                                'range' => [
                                    'created_at' => [
                                        'gte' => $this->queryRange['from'],
                                        'lte' => $this->queryRange['to'],
                                        'time_zone' => $this->tzOffsetHm,
                                        'format' => 'yyyy-MM-dd HH:mm:ss',
                                    ],
                                ],
                            ],
                        ],
                        'should' => [
                            [
                                'term' => [
                                    'view_type_id' => $viewFinished,
                                ],
                            ],
                            [
                                'term' => [
                                    'like' => Opinion::LIKE,
                                ],
                            ],
                        ],
                        'minimum_should_match' => 1,
                    ],
                ],
                'aggs' => [
                    'videos' => [
                        'terms' => [
                            'field' => 'content_id',
                            'size' => 100000,
                        ],
                        'aggs' => [
                            'data' => [
                                'value_count' => [
                                    'field' => 'id',
                                ],
                            ],
                            'popularity_sort' => [
                                'bucket_sort' => [
                                    'sort' => [
                                        'data' => ['order' => 'desc'],
                                    ],
                                    'size' => 3,
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $this->applyFilters($params);

        $response = $this->elasticsearchClient->search($params);
        $ids = [];

        foreach ($response['aggregations']['videos']['buckets'] as $bucket) {
            $ids[] = $bucket['key'];
        }

        if ($ids) {
            $videos = $this->getVideosByIds($ids);
            $views = $this->viewCountsByVideos($ids, $viewFinished);
            $likes = $this->likeCountsByVideos($ids);
            $ranks = $this->videosRanking($this->queryRange['prevFrom'], $this->queryRange['prevTo'], $viewFinished);

            foreach ($ids as $id) {
                $video = $videos[$id];
                $video['views'] = (isset($views[$id])) ? $views[$id] : 0;
                $video['likes'] = (isset($likes[$id])) ? $likes[$id] : 0;

                if ($ranks && isset($ranks[$id])) {
                    $video['prevPosition'] = $ranks[$id] + 1;
                }

                $data['list'][] = $video;
            }
        }

        return $data;
    }

    protected function videosRanking(string $from, string $to, int $viewTypeId, bool $withCounts = false)
    {
        $params = [
            'index' => $this->index,
            'body'  => [
                'size'  => 0,
                'query' => [
                    'bool' => [
                        'filter'  => [
                            'terms' => [
                                'type' => ['view', 'opinion'],
                            ],
                        ],
                        'must' => [
                            [
                                'range' => [
                                    'created_at' => [
                                        'gte' => $from,
                                        'lte' => $to,
                                        'time_zone' => $this->tzOffsetHm,
                                        'format' => 'yyyy-MM-dd HH:mm:ss',
                                    ],
                                ],
                            ],
                        ],
                        'should' => [
                            [
                                'term' => [
                                    'view_type_id' => $viewTypeId,
                                ],
                            ],
                            [
                                'term' => [
                                    'like' => Opinion::LIKE,
                                ],
                            ],
                        ],
                        'minimum_should_match' => 1,
                    ],
                ],
                'aggs' => [
                    'videos' => [
                        'terms' => [
                            'field' => 'content_id',
                            'size' => 100000,
                        ],
                        'aggs' => [
                            'data' => [
                                'value_count' => [
                                    'field' => 'id',
                                ],
                            ],
                            'popularity_sort' => [
                                'bucket_sort' => [
                                    'sort' => [
                                        'data' => ['order' => 'desc'],
                                    ],
                                    'size' => 100000,
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        if ($this->userIdFilter) {
            $params['body']['query']['bool']['must'][] = [
                'term' => [
                    'user_id' => $this->userIdFilter,
                ],
            ];
        }

        $response = $this->elasticsearchClient->search($params);
        $ids = [];

        foreach ($response['aggregations']['videos']['buckets'] as $bucket) {
            if ($withCounts) {
                $ids[$bucket['key']] = $bucket['doc_count'];
            } else {
                $ids[] = $bucket['key'];
            }
        }

        if ($ids) {
            return $withCounts ? $ids : array_flip($ids);
        }

        return null;
    }

    protected function popularAds()
    {
        $popularAds = [];

        // Get info about the ads associated with the given videos.
        $adsByVideos = $this->getAdIdsByVideoIds($this->contentIdsFilter);

        // Get the numbers of views, likes and clicks for the given videos.
        $viewFinished = ViewType::where('name', ViewType::TYPE_FINISHED)->value('id');
        $views = $this->viewCountsByVideos($this->contentIdsFilter, $viewFinished);
        $likes = $this->likeCountsByVideos($this->contentIdsFilter);
        $clicks = $this->adClicksThroughByAds();

        // Calculate the overall popularity for the ads.
        $adsPopularity = [];
        if ($adsByVideos) {
            foreach ($adsByVideos as $videoId => $adIds) {
                $v = isset($views[$videoId]) ? $views[$videoId] : 0;
                $l = isset($likes[$videoId]) ? $likes[$videoId] : 0;

                foreach ($adIds as $adId) {
                    $c = isset($clicks[$adId]) ? $clicks[$adId] : 0;

                    if (!isset($adsPopularity[$adId])) {
                        $adsPopularity[$adId] = [
                            'views' => $v,
                            'likes' => $l,
                            'clicks' => $c,
                            'total' => $v + $l + $c, // Clicks are added only once.
                        ];
                    } else {
                        $adsPopularity[$adId]['views'] += $v;
                        $adsPopularity[$adId]['likes'] += $l;
                        $adsPopularity[$adId]['total'] += $v + $l;
                    }
                }
            }
        }

        // Determine the most popular ads.
        if ($count = count($adsPopularity)) {
            if ($count > 1) {
                uasort($adsPopularity, function($a, $b) {
                    return $a['total'] < $b['total'];
                });
            }

            $top = ($count > 3) ? array_slice($adsPopularity, 0, 3, true) : $adsPopularity;
            $topAdIds = array_keys($top);
            $ads = $this->getAdsByIds($topAdIds);

            foreach ($topAdIds as $topAdId) {
                $ad = $ads[$topAdId];
                $ad['views'] = $adsPopularity[$topAdId]['views'];
                $ad['likes'] = $adsPopularity[$topAdId]['likes'];
                $ad['clicks'] = $adsPopularity[$topAdId]['clicks'];
                $popularAds[] = $ad;
            }
        }

        return $popularAds;
    }

    protected function viewCountsByVideos(array $videoIds, int $viewTypeId)
    {
        $data = [];
        $type = 'view';
        $params = [
            'index' => $this->index,
            'body'  => [
                'size'  => 0,
                'query' => [
                    'bool' => [
                        'filter'  => [
                            'match' => [
                                'type' => $type,
                            ],
                        ],
                        'must' => [
                            [
                                'range' => [
                                    'created_at' => [
                                        'gte' => $this->queryRange['from'],
                                        'lte' => $this->queryRange['to'],
                                        'time_zone' => $this->tzOffsetHm,
                                        'format' => 'yyyy-MM-dd HH:mm:ss',
                                    ],
                                ],
                            ],
                            [
                                'terms' => [
                                    'content_id' => $videoIds,
                                ],
                            ],
                            [
                                'term' => [
                                    'view_type_id' => $viewTypeId,
                                ],
                            ],
                        ],
                    ],
                ],
                'aggs' => [
                    'views' => [
                        'terms' => [
                            'field' => 'content_id',
                        ],
                    ],
                ],
            ],
        ];

        $response = $this->elasticsearchClient->search($params);

        foreach ($response['aggregations']['views']['buckets'] as $bucket) {
            $data[$bucket['key']] = $bucket['doc_count'];
        }

        return $data;
    }

    protected function likeCountsByVideos(array $videoIds)
    {
        $data = [];
        $type = 'opinion';
        $params = [
            'index' => $this->index,
            'body'  => [
                'size'  => 0,
                'query' => [
                    'bool' => [
                        'filter'  => [
                            'match' => [
                                'type' => $type,
                            ],
                        ],
                        'must' => [
                            [
                                'range' => [
                                    'created_at' => [
                                        'gte' => $this->queryRange['from'],
                                        'lte' => $this->queryRange['to'],
                                        'time_zone' => $this->tzOffsetHm,
                                        'format' => 'yyyy-MM-dd HH:mm:ss',
                                    ],
                                ],
                            ],
                            [
                                'terms' => [
                                    'content_id' => $videoIds,
                                ],
                            ],
                            [
                                'term' => [
                                    'like' => Opinion::LIKE,
                                ],
                            ],
                        ],
                    ],
                ],
                'aggs' => [
                    'likes' => [
                        'terms' => [
                            'field' => 'content_id',
                        ],
                    ],
                ],
            ],
        ];

        $response = $this->elasticsearchClient->search($params);

        foreach ($response['aggregations']['likes']['buckets'] as $bucket) {
            $data[$bucket['key']] = $bucket['doc_count'];
        }

        return $data;
    }

    protected function pushedVideosHistogram(bool $withAdsOnly = false)
    {
        $data = ['total' => 0];
        $type = 'push';
        $params = [
            'index' => $this->index,
            'body'  => [
                'size'  => 0,
                'query' => [
                    'bool' => [
                        'filter'  => [
                            'match' => [
                                'type' => $type,
                            ],
                        ],
                        'must' => [
                            [
                                'term' => [
                                    'pushed' => Push::PUSHED,
                                ],
                            ],
                            [
                                'range' => [
                                    'created_at' => [
                                        'gte' => $this->queryRange['from'],
                                        'lte' => $this->queryRange['to'],
                                        'time_zone' => $this->tzOffsetHm,
                                        'format' => 'yyyy-MM-dd HH:mm:ss',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
                'aggs' => [
                    'pushes' => [
                        'date_histogram' => [
                            'field' => 'created_at',
                            'interval' => $this->queryInterval,
                            'time_zone' => $this->tzOffsetHm,
                            'extended_bounds' => [
                                'min' => $this->extendedBounds['min'],
                                'max' => $this->extendedBounds['max'],
                            ],
                        ],
                        'aggs' => [
                            'unique' => [
                                'terms' => [
                                    'field' => 'content_id',
                                    'size' => 100000,
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $this->applyFilters($params);

        if ($withAdsOnly) {
            $params['body']['query']['bool']['must'][] = [
                'exists' => [
                    'field' => 'ad_id',
                ],
            ];
        }

        $response = $this->elasticsearchClient->search($params);

        foreach ($response['aggregations']['pushes']['buckets'] as $bucket) {
            $data['labels'][] = $bucket['key'];
            $count = count($bucket['unique']['buckets']);
            $data['values'][] = $count;
            $data['total'] += $count;
        }

        return [
            'isHorizontal' => false,
            'chartId' => $withAdsOnly ? 'pushed-ads' : 'pushed-videos',
            'data' => [
                'labels' => isset($data['labels']) ? $data['labels'] : [],
                'datasets' => [
                    [
                        'label' => __('misc.label.pushed'),
                        'backgroundColor' => '#d30f54',
                        'data' => isset($data['values']) ? $data['values'] : [],
                    ],
                ],
            ],
            'summary' => [
                'title' => $withAdsOnly ? __('misc.title.total-ads') : __('misc.title.total-videos'),
                'total' => $data['total'],
            ],
        ];
    }

    protected function avgPlayRate(string $theme)
    {
        if ($loaded = $this->getViewCountByType(ViewType::TYPE_LOADED)) {
            $started = $this->getViewCountByType(ViewType::TYPE_STARTED);

            return [
                'value' => round($started / $loaded, 2),
                'theme' => $theme,
            ];
        }

        return 0;
    }

    protected function avgViewRate(string $theme)
    {
        if ($started = $this->getViewCountByType(ViewType::TYPE_STARTED)) {
            $finished = $this->getViewCountByType(ViewType::TYPE_FINISHED);

            return [
                'value' => round($finished / $started, 2),
                'theme' => $theme,
            ];
        }

        return 0;
    }

    protected function getViewCountByType(string $viewType)
    {
        $query = View::join('view_types AS vt', 'views.view_type_id', '=', 'vt.id')
            ->where('vt.name', $viewType);

        if ($this->contentIdFilter) {
            $query->where('views.content_id', $this->contentIdFilter);
        } else {
            $query->join('contents AS c', 'views.content_id', '=', 'c.id');

            if ($this->contentIdsFilter) {
                $query->whereIn('views.content_id', $this->contentIdsFilter);
            } else {
                if ($this->userIdFilter) {
                    $query->where('c.user_id', $this->userIdFilter);
                }
            }
        }

        return $query->count();
    }

    protected function videoPopularityHistogram()
    {
        $data = [];
        $viewFinished = ViewType::where('name', ViewType::TYPE_FINISHED)->value('id');

        $params = [
            'index' => $this->index,
            'body'  => [
                'size'  => 0,
                'query' => [
                    'bool' => [
                        'filter'  => [
                            'terms' => [
                                'type' => ['view', 'opinion'],
                            ],
                        ],
                        'must' => [
                            [
                                'range' => [
                                    'created_at' => [
                                        'gte' => $this->queryRange['from'],
                                        'lte' => $this->queryRange['to'],
                                        'time_zone' => $this->tzOffsetHm,
                                        'format' => 'yyyy-MM-dd HH:mm:ss',
                                    ],
                                ],
                            ],
                        ],
                        'should' => [
                            [
                                'term' => [
                                    'view_type_id' => $viewFinished,
                                ],
                            ],
                            [
                                'term' => [
                                    'like' => Opinion::LIKE,
                                ],
                            ],
                        ],
                        'minimum_should_match' => 1,
                    ],
                ],
                'aggs' => [
                    'views' => [
                        'date_histogram' => [
                            'field' => 'created_at',
                            'interval' => $this->queryInterval,
                            'time_zone' => $this->tzOffsetHm,
                            'extended_bounds' => [
                                'min' => $this->extendedBounds['min'],
                                'max' => $this->extendedBounds['max'],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $this->applyFilters($params);

        $response = $this->elasticsearchClient->search($params);

        foreach ($response['aggregations']['views']['buckets'] as $bucket) {
            $data['labels'][] = $bucket['key'];
            $data['values'][] = $bucket['doc_count'];
        }

        $output = [
            'title' => __('misc.title.popularity'),
            'data' => [
                'labels' => isset($data['labels']) ? $data['labels'] : [],
                'datasets' => [
                    [
                        'data' => isset($data['values']) ? $data['values'] : [],
                    ],
                ],
            ],
        ];

        if ($ranksCurrent = $this->videosRanking($this->queryRange['from'], $this->queryRange['to'], $viewFinished)) {
            if (isset($ranksCurrent[$this->contentIdFilter])) {
                $output['position'] = $ranksCurrent[$this->contentIdFilter] + 1;
            }
        }
        if ($ranksPrevious = $this->videosRanking($this->queryRange['prevFrom'], $this->queryRange['prevTo'], $viewFinished)) {
            if (isset($ranksPrevious[$this->contentIdFilter])) {
                $output['prevPosition'] = $ranksPrevious[$this->contentIdFilter] + 1;
            }
        }

        return $output;
    }

    protected function uniqueViewsHistogram()
    {
        $data = ['total' => 0];
        $type = 'view';

        $finishedViewTypeId = ViewType::where('name', ViewType::TYPE_FINISHED)->value('id');

        $params = [
            'index' => $this->index,
            'body'  => [
                'size'  => 0,
                'query' => [
                    'bool' => [
                        'filter'  => [
                            'match' => [
                                'type' => $type,
                            ],
                        ],
                        'must' => [
                            [
                                'term' => [
                                    'view_type_id' => $finishedViewTypeId,
                                ],
                            ],
                            [
                                'range' => [
                                    'created_at' => [
                                        'gte' => $this->queryRange['from'],
                                        'lte' => $this->queryRange['to'],
                                        'time_zone' => $this->tzOffsetHm,
                                        'format' => 'yyyy-MM-dd HH:mm:ss',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
                'aggs' => [
                    'views' => [
                        'date_histogram' => [
                            'field' => 'created_at',
                            'interval' => $this->queryInterval,
                            'time_zone' => $this->tzOffsetHm,
                            'extended_bounds' => [
                                'min' => $this->extendedBounds['min'],
                                'max' => $this->extendedBounds['max'],
                            ],
                        ],
                        'aggs' => [
                            'unique' => [
                                'terms' => [
                                    'field' => 'device_id',
                                    'size' => 100000,
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $this->applyFilters($params);

        $response = $this->elasticsearchClient->search($params);

        foreach ($response['aggregations']['views']['buckets'] as $bucket) {
            $data['labels'][] = $bucket['key'];
            $count = count($bucket['unique']['buckets']);
            $data['values'][] = $count;
            $data['total'] += $count;
        }

        return [
            'title' => __('misc.title.unique-views'),
            'total' => $data['total'],
            'data' => [
                'labels' => isset($data['labels']) ? $data['labels'] : [],
                'datasets' => [
                    [
                        'data' => isset($data['values']) ? $data['values'] : [],
                    ],
                ],
            ],
        ];
    }

    protected function viewTimeHistogram()
    {
        $data = ['total' => 0];
        $type = 'view';

        $params = [
            'index' => $this->index,
            'body'  => [
                'size'  => 0,
                'query' => [
                    'bool' => [
                        'filter'  => [
                            'match' => [
                                'type' => $type,
                            ],
                        ],
                        'must' => [
                            [
                                'exists' => [
                                    'field' => 'duration',
                                ],
                            ],
                            [
                                'range' => [
                                    'created_at' => [
                                        'gte' => $this->queryRange['from'],
                                        'lte' => $this->queryRange['to'],
                                        'time_zone' => $this->tzOffsetHm,
                                        'format' => 'yyyy-MM-dd HH:mm:ss',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
                'aggs' => [
                    'views' => [
                        'date_histogram' => [
                            'field' => 'created_at',
                            'interval' => $this->queryInterval,
                            'time_zone' => $this->tzOffsetHm,
                            'extended_bounds' => [
                                'min' => $this->extendedBounds['min'],
                                'max' => $this->extendedBounds['max'],
                            ],
                        ],
                        'aggs' => [
                            'duration' => [
                                'sum' => [
                                    'field' => 'duration',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $this->applyFilters($params);

        $response = $this->elasticsearchClient->search($params);

        foreach ($response['aggregations']['views']['buckets'] as $bucket) {
            $data['labels'][] = $bucket['key'];
            $data['values'][] = round($bucket['duration']['value'] / 3600, 2);
            $data['total'] += (int) $bucket['duration']['value'];
        }

        return [
            'title' => __('misc.title.total-time'),
            'total' => round($data['total'] / 3600, 2),
            'data' => [
                'labels' => isset($data['labels']) ? $data['labels'] : [],
                'datasets' => [
                    [
                        'data' => isset($data['values']) ? $data['values'] : [],
                    ],
                ],
            ],
        ];
    }

    protected function viewsHistogram(bool $combined = false)
    {
        $viewTypes = $this->getViewTypes();
        $data = ['total' => 0];
        $params = $this->dateHistogramAggParams('view', 'view_type', 'view_type_id');
        $response = $this->elasticsearchClient->search($params);
        $bucketsByType = $response['aggregations']['view_type']['buckets'];

        foreach ($bucketsByType as $typeBucket) {
            if ($viewTypes[$typeBucket['key']] !== ViewType::TYPE_WATCH_LATER) {
                if ($combined) {
                    foreach ($typeBucket['data']['buckets'] as $key => $bucket) {
                        if (!isset($data['labels'][$key])) {
                            $data['labels'][$key] = $bucket['key'];
                        }

                        if (!isset($data['values'][$key])) {
                            $data['values'][$key] = $bucket['doc_count'];
                        } else {
                            $data['values'][$key] += $bucket['doc_count'];
                        }

                        if ($viewTypes[$typeBucket['key']] !== ViewType::TYPE_LOADED) {
                            $data['total'] += $bucket['doc_count'];
                        }
                    }
                } else {
                    foreach ($typeBucket['data']['buckets'] as $key => $bucket) {
                        if (!isset($data['labels'][$key])) {
                            $data['labels'][$key] = $bucket['key'];
                        }

                        $data[$viewTypes[$typeBucket['key']]]['values'][] = $bucket['doc_count'];

                        if ($viewTypes[$typeBucket['key']] === ViewType::TYPE_FINISHED) {
                            $data['total'] += $bucket['doc_count'];
                        }
                    }
                }
            }
        }

        return $data;
    }

    protected function pushesHistogram()
    {
        $data = ['total' => 0];
        $params = $this->dateHistogramAggParams('push', 'status', 'pushed');
        $response = $this->elasticsearchClient->search($params);
        $bucketsByStatus = $response['aggregations']['status']['buckets'];

        foreach ($bucketsByStatus as $status => $statusBucket) {
            foreach ($statusBucket['data']['buckets'] as $key => $bucket) {
                if (!isset($data['labels'][$key])) {
                    $data['labels'][$key] = $bucket['key'];
                }

                $data['total'] += $bucket['doc_count'];

                if ($status === Push::PUSHED) {
                    $data['successful']['values'][] = $bucket['doc_count'];
                }

                if (!isset($data['all']['values'][$key])) {
                    $data['all']['values'][$key] = $bucket['doc_count'];
                } else {
                    $data['all']['values'][$key] += $bucket['doc_count'];
                }
            }
        }

        return $data;
    }

    protected function likesHistogram()
    {
        $types = [
            0 => 'disliked',
            1 => 'liked',
        ];
        $data = ['total' => 0];
        $params = $this->dateHistogramAggParams('opinion', 'type', 'like');
        $response = $this->elasticsearchClient->search($params);
        $bucketsByType = $response['aggregations']['type']['buckets'];

        foreach ($bucketsByType as $typeBucket) {
            foreach ($typeBucket['data']['buckets'] as $key => $bucket) {
                if (!isset($data['labels'][$key])) {
                    $data['labels'][$key] = $bucket['key'];
                }

                $data[$types[$typeBucket['key']]]['values'][] = $bucket['doc_count'];

                if ($typeBucket['key'] === Opinion::LIKE) {
                    $data['total'] += $bucket['doc_count'];
                }
            }
        }

        return $data;
    }

    protected function subsHistogram()
    {
        $types = [
            0 => 'unsubscribed',
            1 => 'subscribed',
        ];
        $data = ['total' => 0];
        $params = $this->dateHistogramAggParams('subscription', 'type', 'subscribed');
        $response = $this->elasticsearchClient->search($params);
        $bucketsByType = $response['aggregations']['type']['buckets'];

        foreach ($bucketsByType as $typeBucket) {
            foreach ($typeBucket['data']['buckets'] as $key => $bucket) {
                if (!isset($data['labels'][$key])) {
                    $data['labels'][$key] = $bucket['key'];
                }

                $data[$types[$typeBucket['key']]]['values'][] = $bucket['doc_count'];

                if ($typeBucket['key'] === Subscription::SUBSCRIBED) {
                    $data['total'] += $bucket['doc_count'];
                }
            }
        }

        return $data;
    }

    protected function viewStartedHistogram()
    {
        $viewStarted = ViewType::where('name', ViewType::TYPE_STARTED)->value('id');
        $data = ['total' => 0];
        $params = [
            'index' => $this->index,
            'body'  => [
                'size'  => 0,
                'query' => [
                    'bool' => [
                        'filter'  => [
                            'match' => [
                                'type' => 'view',
                            ],
                        ],
                        'must' => [
                            [
                                'range' => [
                                    'created_at' => [
                                        'gte' => $this->queryRange['from'],
                                        'lte' => $this->queryRange['to'],
                                        'time_zone' => $this->tzOffsetHm,
                                        'format' => 'yyyy-MM-dd HH:mm:ss',
                                    ],
                                ],
                            ],
                            [
                                'term' => [
                                    'view_type_id' => $viewStarted,
                                ],
                            ],
                        ],
                    ],
                ],
                'aggs' => [
                    'views' => [
                        'date_histogram' => [
                            'field' => 'created_at',
                            'interval' => $this->queryInterval,
                            'time_zone' => $this->tzOffsetHm,
                            'extended_bounds' => [
                                'min' => $this->extendedBounds['min'],
                                'max' => $this->extendedBounds['max'],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $response = $this->elasticsearchClient->search($params);
        $buckets = $response['aggregations']['views']['buckets'];

        foreach ($buckets as $key => $bucket) {
            $data['labels'][] = $bucket['key'];
            $data['values'][] = $bucket['doc_count'];
            $data['total'] += $bucket['doc_count'];
        }

        return $data;
    }

    protected function adClicksThroughHistogram()
    {
        $clickThroughTypeId = AdClickType::where('key', AdClickType::TYPE_CLICK)->value('id');
        $data = ['total' => 0];
        $params = [
            'index' => $this->index,
            'body'  => [
                'size'  => 0,
                'query' => [
                    'bool' => [
                        'filter'  => [
                            'match' => [
                                'type' => 'ad_click',
                            ],
                        ],
                        'must' => [
                            [
                                'range' => [
                                    'created_at' => [
                                        'gte' => $this->queryRange['from'],
                                        'lte' => $this->queryRange['to'],
                                        'time_zone' => $this->tzOffsetHm,
                                        'format' => 'yyyy-MM-dd HH:mm:ss',
                                    ],
                                ],
                            ],
                            [
                                'term' => [
                                    'click_type' => $clickThroughTypeId,
                                ],
                            ],
                        ],
                    ],
                ],
                'aggs' => [
                    'clicks' => [
                        'date_histogram' => [
                            'field' => 'created_at',
                            'interval' => $this->queryInterval,
                            'time_zone' => $this->tzOffsetHm,
                            'extended_bounds' => [
                                'min' => $this->extendedBounds['min'],
                                'max' => $this->extendedBounds['max'],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $response = $this->elasticsearchClient->search($params);
        $buckets = $response['aggregations']['clicks']['buckets'];

        foreach ($buckets as $key => $bucket) {
            $data['labels'][] = $bucket['key'];
            $data['values'][] = $bucket['doc_count'];
            $data['total'] += $bucket['doc_count'];
        }

        return $data;
    }

    protected function adClicksThroughByAds()
    {
        $clickThroughTypeId = AdClickType::where('key', AdClickType::TYPE_CLICK)->value('id');
        $data = [];
        $params = [
            'index' => $this->index,
            'body'  => [
                'size'  => 0,
                'query' => [
                    'bool' => [
                        'filter'  => [
                            'match' => [
                                'type' => 'ad_click',
                            ],
                        ],
                        'must' => [
                            [
                                'range' => [
                                    'created_at' => [
                                        'gte' => $this->queryRange['from'],
                                        'lte' => $this->queryRange['to'],
                                        'time_zone' => $this->tzOffsetHm,
                                        'format' => 'yyyy-MM-dd HH:mm:ss',
                                    ],
                                ],
                            ],
                            [
                                'term' => [
                                    'click_type' => $clickThroughTypeId,
                                ],
                            ],
                        ],
                    ],
                ],
                'aggs' => [
                    'clicks' => [
                        'terms' => [
                            'field' => 'ad_id',
                            'min_doc_count' => 0,
                            'size' => 100000,
                        ],
                    ],
                ],
            ],
        ];

        $response = $this->elasticsearchClient->search($params);

        foreach ($response['aggregations']['clicks']['buckets'] as $bucket) {
            $data[$bucket['key']] = $bucket['doc_count'];
        }

        return $data;
    }

    protected function wishlistsHistogram()
    {
        $data = ['total' => 0];
        $params = [
            'index' => $this->index,
            'body'  => [
                'size'  => 0,
                'query' => [
                    'bool' => [
                        'filter'  => [
                            'match' => [
                                'type' => 'wishlist',
                            ],
                        ],
                        'must' => [
                            [
                                'range' => [
                                    'created_at' => [
                                        'gte' => $this->queryRange['from'],
                                        'lte' => $this->queryRange['to'],
                                        'time_zone' => $this->tzOffsetHm,
                                        'format' => 'yyyy-MM-dd HH:mm:ss',
                                    ],
                                ],
                            ],
                            [
                                'terms' => [
                                    'ad_id' => $this->adIdsFilter,
                                ],
                            ],
                        ],
                    ],
                ],
                'aggs' => [
                    'saves' => [
                        'date_histogram' => [
                            'field' => 'created_at',
                            'interval' => $this->queryInterval,
                            'time_zone' => $this->tzOffsetHm,
                            'extended_bounds' => [
                                'min' => $this->extendedBounds['min'],
                                'max' => $this->extendedBounds['max'],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $response = $this->elasticsearchClient->search($params);
        $buckets = $response['aggregations']['saves']['buckets'];

        foreach ($buckets as $key => $bucket) {
            $data['labels'][] = $bucket['key'];
            $data['values'][] = $bucket['doc_count'];
            $data['total'] += $bucket['doc_count'];
        }

        return $data;
    }

    protected function getViewTypes()
    {
        return ViewType::all()->pluck('name', 'id')->toArray();
    }

    protected function contentTypeCount(Collection $collection)
    {
        if ($this->userIdFilter) {
            $collection->where('user_id', $this->userIdFilter);
        }

        if ($this->from) {
            $to = Carbon::createFromFormat(self::INCOMING_DATE_FORMAT, $this->from)->toDateTimeString();
            $collection->where('created_at', '>=', $to);
        }

        if ($this->to) {
            $to = Carbon::createFromFormat(self::INCOMING_DATE_FORMAT, $this->to)->toDateTimeString();
            $collection->where('created_at', '<=', $to);
        }

        return $collection->count();
    }

    protected function getChannelsByIds(array $ids)
    {
        return Channel::whereIn('channels.id', $ids)
            ->join('users AS u', 'channels.user_id', '=', 'u.id')
            ->get(['channels.id', 'channels.uid', 'channels.label AS title', 'channels.asset_path', 'u.name AS username'])
            ->keyBy('id')
            ->toArray();
    }

    protected function getVideosByIds(array $ids)
    {
        return Content::whereIn('contents.id', $ids)
            ->join('users AS u', 'contents.user_id', '=', 'u.id')
            ->get(['contents.id', 'contents.uid', 'contents.title', 'contents.asset_path', 'u.name AS username'])
            ->keyBy('id')
            ->toArray();
    }

    protected function getAdsByIds(array $ids)
    {
        return Ad::whereIn('ads.id', $ids)
            ->join('users AS u', 'ads.user_id', '=', 'u.id')
            ->get(['ads.id', 'ads.uid', 'ads.title', 'ads.asset_path', 'u.name AS username'])
            ->keyBy('id')
            ->toArray();
    }

    protected function getAdIdsByVideoIds(array $ids)
    {
        $result = [];
        $records = DB::table('content_categories AS cc')
            ->join('ad_category AS ac', 'cc.category_id', '=', 'ac.category_id')
            ->whereIn('cc.content_id', $ids)
            ->get(['cc.content_id', 'ac.ad_id'])
            ->toArray();

        if (count($records)) {
            foreach ($records as $record) {
                $result[$record->content_id][] = $record->ad_id;
            }
        }

        return $result;
    }

    protected function getVideoIdsByFieldFilter($nestedField, $termField, $termFieldValues, bool $onlyWithinRange = true)
    {
        $data = [];
        $type = 'content';
        $params = [
            'index' => $this->index,
            'body'  => [
                'size'  => 0,
                'query' => [
                    'bool' => [
                        'filter'  => [
                            'match' => [
                                'type' => $type,
                            ],
                        ],
                        'must' => [
                            [
                                'nested' => [
                                    'path' => $nestedField,
                                    'query' => [
                                        'bool' => [
                                            'filter' => [
                                                'exists' => [
                                                    'field' => $nestedField,
                                                ],
                                            ],
                                            'must' => [
                                                'terms' => [
                                                    $termField => $termFieldValues,
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
                'aggs' => [
                    'ids' => [
                        'terms' => [
                            'field' => 'original_id',
                            'size' => 100000,
                        ],
                    ],
                ],
            ],
        ];

        if ($onlyWithinRange) {
            $params['body']['query']['bool']['must'][] = [
                'range' => [
                    'created_at' => [
                        'gte' => $this->queryRange['from'],
                        'lte' => $this->queryRange['to'],
                        'time_zone' => $this->tzOffsetHm,
                        'format' => 'yyyy-MM-dd HH:mm:ss',
                    ],
                ],
            ];
        }

        $response = $this->elasticsearchClient->search($params);

        foreach ($response['aggregations']['ids']['buckets'] as $bucket) {
            $data[] = $bucket['key'];
        }

        return $data;
    }

    protected function getCategoryIdsFromAds()
    {
        $data = [];
        $type = 'ad';
        $params = [
            'index' => $this->index,
            'body'  => [
                'size'  => 0,
                'query' => [
                    'bool' => [
                        'filter'  => [
                            'match' => [
                                'type' => $type,
                            ],
                        ],
//                        'must' => [
//                            [
//                                'range' => [
//                                    'created_at' => [
//                                        'gte' => $this->queryRange['from'],
//                                        'lte' => $this->queryRange['to'],
//                                        'time_zone' => $this->tzOffsetHm,
//                                        'format' => 'yyyy-MM-dd HH:mm:ss',
//                                    ],
//                                ],
//                            ],
//                        ],
                    ],
                ],
                'aggs' => [
                    'categories' => [
                        'nested' => [
                            'path' => 'categories',
                        ],
                        'aggs' => [
                            'ids' => [
                                'terms' => [
                                    'field' => 'categories.id',
                                    'size' => 100000,
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        if ($this->adIdFilter) {
            $params['body']['query']['bool']['must'][] = [
                'term' => [
                    'original_id' => $this->adIdFilter,
                ],
            ];
        } else {
            if ($this->userIdFilter) {
                $params['body']['query']['bool']['must'][] = [
                    'term' => [
                        'user_id' => $this->userIdFilter,
                    ],
                ];
            }

            if ($this->adTypeFilter) {
                $params['body']['query']['bool']['must'][] = [
                    'nested' => [
                        'path' => 'settings',
                        'query' => [
                            'bool' => [
                                'filter' => [
                                    'exists' => [
                                        'field' => 'settings',
                                    ],
                                ],
                                'must' => [
                                    'term' => [
                                        'settings.type' => $this->adTypeFilter,
                                    ],
                                ],
                            ],
                        ],
                    ],
                ];
            }
        }

        $response = $this->elasticsearchClient->search($params);

        foreach ($response['aggregations']['categories']['ids']['buckets'] as $bucket) {
            $data[] = $bucket['key'];
        }

        return $data;
    }

    protected function getAdIds()
    {
        $data = [];
        $type = 'ad';
        $params = [
            'index' => $this->index,
            'body'  => [
                'size'  => 0,
                'query' => [
                    'bool' => [
                        'filter'  => [
                            'match' => [
                                'type' => $type,
                            ],
                        ],
                        'must' => [
                            [
                                'range' => [
                                    'created_at' => [
                                        'gte' => $this->queryRange['from'],
                                        'lte' => $this->queryRange['to'],
                                        'time_zone' => $this->tzOffsetHm,
                                        'format' => 'yyyy-MM-dd HH:mm:ss',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
                'aggs' => [
                    'ids' => [
                        'terms' => [
                            'field' => 'original_id',
                            'size' => 100000,
                        ],
                    ],
                ],
            ],
        ];

        if ($this->userIdFilter) {
            $params['body']['query']['bool']['must'][] = [
                'term' => [
                    'user_id' => $this->userIdFilter,
                ],
            ];
        }

        if ($this->adTypeFilter) {
            $params['body']['query']['bool']['must'][] = [
                'nested' => [
                    'path' => 'settings',
                    'query' => [
                        'bool' => [
                            'filter' => [
                                'exists' => [
                                    'field' => 'settings',
                                ],
                            ],
                            'must' => [
                                'term' => [
                                    'settings.type' => $this->adTypeFilter,
                                ],
                            ],
                        ],
                    ],
                ],
            ];
        }

        $response = $this->elasticsearchClient->search($params);

        foreach ($response['aggregations']['ids']['buckets'] as $bucket) {
            $data[] = $bucket['key'];
        }

        return $data;
    }

    /**
     * @param string $type
     * @param string $termAggName
     * @param string $termAggField
     * @return array - Elasticsearch query params.
     */
    protected function dateHistogramAggParams($type, $termAggName, $termAggField)
    {
        $params = [
            'index' => $this->index,
            'body'  => [
                'size'  => 0,
                'query' => [
                    'bool' => [
                        'filter'  => [
                            'match' => [
                                'type' => $type,
                            ],
                        ],
                        'must' => [
                            [
                                'range' => [
                                    'created_at' => [
                                        'gte' => $this->queryRange['from'],
                                        'lte' => $this->queryRange['to'],
                                        'time_zone' => $this->tzOffsetHm,
                                        'format' => 'yyyy-MM-dd HH:mm:ss',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
                'aggs' => [
                    $termAggName => [
                        'terms' => [
                            'field' => $termAggField,
                            'min_doc_count' => 0,
                        ],
                        'aggs' => [
                            'data' => [
                                'date_histogram' => [
                                    'field' => 'created_at',
                                    'interval' => $this->queryInterval,
                                    'time_zone' => $this->tzOffsetHm,
                                    'extended_bounds' => [
                                        'min' => $this->extendedBounds['min'],
                                        'max' => $this->extendedBounds['max'],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        if ($type === 'subscription' && $this->channelIdFilter) {
            $params['body']['query']['bool']['must'][] = [
                'term' => [
                    'channel_id' => $this->channelIdFilter,
                ],
            ];
        } else {
            $this->applyFilters($params);
        }

        return $params;
    }

    /**
     * Apply selected filters to the query if necessary.
     *
     * @param array $params - Elasticsearch query params.
     */
    protected function applyFilters(array &$params)
    {
        if ($this->contentIdFilter) {
            $params['body']['query']['bool']['must'][] = [
                'term' => [
                    'content_id' => $this->contentIdFilter,
                ],
            ];
        } else {
            if ($this->contentIdsFilter) {
                $params['body']['query']['bool']['must'][] = [
                    'terms' => [
                        'content_id' => $this->contentIdsFilter,
                    ],
                ];
            } else {
                if ($this->userIdFilter) {
                    $params['body']['query']['bool']['must'][] = [
                        'term' => [
                            'user_id' => $this->userIdFilter,
                        ],
                    ];
                }
            }
        }
    }
}
