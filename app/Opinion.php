<?php

namespace App;

use App\Events\ModelCreated;
use App\Events\ModelDeleted;
use App\Events\ModelUpdated;
use DB;
use Illuminate\Support\Collection;
use Sleimanx2\Plastic\Searchable;

class Opinion extends AbstractModel
{
//    use Searchable;

    const LIKE = 1;
    const DISLIKE = 0;

    public $syncDocument = false;

    public $elasticDocumentType = 'opinion';

    protected $fillable = [
        'device_id',
        'content_id',
        'like',
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => ModelCreated::class,
        'updated' => ModelUpdated::class,
        'deleted' => ModelDeleted::class,
    ];

    public function device()
    {
        return $this->hasOne(Device::class, 'id', 'device_id');
    }

    public function content()
    {
        return $this->hasOne(Content::class, 'id', 'content_id');
    }

    /**
     * Build the document data for storing in Elasticsearch.
     *
     * @return array
     */
    public function buildDocument()
    {
        return [
            'type' => $this->elasticDocumentType,
            'id' => $this->elasticDocumentType . '-' . $this->id,
            'device_id' => $this->device_id,
            'content_id' => $this->content_id,
            'user_id' => $this->user_id,
            'like' => $this->like,
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }

    /**
     * Process the model before syncing with the Elasticsearch index.
     */
    public function prepareForElasticsearch()
    {
        $this->type = $this->elasticDocumentType;
        $this->user_id = $this->content->user_id;
    }

    /**
     * Returns the data required for ES index rebuild.
     *
     * @param int $limit
     * @param int $offset
     * @return Collection
     */
    public static function allForElasticsearch($limit = 0, $offset = 0)
    {
        return self::join('contents AS c', 'opinions.content_id', '=', 'c.id')
            ->limit($limit)
            ->offset($offset)
            ->get([
                DB::raw('CONCAT("opinion") AS type'),
                'opinions.id',
                'opinions.device_id',
                'opinions.content_id',
                'opinions.like',
                'opinions.created_at',
                'opinions.updated_at',
                'c.user_id',
            ]);
    }
}
