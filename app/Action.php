<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    const UPDATED_AT = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'action_type_id',
        'entity',
        'entity_class_name',
        'entity_id',
    ];
}
