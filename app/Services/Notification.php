<?php

namespace App\Services;

use App\Device;
use App\Content;
use GuzzleHttp\Client;

class Notification
{
    public $device;
    public $content;
    protected $request;
    const API_URL = "https://onesignal.com/api/v1";
    const ENDPOINT_NOTIFICATIONS = "/notifications";

    protected $appId;

    public function __construct()
    {
        $this->appId = env('ONESIGNAL_APP_ID');
        $this->client = new Client();

    }

    public function silent()
    {
        $this->request['headers']['Content-Type']  = 'application/json';
        $this->request['headers']['Authorization'] = 'Basic ' . env('ONESIGNAL_API_KEY');
        $this->request['body'] = json_encode([
            'content_available'     =>  true,   // @important this parameter ensures that this is a silent notification
            'app_id'                =>  $this->appId,
            'include_player_ids'    =>  [ $device->uid ],
            'data'                  =>  [
                // 'video_url' =>  'https://s3-eu-west-1.amazonaws.com/snique-test-bucket/content/fec3352ab80511e78ab3080027c97816.mp4'
                'video_url' =>  'https://www.quirksmode.org/html5/videos/big_buck_bunny.mp4'
                // 'video_url' =>  $this->content->resource_uri
            ]
        ]);

        return $this->client->post(self::API_URL . self::ENDPOINT_NOTIFICATIONS, $this->request);
    }

    public function send(Content $content)
    {
        $this->content = $content;

        return $this;
    }

    public function to(Device $device)
    {
        $this->device = $device;

        return $this;
    }
}