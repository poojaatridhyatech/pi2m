<?php

namespace App\Services;

use Exception;
use Namshi\JOSE\SimpleJWS;

/**
 * Class JwsService
 * @package Stikit\Services
 */
class JwtService
{
    /**
     * JWS Library holder
     *
     * @var SimpleJWS
     */
    protected $lib;

    /**
     * Algorithm used to encrypt
     *
     * @var string
     */
    protected $algorithm = 'RS256';

    /**
     * Token Payload
     *
     * @var array
     */
    public $payload = [];

    /**
     * RSA Private key string
     *
     * @var string
     */
    private $privateKey;

    /**
     * RSA Public key string
     *
     * @var string
     */
    private $publicKey;

    /**
     */
    public function __construct()
    {
        $this->lib = new SimpleJWS($this->getInit());

        $this->setPrivateKey();
        $this->setPublicKey();
    }

    /**
     * Get JWT token from header 'authorization'
     *
     * @param string $authHeader
     * @return string
     * @throws Exception if can't find it
     */
    public function extractTokenFromAuthHeader($authHeader)
    {
        $details = explode(' ', $authHeader);
        if (! isset($details[1])) {
            throw new \Exception("Couldn't find the token in header");
        }

        return $details[1];
    }

    /**
     * Generate the JWT token
     *
     * @return string
     */
    public function generateToken()
    {
        $this->lib->sign($this->getPrivateKey());

        return $this->lib->getTokenString();
    }

    /**
     * Validate a given token, and return payload on success
     *
     * @param  string $token
     * @return mixed
     */
    public function validateToken($token)
    {
        try {
            $this->lib = SimpleJWS::load($token);
            if ($this->lib->isValid($this->getPublicKey(), $this->algorithm)) {
                return $this->lib->getPayload();
            }
        } catch (Exception $e) {
            //
        }

        return false;
    }

    /**
     * Get the initialization vars
     *
     * @return array
     */
    public function getInit()
    {
        return [
            'alg' => $this->algorithm
        ];
    }

    /**
     * Get the value of payload
     *
     * @return mixed
     */
    public function getPayload()
    {
        return $this->lib->getPayload();
    }

    /**
     * Set the value of payload
     *
     * @param array $payload the payload
     * @return $this
     */
    public function setPayload(array $payload)
    {
        if (
            isset($payload['uid'])
            && ! isset($payload['real_uid'])
            && ! isset($this->payload['real_uid'])
        ) {
            $payload['real_uid'] = $payload['uid'];
        }
        $this->payload = array_merge($this->payload, $payload);
        $this->lib->setPayload($payload);

        return $this;
    }

    /**
     * Return the public key
     *
     * @return resource|false
     */
    public function getPublicKey()
    {
        return openssl_pkey_get_public($this->publicKey);
    }

    /**
     * Set the public key
     *
     * @return $this
     */
    public function setPublicKey()
    {
        $this->publicKey =  "-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCpWfhkOf3jSuizB0CdSQFBdcSu
uxWpflQu5hG1OEfc05hecOKkpKdQq0c6dxjntTkBd1JC0Kw6wEiyhf5VrockVq22
PA5WgQBY7rFUwsyGtlNd0Or1/7f28TA2vE74gRE3KMi4e1NqFe2ONTSJyFgtTBbz
ocogLWwglm9M9VuXcQIDAQAB
-----END PUBLIC KEY-----";

        return $this;
    }

    /**
     * Get the private key
     *
     * @return resource|false
     */
    private function getPrivateKey()
    {
        return openssl_pkey_get_private($this->privateKey);
    }

    /**
     * Set the private key
     *
     * @return $this
     */
    private function setPrivateKey()
    {
        $this->privateKey = '-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQCpWfhkOf3jSuizB0CdSQFBdcSuuxWpflQu5hG1OEfc05hecOKk
pKdQq0c6dxjntTkBd1JC0Kw6wEiyhf5VrockVq22PA5WgQBY7rFUwsyGtlNd0Or1
/7f28TA2vE74gRE3KMi4e1NqFe2ONTSJyFgtTBbzocogLWwglm9M9VuXcQIDAQAB
AoGACLP+9+ejDJNE8i5lO7AMyHhs6y3iJxu+58obDt0dlFOreO8ENsHfd13WA0Mr
nHzLuS+Qoq5mq35GPiZ0s6KMf1QiLchTPebAlpGckOTzy+rLJG5/a3hF9fBcqa5I
OOkdaWMu50B08K9uhi898D3/T67MbwhbjX0pO34VpUrQJiECQQDekH6JHblrr+KV
HoLcH2qzzkOYVovvShqrGc0mgzd8ZQmqX+NaLl4WB99gcomVdBW1JBC601l8a/Pk
hdM8P9GrAkEAwsr7TyM3/bbSnDk+BfCKLcPLC+W/N9ho3eimxufJbf9LPooXdVlB
LZAgGl+IG9FrCiuaLU3lCnwsI4kkjrzXUwJBAIZ5q9rROO2WEkOjcdHqsZ/+qtzq
vhZRIhfruQbiFa51pgdpdIdSqMqK6y7tRbAluc//AJTBirogx/Z13ZHzxY0CQFt/
fQxvPp2ugr9fJAMmOcHFdlrgUDGrNZcG8U/9EGDayj1WIC6+/h6o1GuO9CAc/mqI
kKM3M575NMJ8aO+jRIECQElJ44U+uJBxhjrt32729nEg223AS6YJj/3H6S4uEFyt
tWQXaIe7ZluI1T+ONeKLmbiKx6Z+7VSyNt4qYFKlZ4Y=
-----END RSA PRIVATE KEY-----
';

        return $this;
    }
}
