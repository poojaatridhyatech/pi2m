<?php

namespace App\Help;

use Exception;
use App\Activation;
use Ramsey\Uuid\Uuid;

/**
 * Class Generate
 * @package App\Help
 */
class Generate
{
    public static function crudInstance($name, $type = null)
    {
        $fileName = ucfirst( strtolower($name) ) . '.php';

        $dir = 'Create';
        switch ($type) {
            case 'c':
                $dir = 'create';
                $contents = "<?php

namespace App\Routines\Crud\Create;

use Auth;
use Generate;
use App\\". ucfirst( strtolower($name) ) ." as Model;
use Illuminate\Http\Request;

class ". ucfirst(strtolower($name)) ."
{
    public function __construct(Request request)
    {
        // create
    }
}

            ";

            break;

            case 'u':
                $dir = 'update';
                $contents = "<?php

namespace App\Routines\Crud\Update;

use Auth;
use Generate;
use App\\". ucfirst(strtolower($name)) ." as Model;
use Illuminate\Http\Request;

class ". ucfirst(strtolower($name)) ."
{
    public function __construct(". ucfirst(strtolower($name)) ." ". $name .", Request request)
    {
        // update
    }
}

            ";

            break;

            case 'd':
                $dir = 'delete';
                $contents = "<?php

namespace App\Routines\Crud\Delete;

use Auth;
use Generate;
use App\\". ucfirst(strtolower($name)) ." as Model;
use Illuminate\Http\Request;

class ". ucfirst(strtolower($name)) ."
{
    public function __construct(". ucfirst(strtolower($name)) ." ". $name .")
    {
        // delete
    }
}

            ";

            break;
            
            default:
                $dir = 'all';
                break;
        }

        $path = app_path() . '/Routines/Crud/' . ucfirst($dir) . '/' . $fileName;

        if ( file_exists($path) ) {
            throw new Exception("File Exists.");
        }

        //Some simple example content.
            
            //Save our content to the file.
            file_put_contents($path, $contents);


        return true;
    }

    /**
     * Generates a client secret
     * @param  integer $length Length of the string
     * @return string
     */
    public static function secret($length = 32)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_&%$@';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return str_shuffle(self::noDashUid() . $randomString);
    }

    /**
     * Generates an activation link that can be sent to a 
     * newly created user.
     * 
     * @param  Activation $activation
     * @return string
     */
    public static function activationLink(Activation $activation)
    {
        return route('activate') . '?token=' . $activation->hash;
    }

    /**
     * Create a simple message from an exception that will have
     * the filename, line number and the exception message
     *
     * @param  Exception $exception
     * @return string
     */
    public static function usefulExceptionMessage(Exception $exception)
    {
        return $exception->getMessage() . ' in ' . $exception->getFile() . ' on line ' . $exception->getLine();
    }

    /**
     * Generate a UUID
     *
     * @return string
     */
    public static function uid()
    {
        $uuid1 = Uuid::uuid1();

        return $uuid1->toString();
    }

    /**
     * Generates a UUID without the dashes between
     *
     * @return string
     */
    public static function noDashUid()
    {
        return str_replace('-', '', self::uid());
    }
}
