<?php

namespace App\Help;

use Exception;
use Illuminate\Http\Request;

/**
 * Class Extract
 * @package App\Help
 */
class Extract
{
    public static function modelIdsFromChecklist($keyword, Request $request, $model = null)
    {
        if (! $model) {
            $className = '\App\\' . ucfirst(str_singular($keyword));
            $model = new $className;
        } else {
            $model = new $model;
        }
        
        $uids = [];

        foreach ($request->all() as $key => $field) {
            if ( starts_with($key, $keyword) ) {
                $exp = explode('_', $key);
                $uid = $exp[1];
                
                $uids[] = $uid;
            }
        }

        $models = $model->whereIn('uid', $uids)->pluck('id')->toArray();

        return $models;

    }
}
