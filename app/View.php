<?php

namespace App;

use App\Device;
use App\Content;
use App\Events\ModelCreated;
use App\Events\ModelDeleted;
use App\Events\ModelUpdated;
use App\ViewType;
use DB;
use Illuminate\Support\Collection;
use Sleimanx2\Plastic\Searchable;

class View extends AbstractModel
{
//    use Searchable;

    public $syncDocument = false;

    public $elasticDocumentType = 'view';

    protected $fillable = [
        'device_id',
        'content_id',
        'view_type_id',
        'duration',
        'data',
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => ModelCreated::class,
        'updated' => ModelUpdated::class,
        'deleted' => ModelDeleted::class,
    ];

    public function type()
    {
        return $this->hasOne(ViewType::class, 'id', 'view_type_id');
    }

    public function device()
    {
        return $this->hasOne(Device::class, 'id', 'device_id');
    }

    public function content()
    {
        return $this->hasOne(Content::class, 'id', 'content_id');
    }

    /**
     * Build the document data for storing in Elasticsearch.
     *
     * @return array
     */
    public function buildDocument()
    {
        return [
            'type' => $this->elasticDocumentType,
            'id' => $this->elasticDocumentType . '-' . $this->id,
            'content_id' => $this->content_id,
            'device_id' => $this->device_id,
            'view_type_id' => $this->view_type_id,
            'user_id' => $this->user_id,
            'duration' => $this->duration,
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }

    /**
     * Process the model before syncing with the Elasticsearch index.
     */
    public function prepareForElasticsearch()
    {
        $this->type = $this->elasticDocumentType;
        $this->user_id = $this->content->user_id;
    }

    /**
     * Returns the data required for ES index rebuild.
     *
     * @param int $limit
     * @param int $offset
     * @return Collection
     */
    public static function allForElasticsearch($limit = 0, $offset = 0)
    {
        return self::join('contents AS c', 'views.content_id', '=', 'c.id')
            ->limit($limit)
            ->offset($offset)
            ->get([
                DB::raw('CONCAT("view") AS type'),
                'views.id',
                'views.content_id',
                'views.device_id',
                'views.view_type_id',
                'views.duration',
                'views.created_at',
                'views.updated_at',
                'c.user_id',
            ]);
    }
}
