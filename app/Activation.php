<?php

namespace App;

use Generate;
use App\User;

class Activation extends AbstractModel
{
    protected $fillable = [
        'uid',
        'user_id',
        'hash',
        'activated',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public static function new(User $user)
    {
        $uid = Generate::noDashUid();

        $instance = self::create([
            'uid'       =>  $uid,
            'user_id'   =>  $user->id,
            'hash'      =>  bcrypt($uid . '_' . $user->uid),
            'activated' =>  false,
        ]);

        return $instance;
    }
}
