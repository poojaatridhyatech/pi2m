<?php

namespace App;

use App\Device;
use App\Content;
use App\Events\ModelCreated;
use App\Events\ModelDeleted;
use App\Events\ModelUpdated;
use DB;
use Illuminate\Support\Collection;
use Sleimanx2\Plastic\Searchable;

class Push extends AbstractModel
{
    //use Searchable;

    const PUSHED = 1;
    const NOT_PUSHED = 0;

    public $syncDocument = false;

    public $elasticDocumentType = 'push';

    protected $fillable = [
        'device_id',
        'content_id',
        'deleted',
        'pushed',
        'push_at',
        'channel_id',
        'notification',
        'ad_id',
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => ModelCreated::class,
        'updated' => ModelUpdated::class,
        'deleted' => ModelDeleted::class,
    ];

    /**
     * Push constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        if (!$this->documentIndex) {
            if ($currentIndex = session('elasticsearch_current_index')) {
                $this->documentIndex = $currentIndex;
            }
        }

        parent::__construct($attributes);
    }

    public function scheduled()
    {
        return $this->where('push_at', '<', date('Y-m-d H:i:s'))
                    ->where('pushed', false)
                    ->get();
    }

    public function device()
    {
        return $this->hasOne(Device::class, 'id', 'device_id');
    }

    public function content()
    {
        return $this->hasOne(Content::class, 'id', 'content_id');
    }

    public function channel()
    {
        return $this->hasOne(Channel::class, 'id', 'channel_id');
    }

    /**
     * Build the document data for storing in Elasticsearch.
     *
     * @return array
     */
    public function buildDocument()
    {
        return [
            'type' => $this->elasticDocumentType,
            'id' => $this->elasticDocumentType . '-' . $this->id,
            'device_id' => $this->device_id,
            'content_id' => $this->content_id,
            'ad_id' => $this->ad_id,
            'user_id' => $this->user_id,
            'deleted' => $this->deleted,
            'pushed' => $this->pushed,
            'created_at' => $this->created_at->toDateTimeString(),
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }

    /**
     * Process the model before syncing with the Elasticsearch index.
     */
    public function prepareForElasticsearch()
    {
        $this->type = $this->elasticDocumentType;
        $this->user_id = $this->content->user_id;
    }

    /**
     * Returns the data required for ES index rebuild.
     *
     * @param int $limit
     * @param int $offset
     * @return Collection
     */
    public static function allForElasticsearch($limit = 0, $offset = 0)
    {
        return self::join('contents AS c', 'pushes.content_id', '=', 'c.id')
            ->limit($limit)
            ->offset($offset)
            ->get([
                DB::raw('CONCAT("push") AS type'),
                'pushes.id',
                'pushes.device_id',
                'pushes.content_id',
                'pushes.ad_id',
                'pushes.deleted',
                'pushes.pushed',
                'pushes.created_at',
                'pushes.updated_at',
                'c.user_id',
            ]);
    }
}
