<?php

namespace App\Jobs;

use App\AbstractModel;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Plastic;
use Sleimanx2\Plastic\Exception\MissingArgumentException;

abstract class AbstractElasticDocument implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, Queueable;

    /**
     * @var AbstractModel
     */
    protected $model;

    /**
     * Elasticsearch current index name.
     *
     * @var string
     */
    protected $index;

    /**
     * @var string
     */
    protected $documentType = '_doc';

    /**
     * Create a new job instance.
     *
     * @param AbstractModel $model
     */
    public function __construct($model)
    {
        $this->model = $model;
        $this->setIndex();
    }

    public function saveDocument()
    {
        $this->exitIfModelNotReady();

        $document = $this->model->getDocumentData();

        $params = [
            'id' => $document['id'],
            'type' => $this->documentType,
            'index' => $this->index,
            'body' => $document,
        ];

        $client = Plastic::getClient();

        return $client->index($params);
    }

    public function updateDocument()
    {
        $this->exitIfModelNotReady();

        $document = $this->model->getDocumentData();

        return $this->update($document);
    }

    public function updateDocumentByModel(Model $model)
    {
        $document = $model->getDocumentData();

        return $this->update($document);
    }

    protected function update(array $document)
    {
        $params = [
            'id' => $document['id'],
            'type' => $this->documentType,
            'index' => $this->index,
            'body' => [
                'doc' => $document,
            ],
        ];

        $client = Plastic::getClient();

        return $client->update($params);
    }

    protected function setIndex()
    {
        $client = Plastic::getClient();
        $defaultIndex = Plastic::getDefaultIndex();
        $indexAlias = env('ELASTICSEARCH_INDEX_ALIAS', '');
        if ($indexAlias) {
            $aliasInfo = $client->indices()->getAlias([
                'index' => '*',
                'name' => $indexAlias,
            ]);
            $this->index = ($aliasInfo) ? current(array_keys($aliasInfo)) : $defaultIndex;
        } else {
            $this->index = $defaultIndex;
        }
    }

    private function exitIfModelNotReady(bool $deleted = false)
    {
        if (!$this->model) {
            throw new MissingArgumentException('You should set the model first.');
        }

        if (!$deleted && !$this->model->exists) {
            throw new Exception('Model not persisted yet.');
        }
    }
}
