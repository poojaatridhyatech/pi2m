<?php

namespace App\Jobs;

use Exception;

class ElasticUpdateVideoChannels extends AbstractElasticDocument
{
    /**
     * Execute the job.
     */
    public function handle()
    {
        try {
            if (count($this->model->channels)) {
                foreach ($this->model->channels as $channel) {
                    $channel->prepareForElasticsearch();
                    $this->updateDocumentByModel($channel);
                }
            }
        } catch (Exception $e) {
            // Do nothing
        }
    }
}
