<?php

namespace App\Jobs;

use Exception;

class ElasticDocumentCreate extends AbstractElasticDocument
{
    /**
     * Execute the job.
     */
    public function handle()
    {
        try {
            if (!$this->model->shouldSyncDocument()) {
                if (method_exists($this->model, 'prepareForElasticsearch')) {
                    $this->model->prepareForElasticsearch();
                }

                if (!property_exists($this->model, 'documentIndex') || !$this->model->documentIndex) {
                    $this->model->documentIndex = $this->index;
                }

                $this->saveDocument();
            }
        } catch (Exception $e) {
            // Do nothing
        }
    }
}
