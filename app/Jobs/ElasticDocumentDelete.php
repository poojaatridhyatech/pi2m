<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Plastic;

class ElasticDocumentDelete implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    private $class;

    /**
     * @var int
     */
    private $id;

    /**
     * Elasticsearch current index name.
     *
     * @var string
     */
    private $index;

    /**
     * @var string
     */
    protected $documentType = '_doc';

    /**
     * Create a new job instance.
     *
     * @param string $class - Model's class name
     * @param int $id - Document ID
     */
    public function __construct($class, $id)
    {
        $this->class = $class;
        $this->id = $id;
        $this->setIndex();
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        $client = Plastic::getClient();
        $class = $this->class;
        $model = new $class();

        $params = [
            'id' => $model->elasticDocumentType . '-' . $this->id,
            'index' => $this->index,
            'type' => $this->documentType,
        ];

        if ($client->exists($params)) {
            $client->delete($params);
        }
    }

    private function setIndex()
    {
        $client = Plastic::getClient();
        $defaultIndex = Plastic::getDefaultIndex();
        $indexAlias = env('ELASTICSEARCH_INDEX_ALIAS', '');
        if ($indexAlias) {
            $aliasInfo = $client->indices()->getAlias([
                'index' => '*',
                'name' => $indexAlias,
            ]);
            $this->index = ($aliasInfo) ? current(array_keys($aliasInfo)) : $defaultIndex;
        } else {
            $this->index = $defaultIndex;
        }
    }
}
