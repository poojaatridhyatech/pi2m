<?php

namespace App\Http\Middleware;

use Closure;
use Generate;
use App\Client;
use App\Device;
use App\Subscription;
use App\Services\JwtService;
use App\Exceptions\SniqueAuthValidation;

class MainApiGate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $jwt = new JwtService;
        $authHeader = $request->headers->get('authorization');
        if ( ! $authHeader ) {
            throw new SniqueAuthValidation(trans('gate.no_auth_header_found'));
        }

        $token = $jwt->extractTokenFromAuthHeader($authHeader);
        $jwt->validateToken($token);
        if ( ! isset($jwt->getPayload()['secret']) ) {
            throw new SniqueAuthValidation( trans('gate.not_valid_jwt') );
        }

        $client = Client::where('secret', decrypt($jwt->getPayload()['secret']))->first();
        if ( ! $client ) {
            throw new SniqueAuthValidation( trans('gate.client_not_found') );
        }

        $deviceHeader = $request->headers->get('device-uuid');
        $oneSignalHeader = $request->headers->get('onesignal-uuid');

        if ( ! $oneSignalHeader ) {
            throw new SniqueAuthValidation('OS Header not sent');
        }

        $device = Device::where('uid', $deviceHeader)->first();
        if ( ! $device ) {
            // create new Device instance
            $info = $request->headers->get('device-info');
            $device = Device::create([
                'uid'   =>  $deviceHeader,
                'info'  =>  ($device) ? json_encode($device) : null,
                'onesignal_uid' =>  ($oneSignalHeader) ? $oneSignalHeader : null,
            ]);
        }

        if ( 
            ($oneSignalHeader && $device->onesignal_uid !== $oneSignalHeader) 
            || 
            (! $device->onesignal_uid) 
        ) {
            $device->onesignal_uid = $oneSignalHeader;
            $device->save();
        }

        // attach device to client
        $device->clients()->sync([$client->id]);

        // subscribe them to all clients channels
        if ( count($device->subscriptions) < count($client->channels) ) {
            foreach ($client->channels as $channel) {
                $subscription = Subscription::where('device_id', $device->id)->where('channel_id', $channel->id)->first();
                if (  ! $subscription ) {
                    $device->subscribeTo($channel);
                }
            }
        }

        // store some data in session
        session()->put('device', $device->uid);
        session()->put('client', $client->uid);

        return $next($request);
    }
}