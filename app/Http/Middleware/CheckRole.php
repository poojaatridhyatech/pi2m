<?php

namespace App\Http\Middleware;

use App\Role;
use Closure;
use Illuminate\Http\Request;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param string $role
     * @return mixed
     */
    public function handle($request, Closure $next, string $role)
    {
        $r = Role::where('name', $role)->first();

        if ($r && !$request->user()->hasRole($r)) {
            return abort(403);
        }

        return $next($request);
    }
}
