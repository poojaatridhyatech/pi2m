<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class CheckDeveloper
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if ( $user->email === 'imants@eskimo.uk.com' ) {
            return $next($request);
        }

        abort(404);
    }
}
