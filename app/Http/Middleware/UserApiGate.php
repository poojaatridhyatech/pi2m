<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use App\Services\JwtService;
use App\Exceptions\SniqueAuthValidation;

class UserApiGate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $jwt = new JwtService;
        $authHeader = $request->headers->get('authorization');
        if ( ! $authHeader ) {
            throw new SniqueAuthValidation('Unauthorised');
        }

        $token = $jwt->extractTokenFromAuthHeader($authHeader);
        $jwt->validateToken($token);

        if (!isset($jwt->getPayload()['uid'])) {
            throw new SniqueAuthValidation('Unauthorised');
        }

        $user = User::where('uid', $jwt->getPayload()['uid'])->first();

        if ( ! $user ) {
            throw new SniqueAuthValidation('Unauthorised');
        }

        session()->put('user', $user->uid);

        return $next($request);
    }
}
