<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Role;
use App\Http\Requests\PutUser;
use App\Routines\Crud\Update\User as UpdateUser;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    //
    public function index()
    {
        $user = Auth::user();

        return view('account.index', [
            'user' => $user,
            'roles' => Role::where('name', '!=', Role::ROLE_USER)->get(),
            'activeRoles' => $user->roles()->pluck('id')->toArray(),
        ]);
    }

    public function update(PutUser $request)
    {
        $user = Auth::user();
        //new UpdateUser($user, $request);
        $user->name = $request->name;
        if ( $user->email != $request->email ) {
            $testUser = User::where('email', $request->email)->first();
            if ($testUser) {
                return redirect()->route('account.page')->with('errorMsg', 'The email has already been taken.');
            }
            $user->email = $request->email;
        }
        if ($request->asset_path) {
            $user->asset_path = $request->asset_path;
        }
        $user->save();

        return redirect()->route('account.page');
    }

    public function postChangePassword(Request $request)
    {
        if ($request->password === $request->password_verify) {
            $user = Auth::user();
            $user->password = bcrypt($request->password);
            $user->save();
            return $this->success([
                'status'  =>  'ok'
            ]);
        }

        return $this->error([
            'status'  =>  'err',
            'message' => 'password doesn\'t match'
        ]);
    }

    public function destroy(Request $request) {
        $user = Auth::user();

        //$u = User::where('uid', $user->uid)->first();

        // $u->categories()->detach();
        // $u->channels()->detach();
        // $u->ads()->detach();
        // $u->activations()->detach();
        // $u->clients()->detach();
        // $u->roles()->detach();

        // $u->categories()->dissociate();
        // $u->channels()->dissociate();
        // $u->ads()->dissociate();
        // $u->activations()->dissociate();
        // $u->clients()->dissociate();
        // $u->roles()->dissociate();

        //$u->delete();

        $user->delete();

        Auth::logout();

        return redirect('/');
    }

}
