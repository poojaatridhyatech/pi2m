<?php

namespace App\Http\Controllers;

use App\Http\Requests\DashboardAdsFilter;
use App\Http\Requests\DashboardVideosFilter;
use App\Repos\ActionRepo;
use App\Repos\DashboardRepo;
use Artisan;
use Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return RedirectResponse|Redirector
     */
    public function index()
    {
        $user = Auth::user();
        if ($user) {
            return redirect(route('dashboard.content.summary'));
        }

        return redirect(route('login'));
    }

    public function dashboardContentSummary()
    {
        //return view('dashboard.content.summary');
        $user = Auth::user();
        return view('dashboard.summary')->with([
            'active' => 'videos',
            'user' => $user
        ]);
    }

    public function dashboardAdSummary()
    {
        //return view('dashboard.ad.summary');
        $user = Auth::user();
        return view('dashboard.summary')->with([
            'active' => 'ads',
            'user' => $user
        ]);
    }

    public function dashboardAdAds()
    {
        return view('dashboard.ad.ads');
    }

    public function contentChartsData(DashboardVideosFilter $request, DashboardRepo $dashboardRepo)
    {
        $filters = $request->only([
            'dateFrom',
            'dateTo',
            'selectedContentType',
            'selectedUser',
            'selectedChannel',
            'selectedVideo',
            'selectedCategories',
            'tzOffset',
        ]);
        $data = $dashboardRepo->loadVideosData($filters);

        return response()->json($data);
    }

    public function adChartsData(DashboardAdsFilter $request, DashboardRepo $dashboardRepo)
    {
        $filters = $request->only([
            'dateFrom',
            'dateTo',
            'selectedUser',
            'selectedAdType',
            'selectedSearchType',
            'selectedAd',
            'selectedCategories',
            'tzOffset',
        ]);
        $data = $dashboardRepo->loadAdsData($filters);

        return response()->json($data);
    }

    public function notifications(Request $request, ActionRepo $actionRepo)
    {
        $request->validate([
            'tzOffset' => 'required|numeric',
        ]);

        $actions = $actionRepo->loadActions($request->tzOffset);

        return response()->json($actions);
    }

    public function reindexPage()
    {
        $user = Auth::user();

        if ($user->isAdmin()) {
            return view('dashboard.reindex');
        }

        return abort(403);
    }

    public function reindexRun()
    {
        $user = Auth::user();

        if ($user->isAdmin()) {
            $exitCode = Artisan::call('es-index:rebuild');

            if ($exitCode === 0) {
                return response(null, 204);
            }

            return response('Something went wrong! Check the log file.', 500);
        }

        return abort(403);
    }
}
