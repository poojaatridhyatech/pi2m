<?php

namespace App\Http\Controllers;

use Generate;
use App\User;
use Exception;
use Validator;
use Illuminate\Http\Request;
use App\Services\JwtService;
use App\Routines\Auth\Facebook;
use App\Exceptions\SniqueAuthValidation;

/**
 * @resource Social Authentication
 */
class AuthController extends Controller
{
    /**
     * Facebook
     * 
     * @param  Request    $request         [description]
     * @param  Facebook   $facebookRoutine [description]
     * @param  JwtService $jwt             [description]
     * @param  facebook     $type            [description]
     * @return [type]                      [description]
     */
    public function postAuth(
        Request $request,
        Facebook $facebookRoutine,
        JwtService $jwt,
        $type
    ){
        switch ($type) {
            case 'facebook':

                $validator = Validator::make($request->all(), [
                    'token' => 'required'
                ]);

                if ($validator->fails()) {
                    $fails = $validator->errors();
                    throw new SniqueAuthValidation($fails->first());
                }

                $facebookUser = $facebookRoutine
                    ->setToken($request->token)
                    ->attempt()
                    ->getUser()
                ;

                $user = User::where('email', $facebookUser->email)->first();

                if ( ! $user ) {
                    // if a user doesn't exist in the system create one
                    $user = User::create([
                        'uid'   =>  Generate::noDashUid(),
                        'name'  =>  $facebookUser->name,
                        'email'  =>  $facebookUser->email,
                    ]);
                }
            break;
        }

        $jwt = new JwtService;
        $jwt->setPayload([
            'uid' => $user->uid,
        ]);

        return $this->success([
            'user_id'   =>  $user->uid,
            'token'     =>  $jwt->generateToken(),
        ]);
    }
}