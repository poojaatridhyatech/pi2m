<?php

namespace App\Http\Controllers;

use App\Events\ModelCreated;
use App\Events\ModelUpdated;
use Auth;
use App\Ad;
use Storage;
use Generate;
use App\User;
use App\Category;
use App\Content;
use App\Channel;
use App\Http\Requests\UploadContent;
use Illuminate\Http\Request;

/**
 * @resource Videos
 *
 * Access and modify uploaded content.
 */
class ContentController extends Controller
{
    public function postSearchByUid(Request $request)
    {
        $content = Auth::user()->contents()->where('uid', $request->uid)->orWhere('title', 'LIKE', '%' . $request->uid . '%')->paginate(10);
        
        return view('uploads')->with([
            'list' => $content->appends(Request::capture()->except(['page'])),
            'selectedUser' => null,
            'users'  => [],
            'active' => 'videos'
        ]);
    }

    public function getEdit($uid)
    {
        $user = null;
        $content =  Content::where('uid', $uid)->first();

        if ($content) {
            $user = $content->user;
        } else {
            $user = Auth::user();
        }

        $categories = $user->categories()->get(['id', 'name']);
        $channels = $user->channels()->get(['id', 'name']);

        $categories_selected = [];
        $channels_selected = [];
        if($content) {
            $categories_selected = $content->categories()->pluck('category_id')->toArray();
            $channels_selected = $content->channels()->pluck('channel_id')->toArray();
        }

        return view('upload')->with([
            'content'  =>  $content,
            'user' => $user,

            'categories'  =>  $categories,
            'channels'  =>  $channels,

            'categories_selected' => $categories_selected,
            'channels_selected' => $channels_selected,
        ]);

    }

    public function getAll(Request $request)
    {
        $userId = $request->get('user');
        $rating = $request->get('rating');
        $categoryIds = $request->get('cat');
        $search = $request->get('search');

        $user = Auth::user();

        $selectedUser = null;
        $selectedCategories = [];

        $query = null;

        if (empty($categoryIds)) {
            $query = Content::select('*');
        } else {
            $selectedCategories = Category::whereIn('id', $categoryIds)->get()->toArray();
            $query = Content::distinct()
                    ->select('contents.*')
                    ->join('content_categories', 'contents.id', '=', 'content_categories.content_id')
                    ->join('categories', 'content_categories.category_id', '=', 'categories.id')
                    ->join('users', 'contents.user_id', '=', 'users.id')
                    ->whereIn('categories.id', $categoryIds);
        }

        if (!empty($userId)) {
            $selectedUser = User::where('id', $userId)->first();
            $query->where('contents.user_id', $selectedUser->id);
        } else if (!$user->isAdmin()) {
            $query->where('contents.user_id', $user->id);
        }
        if(!empty($search)) {
            $query->where('contents.title', 'like', '%'.$search.'%');
        }
        $contents = $query->orderBy('contents.id', 'desc')->paginate(10);

        foreach ($contents as $key => $content) {

            $content->explicit = $content->isExplicit();
            $content->can_push = $content->canPush();
            $content->username = $content->user->name;
            unset(
                $content->user, 
                $content->description, 
                $content->resource_uri, 
                $content->mime,
                $content->extension,
                $content->asset_path,
                $content->duration,
                $content->publish_at,
                $content->unpublish_at,
                $content->parent_id,
                $content->user_id
            );
            if (! empty($rating) && $content->explicit === false) {
                unset($contents[$key]);
            }

        }

        return view('uploads')->with([
            'list' => $contents->appends(Request::capture()->except(['page'])),
            'selectedUser' => $selectedUser,
            'selectedCategories' => $selectedCategories,
            'params' => Request::capture()->all(),
            'active' => 'videos'
        ]);
    }

    public function getPushToChannel($contentUid)
    {
        $content = Content::where('uid', $contentUid)->first();

        $channels = $content->channels;

        $pages = $content->channels()->with('user')->paginate(10);
        foreach ($pages as $channel) {
            $channel->content_status = $channel->contentStatus($content);
        }

        return view('push_to_channel')->with([
            'active'    =>  'channels',
            'content'   =>  $content,
            'list'  =>  $pages
        ]);
    }

    /**
     * About Video
     * 
     * Returns information about a video.
     * 
     * @param  string $uid
     * @return json
     */
    public function getAbout($uid)
    {
        $transformer = new ContentTransformer;
        $content = Content::where('uid', $uid)->first();

        return $this->success([
            'data'  =>  $transformer->transform($content, $this->getDevice())
        ]);
    }

    /**
     * Search
     * 
     * Search content.
     * 
     * @param  [type] $uid [description]
     * @return [type]      [description]
     */
    public function postSearch(Request $request)
    {
        $contents = Content::search($request->term)->get();

        return $this->success([
            'data'  =>  $contents
        ]);
    }

    public function postSearch2(Request $request)
    {
        $fields = ['contents.id','contents.uid','contents.title', 'contents.asset_path'];
        $contents = [];

        $query = Content::distinct()
                    ->select($fields)
                    ->join('content_categories', 'contents.id', '=', 'content_categories.content_id')
                    ->join('categories', 'content_categories.category_id', '=', 'categories.id')
                    ->join('users', 'contents.user_id', '=', 'users.id')
                    ->where('contents.title', 'like', '%'.$request->searchkey.'%' );

        if (!empty($request->user) && $request->user > 0) {
            $query->where('users.id', $request->user);
        }

        if (!empty($request->categories) && $request->categories > 0) {
            $query->whereIn('categories.id', $request->categories);
        }

        $contents = $query->get();

        return response()->json($contents);
    }

    public function postContent(UploadContent $request, $uid = null)
    {
        // $upload = $request->file('content');
        $content = Content::where('uid', $uid)->first();

        $uid = ($content) ? $content->uid : Generate::noDashUid();
        
        // if (!empty($upload)) {
        //     $uploadFileName = $uid . '.' . $upload->getClientOriginalExtension();
        //     $s3 = Storage::disk('s3');
        //     $filePath = '/content/' . $uploadFileName;
        //     $s3->put($filePath, file_get_contents($upload), 'public');
        // }

        // check if the file has been uploaded properly
        
        $expiry = date('Y-m-d H:i:00', strtotime($request->expires_at));
        if ($content) {
            $action = 'updated';

            $content->title = $request->title;
            $content->description = $request->description;
            $content->asset_path = $request->asset_path;
            $content->expires_at = $expiry;
            $content->resource_uri = Storage::disk('s3')->url($request->filename);

            if (!empty($upload)) {
                $content->extension =  '-';
                $content->mime  =  $request->mimetype;
            }
            $content->save();

        } else {
            $action = 'created';

            $content = Content::create([
                'uid'   =>  $uid,
                'title' =>  $request->title,
                'resource_uri'  =>  Storage::disk('s3')->url($request->filename),
                'description'  =>  $request->description,
                'asset_path'  =>  $request->asset_path,
                'user_id'   =>  Auth::user()->id,
                'expires_at' => $expiry,
                'extension' => '-',
                'mime'  =>  $request->mimetype,
            ]);
        }

        $content->categories()->sync($request->categories);
        $content->channels()->sync($request->channels);

//        if ($action === 'created') {
//            event(new ModelCreated($content));
//        } else {
//            event(new ModelUpdated($content));
//        }

        return redirect()->route('content.page');
    }

    public function removeContent(Request $request)
    {
        $uids = $request->uids;
        $contents = Content::whereIn('uid', $uids)->get();

        foreach ($contents as $v) {
            $v->categories()->detach();
            $v->channels()->detach();
            $v->devices()->detach();
            $v->tags()->detach();
            $v->opinions()->detach();
            $v->bookmarks()->detach();
            //$v->views()->detach();

            $v->delete();
        }
        return redirect()->route('content.page');
    }

    public function getInfo(Request $request)
    {
        $content = Content::where('id', $request->id )->first(['id','uid','title', 'asset_path']);
        return response()->json($content);
    }
}
