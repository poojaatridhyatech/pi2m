<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use App\Http\Requests\RequestDemo;
use App\Mail\RequestDemo as RequestDemoMail;


class MailController extends Controller
{
    public function requestDemo(RequestDemo $request)
    {
        Mail::to('info@pushit2mobile.com ')
            ->send( new RequestDemoMail($request->name, $request->email, $request->message) );

        return redirect()->to('/#contact')->with([
            'demo_requested'    =>  true
        ]);
    }
}
