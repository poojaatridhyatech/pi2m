<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Device;
use App\Http\Requests\PostDevice;
use App\Routines\Crud\Create\Device as CreateDevice;
use App\Routines\Crud\Delete\Device as DeleteDevice;
use App\Routines\Crud\Update\Device as UpdateDevice;


class DeviceController extends Controller
{
    
    public function getEdit($uid = null)
    {
        $device = Device::where('uid', $uid)->first();

        return view('device')->with([
            'device'  =>  $device,
        ]);
    }
    
    public function getAll($channelUid = null)
    {
        $devices = Device::orderBy('id', 'desc')->paginate(10);

        return view('devices')->with([
            'list'  =>  $devices,
        ]);
    }

    public function post(PostDevice $request, $uid = null)
    {
        $device = $this->createOrUpdate(Device::all(), $uid);
        if ($device) {
            new UpdateDevice($device,$request);
        } else {
            new CreateDevice($request);
        }

        return redirect()->route('devices.page');
    }

    public function delete(Request $request)
    {
        $uids = $request->uids;

        $devices = Device::whereIn('uid', $uids)->get();
        foreach ($devices as $dev) {
            new DeleteDevice($dev);
        }

        return redirect()->route('devices.page');
    }

}
