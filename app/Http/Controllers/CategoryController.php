<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Generate;
use App\User;
use App\Category;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Requests\PostCategory;
use App\Routines\Crud\Create\Category as CreateCategory;
use App\Routines\Crud\Update\Category as UpdateCategory;
use App\Routines\Crud\Delete\Category as DeleteCategory;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller
{
    /**
     * Edit a single instance
     *
     * @param string $uid
     * @return view
     */
    public function getEdit($uid)
    {
        $user = null;
        $category =  Category::where('uid', $uid)->first();

        if ($category) {
            $user = $category->user;
        } else {
            $user = Auth::user();
        }

        $videos = $user->contents()->with([
            'channels' => function($q) { $q->select('channel_id');},
        ])->get(['id',/*'uid',*/ 'title']);

        $videos_selected = [];
        if ($category) {
            $videos_selected = $category->contents()->pluck('content_id')->toArray();
        }

        return view('category')->with([
            'category'  =>  $category,
            'user' => $user,

            'videos'  =>  $videos,

            'videos_selected' => $videos_selected,
        ]);
    }

    /**
     * Return all avialable instances
     *
     * @return view
     */
    public function getAll(Request $request)
    {
        $userId = $request->get('user');
        $user = Auth::user();
        
        $selectedUser = null;
        $categories;

        if (empty($userId)) {
            if ($user->isAdmin()) {
                $categories = Category::orderBy('id', 'desc')->paginate(10);
            } else {
                $categories = $user->categories()->orderBy('id', 'desc')->paginate(10);
            }
        } else {
            $selectedUser = User::where('id', $userId)->first();
            $categories = $selectedUser->categories()->orderBy('id', 'desc')->paginate(10);
        }

        foreach ($categories as $cat) {
            //$cat->explicit = $cat->isExplicit();
            //$cat->formatted_created_at = date('d-M-y', strtotime($cat->created_at));
            //$cat->formatted_updated_at = $cat->updated_at->diffForHumans();
            $cat->username = $cat->user->name;
            unset($cat->user);
        }

        return view('categories')->with([
            //'list' =>  $categories,
            //'list' => $categories->appends(Input::except(['page'])),
            'list' => $categories->appends(Request::capture()->except(['page'])),
            'selectedUser' => $selectedUser,
        ]);
    }

    /**
     * Return a list of categories for the currently logged in user.
     *
     * @return JsonResponse
     */
    public function getList()
    {
        $user = Auth::user();

        if ($user->isAdmin()) {
            $categories = Category::orderBy('name')->get(['id', 'uid', 'name']);
        } else {
            $categories = $user->categories()->orderBy('name')->get(['id', 'uid', 'name']);
        }

        return response()->json($categories);
    }

    /**
     * View videos of one category
     *
     * @param string $uid
     * @return view
     */
    public function viewVideos($uid)
    {
        $user = Auth::user();

        $category =  Category::where('uid', $uid)->first();
        $contents = $category->contents()->orderBy('id', 'desc')->paginate(10);

        foreach ($contents as $v) {
            $v->explicit = $v->isExplicit();
            $v->can_push = $v->canPush();
            $v->username = $v->user->name;
            unset(
                $v->user,
                $v->description, 
                $v->resource_uri, 
                $v->mime,
                $v->extension,
                $v->asset_path,
                $v->duration,
                $v->publish_at,
                $v->unpublish_at,
                $v->parent_id,
                $v->user_id
            );
        }

        return view('category_view')->with([
            'category'  =>  $category,
            'list'  =>  $contents,
            'active' => 'videos'
        ]);
    }

    public function viewAds($uid)
    {
        $user = Auth::user();

        $category =  Category::where('uid', $uid)->first();
        $ads = $category->ads()->orderBy('id', 'desc')->paginate(10);

        foreach ($ads as $v) {
            //$ch->explicit = $ch->isExplicit();
            $v->username = $v->user->name;
            unset($v->user);
        }

        return view('category_view')->with([
            'category'  =>  $category,
            'list'  =>  $ads,
            'active' => 'ads'
        ]);
    }

    /**
     * Create or Edit an instance
     *
     * @param PostCategory $request
     * @param string $uid [if not specified, new will be created]
     * @return redirect
     */
    public function post(PostCategory $request, $uid = null)
    {
        $user = Auth::user();
        $category = null;
        
        if ($user->isAdmin()) {
            $category = $this->createOrUpdate(Category::all(), $uid);
        } else {
            $category = $this->createOrUpdate($user->categories(), $uid);
        }

        if ($category) {
            new UpdateCategory($category, $request);
        } else {
            new CreateCategory($request);
        }

        return redirect()->route('categories.page');
    }

    /**
     * Store a newly created category in storage (used for inline creation).
     *
     * @param PostCategory $request
     * @return ResponseFactory|Response
     */
    public function store(PostCategory $request)
    {
        $category = new CreateCategory($request);
        $cat = Category::where('uid', $category->category->uid)->get(['id', 'uid', 'name']);

        return response()->json($cat[0]);
    }

    /**
    * Delete an instance
    *
    * @param 
    * @return redirect
    */
    public function delete(Request $request)
    {
        $uids = $request->uids;
        //$categories = Auth::user()->categories()->whereIn('uid', $uids)->get();
        $categories = Category::whereIn('uid', $uids)->get();

        foreach ($categories as $cat) {
            new DeleteCategory($cat);
        }

        return redirect()->route('categories.page');
    }
    
    public function dissociate(Request $request, $cat, $type)
    {
        $uids = $request->uids;

        $category = Category::where('uid', $cat)->first();
        //dd($request);

        if (empty($category)) {
            return redirect()->route('categories.'.$type, $cat );
        }

        if ($type == 'videos')
        {
            $ids = $category->contents()->wherein('uid', $uids)->pluck('content_id')->toArray();
            $category->contents()->detach($ids);

        } else if ($type == 'ads')
        {
            $ids = $category->ads()->wherein('uid', $uids)->pluck('ads_id')->toArray();
            $category->ads()->detach($ids);
        }
        return redirect()->route('categories.'.$type, $cat);
    }

    public function postSearch(Request $request)
    {
        $categories = [];
        $fields = ['id','uid','name', 'asset_path'];

        if (!empty($request->user) && $request->user > 0) {
            $selectedUser = User::where('id', $request->user)->first();
            $categories = $selectedUser->categories()->where('name', 'like', '%'.$request->searchkey.'%' )->get($fields);
        } else {
            $categories = Category::where('name', 'like', '%'.$request->searchkey.'%' )->get($fields);
        }

        return response()->json($categories);
    }

    public function getInfo(Request $request)
    {
        $category = Category::where('id', $request->id )->first(['id','uid','name', 'asset_path']);
        return response()->json($category);
    }

}

