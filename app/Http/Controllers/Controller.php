<?php

namespace App\Http\Controllers;

use App\User;
use App\Client;
use App\Device;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @return User|null
     */
    protected function getUser()
    {
        return User::where('uid', session()->get('user'))->first();
    }

    protected function getDevice()
    {
        return Device::where('uid', session()->get('device'))->first();
    }

    protected function getClient()
    {
        return Client::where('uid', session()->get('client'))->first();
    }

    protected function success($data = [])
    {
        if ( empty($data) ) {
            return $this->success([
                'success'   =>  true,
            ]);
        }

        return response()->json($data);
    }

    protected function failure($data, $code = 400)
    {
        if (empty($data)) {
            return response()->json([
                'success'   =>  false,
            ], $code);
        }

        return response()->json($data, $code);

    }

    protected function createOrUpdate($query, $uid)
    {
        if ($uid) {
            return $query->where('uid', $uid)->first();
        }

        return;
    }

    protected function getFromCheckList($key, $request, $model = null)
    {
        if ( ! $model ) {
            $className = '\App\\' . ucfirst(str_singular($key));
            $model = new $className;
        } else {
            $model = new $model;
        }
        
        $uids = [];
        // handle attaching to clients
        foreach ($request->all() as $key => $field) {
            if (starts_with($key, 'client')) {
                $exp = explode('_', $key);
                $uid = $exp[1];
                
                $uids[] = $uid;
            }
        }

        $clients = $model->whereIn('uid', $uids)->pluck('id')->toArray();

        return $clients;
    }
}
