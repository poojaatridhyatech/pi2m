<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeleteMultipleByUids;
use App\Http\Requests\PostUser;
use App\Http\Requests\PutUser;
use App\Http\Requests\ForgotPasswordReq;
use Auth;
use Mail;
use App\Routines\Crud\Create\User as CreateUser;
use App\Routines\Crud\Update\User as UpdateUser;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use App\User;
use App\Role;
use App\Activation;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Mail\UserCreated;
use App\Mail\ForgotPassword;

class UserController extends Controller
{
    protected $perPage = 20;

    /**
     * Display a listing of the users.
     *
     * @return Factory|View
     */
    public function index()
    {
        $user = Auth::user();

        if (!$user->isAdmin()) {
            return redirect()->back();
        }

        return view('users.index');
    }

    /**
     * Get the list of users depending on the currently logged in user.
     *
     * @return JsonResponse
     */
    public function listing()
    {
        $user = Auth::user();
        $users = [];

        if ($user->isAdmin()) {
            $users = User::with(['roles'])->orderBy('name')->get();

            foreach ($users as $user) {
                $user->active = $user->status ? __('misc.label.active') : __('misc.label.deactivated');
            }
        }

        return response()->json($users);
    }

    /**
     * Show the form for creating a new user.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('users.create', [
            'roles' => Role::where('name', '!=', Role::ROLE_USER)->get(),
        ]);
    }

    /**
     * Store a newly created user in storage.
     *
     * @param PostUser $request
     * @return RedirectResponse
     */
    public function store(PostUser $request)
    {
        new CreateUser($request);

        return redirect()->route('users.index');
    }

    /**
     * Get user by ID.
     *
     * @param User $user
     * @return JsonResponse
     */
    public function user(User $user)
    {
        if (Auth::user()->isAdmin()) {
            return response()->json($user);
        }

        try {
            // if it's a new user then email them the activation link
            // also create a default client for them
            if ($new === true) {
                Client::create([
                    'name' => 'default',
                    'description' => 'Default client',
                    'uid'   =>  Generate::noDashUid(),
                    'secret'    =>  Generate::secret(),
                    'user_id'   =>  $user->id,
                ]);

                // also create a default category
                Category::create([
                    'name'  =>  'default',
                    'description'   =>  'default category',
                    'user_id'   =>  $user->id,
                    'uid'   =>  Generate::noDashUid(),
                    'asset_path'    =>  '',
                ]);

                // also create a default channel
                Channel::create([
                    'name'  =>  'default',
                    'label'  =>  'default',
                    'description'   =>  'default channel',
                    'user_id'   =>  $user->id,
                    'uid'   =>  Generate::noDashUid(),
                    'asset_path'    =>  '',
                ]);


                Mail::to($request->email)->send(new UserCreated($user));
            }
        } catch (Exception $e) {
            Log::error(Generate::usefulExceptionMessage($e));
            return response()->json(['error' => 'Not Found'], 404);
        }
    }

    /**
     * Show the form for editing the specified user.
     *
     * @param User $user
     * @return Factory|View
     */
    public function edit(User $user)
    {
        return view('users.edit', [
            'user' => $user,
            'roles' => Role::where('name', '!=', Role::ROLE_USER)->get(),
            'activeRoles' => $user->roles()->pluck('id')->toArray(),
        ]);
    }

    /**
     * Update the specified user in storage.
     *
     * @param PutUser $request
     * @param User $user
     * @return RedirectResponse
     */
    public function update(PutUser $request, User $user)
    {
        new UpdateUser($user, $request);

        return redirect()->route('users.index');
    }

    /**
     * Remove the specified user from storage.
     *
     * @param User $user
     * @return RedirectResponse
     */
    public function destroy(User $user)
    {
        $user->delete();

        return response(null, 204);
    }

    /**
     * Remove multiple users by the specified UUIDs.
     *
     * @param DeleteMultipleByUids $request
     * @return ResponseFactory|Response
     */
    public function destroyMulti(DeleteMultipleByUids $request)
    {
        $uids = $request->get('uids');

        foreach ($uids as $uid) {
            User::where('uid', $uid)->delete();
        }

        return response(null, 204);
    }

    public function postChangePassword(Request $request)
    {
        if ($request->password === $request->password_verify) {
            $user = Auth::user();
            $user->password = bcrypt($request->password);
            $user->save();
        }

        return redirect()->back();
    }

    public function getActivate(Request $request)
    {
        $activation = Activation::where('hash', $request->query('token'))->first();
        if ( ! $activation ) {
            abort('404');
        }

        return view('users.activate')->with([
            'hash'  =>  $activation->hash,
            'activate'  =>  $request->get('activate')
        ]);
    }

    public function postActivate(Request $request)
    {
        $activation = Activation::where('hash', $request->query('token'))->first();
        if ( ! $activation ) {
            abort('404');
        }

        if ($request->password === $request->password_verify) {
            $user = $activation->user;
            $user->password = bcrypt($request->password);
            $user->save();

            $activation->delete();
        } else {
            return redirect()->back()->with([
                'passwords_dont_match'    =>  ['Passwords dont match'],
            ]);
        }

        return redirect()->to('login');
    }

    public function postReset(ForgotPasswordReq $request)
    {
        try {
            $user = User::where('email', $request->email)->first();

            if ($user) {
                $user->password = '';
                $user->save();

                Mail::to($user->email)->send(new ForgotPassword($user));

                return redirect()->back()->with([
                    'success'   =>  true
                ]);
            }

        } catch (\Eception $e) {
            return redirect()->back()->with([
                'success'   =>  false
            ]);

        }
        
    }

    public function postSearch(Request $request)
    {
        $users = User::where('name', 'like', '%'.$request->searchkey.'%' )->get(['id','uid','name', 'asset_path']);
        return response()->json($users);
    }

    public function getUserInfo(Request $request)
    {
        $user = User::where('id', $request->id )->first(['id','uid','name', 'asset_path']);
        return response()->json($user);
    }

}
