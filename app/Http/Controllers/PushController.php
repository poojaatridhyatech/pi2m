<?php

namespace App\Http\Controllers;

use Log;
use Auth;
use Extract;
use Generate;
use App\Push;
use Exception;
use Validator;
use App\Device;
use App\Channel;
use App\Content;
use Carbon\Carbon;
use App\Routines\CanPush;
use App\Events\PushContent;
use Illuminate\Http\Request;
use App\Routines\SchedulePush;
use App\Http\Requests\RecallPush;
use App\Http\Requests\PushToDevice;
use App\Events\RecallPush as RecallPushEvent;

class PushController extends Controller
{
    public function postRecallPush(RecallPush $request)
    {
        $content = Content::where('uid', $request->content_uid)->first();
        $channel = Channel::where('uid', $request->channel_uid)->first();

        $pushes = Push::where('channel_id', $channel->id)->where('content_id', $content->id)->get();

        event(new RecallPushEvent($pushes));

        return $this->success([
            'queued'    =>  count($pushes)
        ]);
    }

    public function postPushContent(PushToDevice $request, $uid)
    {
        $content = Content::where('uid', $uid)->first();
        $uids = [];

        foreach ($request->all() as $key => $field) {
            if ( starts_with($key, 'channel') ) {
                $exp = explode('_', $key);
                $uid = $exp[1];
                $uids[] = $uid;
            }
        }

        $channels = Channel::whereIn('uid', $uids)->get();

        if ($request->device_uid) {
            $device = Device::where('onesignal_uid', $request->device_uid)->first();

            if (!$device) return 'Failed to send. Device not found';

            $distribute = new \App\Routines\DistributeContent($content);
            $distribute->toDevice($device);

            return 'sent to single device';
        }

        $devicesToSendTo = [];
        $cc = 0;
        foreach ($channels as $channel) {
            $distribute = new \App\Routines\DistributeContent($content);
            $res = $distribute->toChannel($channel);

            $cc++;
        }

        return 'sent to ' . $cc . ' channels';
    }

    /**
        * Primary push endpoint
        *
        * @param PushToDevice $request
        * @return json
    */
    public function postPushToDevices(PushToDevice $request)
    {
        try {
            $user = Auth::user();

            $content = $user->contents()->where('uid', $request->content_uid)->first();

            if ($request->onesignal_uid) {
                if (strlen($request->onesignal_uid) !== 36) {
                    return response()->json(['errors' => [['Please enter correct Onesignal User UUID']]], 400);
                }
                

                // push to device
                $device = Device::where('onesignal_uid', $request->onesignal_uid)->first();
                if ($content && $device) {

                    if ( $request->expiry_time ) {
                        $value = (int) $request->expiry_time;
                        $now = new Carbon();
                        switch ($request->expiry_time_value) {
                            case 'minutes':
                                $now->addMinutes($value);
                            break;

                            case 'hours':
                                $now->addHours($value);
                            break;

                            case 'days':
                                $now->addDays($value);

                            break;
                        }
                    } else {
                        if ($request->expires_at_date) {
                            $now = $request->expires_at_date;
                        } else {
                            $c = new Carbon();
                            $now = $c->addDays(2);
                        }
                        
                    }

                    $content->expires_at = date('Y-m-d H:i:s', strtotime($now));
                    $content->save();

                    $schedule = new SchedulePush($content, $device, $request->notification);
                    $schedule->at(null); // send immediately
                    \Log::info('Device: ' . $device->onesignal_uid);
                    event( new PushContent( $content, $device, $schedule->getPush() ) );

                    return $this->success([
                        'queued'    =>  1
                    ]);
                }
            } else {

                // push to channel
                $channels = Channel::whereIn('uid', $request->channels)->get();

                if ($content && (count($channels) > 0) ) {
                    
                    if ($request->expiry_time) {
                        $value = (int) $request->expiry_time;
                        $now = new Carbon();
                        switch ($request->expiry_time_value) {
                            case 'minutes':
                                $now->addMinutes($value);
                            break;
                            case 'hours':
                                $now->addHours($value);
                            break;
                            case 'days':
                                $now->addDays($value);
                            break;
                        }
                    } else {
                        if ($request->expires_at_date) {
                            $now = $request->expires_at_date;
                        } else {
                            $c = new Carbon();
                            $now = $c->addDays(2);
                        }
                        
                    }


                    $content->expires_at = date('Y-m-d H:i:s', strtotime($now));
                    $content->save();

                    foreach ($channels as $channel) {
                        $subscribers = $channel->subscribers()->where('subscribed', true)->get();
                        $cc = 0;
                        foreach ($subscribers as $sub) {
                            $schedule = new SchedulePush($content, $sub);
                            if ($request->push_at) {
                                $schedule->at( date('Y-m-d H:i:s', strtotime($request->push_at)) );
                            } else {
                                $schedule->at( null ); // send immediately
                                
                                event(new PushContent($content, $sub, $schedule->getPush()));
                            }
                            $push = $schedule->getPush();
                            $push->channel_id = $channel->id;
                            $push->save();

                            $cc++;
                        }
                    }

                    return $this->success([
                        'queued'    =>  $cc
                    ]);
                }
            }

            return response()->json(['errors' => [['Content or device not found']]], 400);

        } catch(Exception $e) {
            Log::error('ERROR-WHEN-PUSHING. ' . Generate::usefulExceptionMessage($e));

            return $this->failure(['error' => $e->getMessage()], 500);
        }
             
    }

    public function getPushContent($uid)
    {
        $content = Content::where('uid', $uid)->first();

        return view('manage_push')->with([
            'content'   =>  $content
        ]);
    }
}
