<?php

namespace App\Http\Controllers\Api;

use App\Stat;
use App\Content;
use App\Channel;
use Illuminate\Http\Request;
use App\Http\Requests\PostedStats;
use App\Http\Controllers\Controller;

class StatController extends Controller
{
    public function post(PostedStats $request, $contentUid)
    {
        $device = $this->getDevice();

        $channel = Channel::where('uid', $request->channel_id)->first();
        $content = Content::where('uid', $contentUid)->first();

        $stat = Stat::create([
            'device_id' =>  $device->id,
            'content_id'    =>  $content->id,
            'channel_id'    =>  ($channel) ? $channel->id : null,
            'data'  =>  json_encode($request->data),
        ]);

        return $this->success();
    }
}
