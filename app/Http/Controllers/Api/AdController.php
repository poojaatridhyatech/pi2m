<?php

namespace App\Http\Controllers\Api;

use App\Wishlist;
use Auth;
use App\Content;
use App\Ad;
use App\AdClick;
use App\AdClickType;
use App\Http\Requests\Wish;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Transformers\Ad as AdTransformer;
use Carbon\Carbon;

/**
 * @resource Ads
 */
class AdController extends Controller
{
    /**
     * @var App\Transformers\Ad
     */
    protected $adTransformer;

    public function __construct(
        AdTransformer $adTransformer
    ) {
        $this->adTransformer  = $adTransformer;
    }

    /**
     * Returns all ads that are associated with the device
     *
     * @return json
     */
    public function getWishes()
    {
        $device = $this->getDevice();

        return $this->success([
            'data'  =>  $this->adTransformer->transformMultiple($device->wishes)
        ]);
    }

    /**
     * Returns all ads that are associated with the content
     *
     * @param string $uid
     * @return json
     */
    public function getPerVideo($uid)
    {
        $content = Content::where('uid', $uid)->first();

        return $this->success([
            'data'  =>  $this->adTransformer->transformMultiple($content->ads_array())
        ]);
    }

    /**
     * Add to wishlist
     *
     * @param Wish $request
     * @param string $uid
     * @param string $aduid
     * @return JsonResponse
     */
    public function postWishlist(Wish $request, $uid, $aduid)
    {
        $device = $this->getDevice();
        $ad = Ad::where('uid', $aduid)->first();

        Wishlist::create([
            'device_id' => $device->id,
            'ad_id' => $ad->id,
        ]);

        return $this->success();
    }

    /**
     * Record clicks on ads
     *
     * @param  Request $request [description]
     * @param  string  $ads_uid     [description]
     * @return json
     */
    public function postclick(Request $request, $ads_uid)
    {
        $device = $this->getDevice();
        $client = $this->getClient();
        $ad = Ad::where('uid', $ads_uid)->first();
        
        $click_type_key = $request->clickType;
        $click_type = AdClickType::where('key', $click_type_key)->first();

        if (!empty($ad) && !empty($device) && !empty($client) && !empty($click_type)) {

            $stat = AdClick::create([
                'ad_id' => $ad->id,
                'client_id' => $client->id,
                'device_id' => $device->id,
                'click_type' => $click_type->id,
            ]);

            return $this->success();
        }
        return $this->error([
            'status'  =>  'error',
            'message' => 'wrong information'
        ]);
    }
}
