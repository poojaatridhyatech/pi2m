<?php

namespace App\Http\Controllers\Api;

use Log;
use Auth;
use Extract;
use Generate;
use App\Push;
use Exception;
use OneSignal;
use App\Content;
use App\ViewType;
use App\Routines\CanPush;
use App\Events\PushContent;
use Illuminate\Http\Request;
use App\Routines\PostOpinion;
use App\Http\Requests\PostView;
use App\Routines\TrendingContent;
use App\Http\Requests\LikeContent;
use App\Routines\RecommendContent;
use App\Routines\DistributeContent;
use App\Http\Requests\AboutContent;
use App\Http\Requests\SearchContent;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContentDownloaded;
use App\Http\Requests\PostRequestContent;
use App\Transformers\Content as ContentTransformer;

/**
 * @resource Channels
 */
class ContentController extends Controller
{
    protected $contentTransformer;

    public function __construct(ContentTransformer $contentTransformer)
    {
        $this->contentTransformer = $contentTransformer;
    }

    /**
     * About Video
     *
     * Returns information about a video.
     *
     * @param  string $uid
     * @return json
     */
    public function getSingle($uid)
    {
        $content = Content::where('uid', $uid)->first();

        return $this->success([
            'data'  =>  $this->contentTransformer->transform($content, $this->getDevice())
        ]);
    }

    /**
     * Get information about multiple instances
     *
     * @param AboutContent $request
     * @return json
     */
    public function postAbout(AboutContent $request)
    {
        $resp = [];
        $contents = Content::whereIn('uid', $request->uids)->get();

        foreach ($contents as $content) {
            $resp[] = $this->contentTransformer->transform($content, $this->getDevice());
        }

        return $this->success([
            'data'  =>  $resp
        ]);
    }

    /**
     * Search
     *
     * Search content.
     *
     * @param  [type] $uid [description]
     * @return [type]      [description]
     */
    public function search(SearchContent $request)
    {
        $contents = Content::search($request->term)->get();

        return $this->success([
            'data'  =>  $contents
        ]);
    }

    /**
     * Create a view instance for the video
     *
     * @param PostView $request
     * @param string $uid
     * @return json
     */
    public function postView(PostView $request, $uid)
    {
        $device = $this->getDevice();

        $content = Content::where('uid', $uid)->first();

        if ($content) {
            if ($request->type === ViewType::watchLater()->name) {
                $content->watchLater($device);
            }
            
            if ($request->type === ViewType::finished()->name) {
                $content->watched($device);
            }


            return $this->success();
        }

        return $this->failure([
            'success'   =>  false,
            'error'     =>  'Video not found'
        ], 404);
    }

    /**
     * Bookmark
     *
     * Bookmark content.
     *
     * @param  Request $request [description]
     * @param  [type]  $uid     [description]
     * @return [type]           [description]
     */
    public function postBookmark(Request $request, $uid)
    {
        $device = $this->getDevice();
        $content = Content::where('uid', $uid)->first();

        if ($device->bookmarked($content)) {
            $device->bookmarks()->detach($content->id);
        } else {
            $device->bookmarks()->attach($content->id);
        }

        return $this->success();
    }

    /**
     * Opinion
     *
     * Post an opinion, like or dislike content.
     *
     * @param  Request $request
     * @param  string  $uid
     * @return json
     */
    public function postOpinion(LikeContent $request, $uid)
    {
        $device = $this->getDevice();
        $content = Content::where('uid', $uid)->first();

        // handle the opinion
        new PostOpinion($device, $content, $request->like);

        return $this->success();
    }

    /**
     * Download Finished
     *
     * Confirm that a certain video has been downloaded to the device.
     *
     * @param  ContentDownloaded $request
     * @param  string  $contentUid
     * @return json
     */
    public function downloaded(ContentDownloaded $request, $contentUid)
    {
        $content = Content::where('uid', $contentUid)->first();
        $device = $this->getDevice();

        if (! $request->send_to) {
            return $this->failure([
                'error' =>  'send_to paramter missing'
            ], 400);
        }
        // send a PN
        if (env('APP_ENV') !== 'testing') {
            try {
                OneSignal::sendNotificationToUser(
                    'New Content Available!',
                    $request->send_to,
                    $url = null,
                    $data = [],
                    $buttons = null,
                    $schedule = null
                );
            } catch (Exception $e) {
                Log::error(Generate::usefulExceptionMessage($e));
            }
        }

        // store the "push" / download
        $p = Push::where('device_id', $device->id)->where('content_id', $content->id)->first();
        if ($p) {
            $p->pushed = true;
            $p->save();
        } else {
            $p = Push::create([
                    'device_id' =>  $device->id,
                    'content_id'    =>  $content->id,
                    'pushed'    =>  true,
                ]);

        }
        
        return $this->success();
    }

    /**
     * Video Deleted
     *
     * Mark the video as deleted.
     *
     * @param  string  $contentUid
     * @return json
     */
    public function deleted($contentUid)
    {
        $content = Content::where('uid', $contentUid)->first();
        $device = $this->getDevice();

        // mark the push as deleted
        $push = Push::where('content_id', $content->id)->where('device_id', $device->id)->first();
        if ($push) {
            $push->deleted = true;
            $push->save();
        }

        return $this->success();
    }

    /**
     * Return recommended videos based on the 
     * users previously watched videos
     *
     * @return json
     */
    public function getRecommended()
    {
        $resp = [];
        $device = $this->getDevice();
        $client = $this->getClient();

        $recommendations = new RecommendContent($client, $device);
        $contents = $recommendations->get();

        foreach ($contents as $content) {
            $resp[] = $this->contentTransformer->transform($content, $device);
        }

        return $this->success([
            'data'  =>  $resp
        ]);
    }

    /**
     * Push recommended videos to the device
     *
     * @param Request $request
     * @return json
     */
    public function postRecommended(Request $request)
    {
        $report = [
            'queued'    =>  0,
            'failed'  =>  0,
        ];
        $device = $this->getDevice();
        $client = $this->getClient();
        $recommendations = new RecommendContent($client, $device);
        $contents = $recommendations->get($request->qty);

        foreach ($contents as $content) {

            if ( ! $device ) {
                throw new Exception('Device not found.');
            }

            event( new PushContent($content, $device, $client) );

            $report['queued']++;
        }

        return $this->success([
            'success'   =>  true,
            'data'  =>  $report
        ]);
    }

    public function getTrending()
    {
        $resp = [];
        $device = $this->getDevice();
        $client = $this->getClient();

        $routine = new TrendingContent($client, $device);
        $contents = $routine->get();

        foreach ($contents as $content) {
            $resp[] = $this->contentTransformer->transform($content, $device);
        }

        return $this->success([
            'data'  =>  $resp
        ]);

    }

    /**
     * Push trending videos to the device
     *
     * @param Request $request
     * @return json
     */
    public function postTrending(Request $request)
    {
        $report = [
            'queued'    =>  0,
            'failed'  =>  0,
        ];
        $device = $this->getDevice();
        $client = $this->getClient();
        $trending = new TrendingContent($client, $device);
        $contents = $trending->get($request->qty);

        foreach ($contents as $content) {

            if ( ! $device ) {
                throw new Exception('Device not found.');
            }

            event( new PushContent($content, $device, $client) );

            $report['queued']++;
        }

        return $this->success([
            'success'   =>  true,
            'data'  =>  $report
        ]);
    }

    public function postRequestContent(PostRequestContent $request)
    {
        $report = [
            'queued'    =>  0,
            'failed'  =>  0,
            'not_sending'  =>  0,
        ];

        $device = $this->getDevice();
        $client = $this->getClient();
        $contents = Content::whereIn('uid', $request->uids)->get();

        foreach ($contents as $content) {
            $dist = new DistributeContent($content);
            $dist->toDevice($device);

            if (isset($report[$dist->status()])) {
                $report[$dist->status()]++;
            }
        }

        return $this->success([
            'success'   =>  true,
            'data'  =>  $report
        ]);

    }
}
