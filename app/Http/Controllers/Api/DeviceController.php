<?php

namespace App\Http\Controllers\Api;

use App\Device;
use Illuminate\Http\Request;
use App\Http\Requests\PostSettings;
use App\Http\Controllers\Controller;

class DeviceController extends Controller
{
    public function postSettings(PostSettings $request)
    {
        $device = $this->getDevice();
        $device->settings = json_encode($request->settings);
        $device->save();

        return $this->success();
    }
}
