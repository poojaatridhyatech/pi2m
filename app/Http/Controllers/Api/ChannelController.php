<?php

namespace App\Http\Controllers\Api;

use Auth;
use Extract;
use App\Client;
use App\Channel;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StopChannelStream;
use App\Transformers\Channel as ChannelTransformer;
use App\Transformers\Category as CategoryTransformer;
use App\Routines\Crud\Create\Channel as CreateChannel;
use App\Routines\Crud\Update\Channel as UpdateChannel;
use App\Routines\Crud\Delete\Channel as DeleteChannel;

/**
 * @resource Channels
 */
class ChannelController extends Controller
{
    /**
     * @var App\Transformers\Channel
     */
    protected $channelTransformer;

    /**
     * @var App\Transformers\Category
     */
    protected $categoryTransformer;

    public function __construct(
        ChannelTransformer $channelTransformer,
        CategoryTransformer $categoryTransformer
    ) {
        $this->channelTransformer  = $channelTransformer;
        $this->categoryTransformer = $categoryTransformer;
    }

    /**
     * Return one channel
     *
     * @param string $uid
     * @return json
     */
    public function getSingle($uid)
    {
        $client = $this->getClient();
        
        $channel = $client->channels()->where('uid', $uid)->first();

        return $this->success([
            'data' => $this->channelTransformer->transform($channel, $this->getDevice()),
        ]);
    }

    /**
     * All Channels
     *
     * Returns all channels that are avialable to the application.
     *
     * @return json
     */
    public function getAll()
    {
        $client = $this->getClient();

        $resp = [];
        foreach ($client->channels as $channel) {
            $transformed = $this->channelTransformer->transform($channel, $this->getDevice());
            $resp[] = $transformed;
        }

        return $this->success([
            'data' => $resp,
        ]);
    }

    /**
     * Return all channel categories
     *
     * @return json
     */
    public function getCategories()
    {
        $client = $this->getClient();
        $categories = $client->user->categories()->get();

        return $this->success([
            'data'  =>  $categories,
        ]);
    }

    /**
     * Subscribe
     *
     * Subscribe to a specific channel.
     *
     * @param  Request $request [description]
     * @param  [type]  $uid     [description]
     * @return [type]           [description]
     */
    public function postSubscribe(Request $request, $uid)
    {
        $device = $this->getDevice();
        $channel = Channel::where('uid', $uid)->first();

        if ($device->subscribedTo($channel)) {
            $device->unsubscribeFrom($channel);
        } else {
            $device->subscribeTo($channel);
        }

        return $this->success();
    }

    public function postStop(StopChannelStream $request, $uid)
    {
        $device = $this->getDevice();
        $channel = Channel::where('uid', $uid)->first();

        if ($device->subscribedTo($channel)) {
            $device->unsubscribeFrom($channel);
        }

        return $this->success();
    }
}
