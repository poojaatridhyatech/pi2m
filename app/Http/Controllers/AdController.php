<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Http\Requests\AdFilter;
use App\Http\Requests\DeleteMultipleByUids;
use App\Http\Requests\PostAd;
use App\Routines\Crud\Create\Ad as CreateAd;
use App\Routines\Crud\Update\Ad as UpdateAd;
use Auth;
use Carbon\Carbon;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\Response;

class AdController extends Controller
{
    /**
     * Display a listing of the ads.
     *
     * @return Factory|View
     */
    public function index()
    {
        return view('ads.index');
    }

    /**
     * Get the list of ads depending on the currently logged in user and filters.
     *
     * @param AdFilter $request
     * @return JsonResponse
     */
    public function listing(AdFilter $request)
    {
        $user = Auth::user();

        if ($user->isAdmin()) {
            $userId = $request->get('user') ? $request->get('user') : null;

            if ($userId) {
                $ads = Ad::with(['user', 'categories'])->where('user_id', $userId)->orderBy('title')->get();
            } else {
                $ads = Ad::with(['user', 'categories'])->orderBy('title')->get();
            }
        } else {
            $ads = Ad::with(['user', 'categories'])->where('user_id', $user->id)->orderBy('title')->get();
        }

        $categoryId = $request->get('category') ? $request->get('category') : null;
        $type = $request->get('type') ? $request->get('type') : null;

        return response()->json($ads);
    }

    /**
     * Show the form for creating a new ad.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('ads.create', ['currencies' => $this->currencies()]);
    }

    /**
     * Store a newly created ad in storage.
     *
     * @param PostAd $request
     * @return RedirectResponse
     */
    public function store(PostAd $request)
    {
        new CreateAd($request);

        return redirect()->route('ads.index');
    }

    /**
     * Display the specified ad.
     *
     * @param Ad $ad
     * @return Factory|View
     */
    public function show(Ad $ad)
    {
        return view('ads.show', ['ad' => $ad]);
    }

    /**
     * Get the specified ad represented in a JSON format.
     *
     * @param Ad $ad
     * @return JsonResponse
     */
    public function json(Ad $ad)
    {
        $ad->type = json_decode($ad->type);
        $ad->expires_at = $ad->expires_at
            ? Carbon::createFromFormat('Y-m-d H:i:s', $ad->expires_at)->toIso8601String()
            : null;

        return response()->json($ad);
    }

    /**
     * Get the list of ad's categories.
     *
     * @param Ad $ad
     * @return JsonResponse
     */
    public function categories(Ad $ad)
    {
        $categories = $ad->categories()->pluck('id');
        return response()->json($categories);
    }

    /**
     * Show the form for editing the specified ad.
     *
     * @param Ad $ad
     * @return Factory|View
     */
    public function edit(Ad $ad)
    {
        return view('ads.edit', ['ad' => $ad, 'currencies' => $this->currencies()]);
    }

    /**
     * Update the specified ad in storage.
     *
     * @param PostAd $request
     * @param Ad $ad
     * @return RedirectResponse
     */
    public function update(PostAd $request, Ad $ad)
    {
        new UpdateAd($ad, $request);

        return redirect()->route('ads.index');
    }

    /**
     * Remove the specified ad from storage.
     *
     * @param Ad $ad
     * @return RedirectResponse
     */
    public function destroy(Ad $ad)
    {
        $ad->delete();

        return response(null, 204);
    }

    /**
     * Remove multiple ads by the specified UUIDs.
     *
     * @param DeleteMultipleByUids $request
     * @return ResponseFactory|Response
     */
    public function destroyMulti(DeleteMultipleByUids $request)
    {
        $uids = $request->get('uids');

        foreach ($uids as $uid) {
            if ($ad = Ad::where('uid', $uid)->first()) {
                $ad->delete();
            }
        }

        return response(null, 204);
    }

    /**
     * @todo Replace with non-hardcoded solution.
     */
    protected function currencies()
    {
        return [
            'GBP' => '£',
            'EUR' => '€',
            'USD' => '$',
        ];
    }

    public function postSearch(Request $request)
    {
        $fields = ['ads.id','ads.uid','ads.title'];
        $ads = [];

        $query = Ad::distinct()
                    ->select($fields)
                    ->join('ad_category', 'ads.id', '=', 'ad_category.ad_id')
                    ->join('categories', 'ad_category.category_id', '=', 'categories.id')
                    ->join('users', 'ads.user_id', '=', 'users.id')
                    ->where('ads.title', 'like', '%'.$request->searchkey.'%' );

        if (!empty($request->user) && $request->user > 0) {
            $query->where('users.id', $request->user);
        }

        if (!empty($request->categories) && $request->categories > 0) {
            $query->whereIn('categories.id', $request->categories);
        }

        $ads = $query->get();
        return response()->json($ads);
    }

    public function getInfo(Request $request)
    {
        $ad = Ad::where('id', $request->id )->first(['id','uid','title']);
        return response()->json($ad);
    }
}
