<?php

namespace App\Http\Controllers;

use Auth;
//use Extract;
use Exception;
use App\User;
use App\Client;
use Illuminate\Http\Request;
use App\Http\Requests\PostClient;
use App\Routines\Crud\Create\Client as CreateClient;
use App\Routines\Crud\Delete\Client as DeleteClient;
use App\Routines\Crud\Update\Client as UpdateClient;

class ClientController extends Controller
{
    /**
     * Edit a single instance
     *
     * @param string $uid
     * @return view
     */
    public function getEdit($uid)
    {
        $client =  Client::where('uid', $uid)->first();
        
        $user = null;
        $channels = [];
        $channels_selected = [];

        if ($client) {
            $user = $client->user;
            $channels = $user->channels()->get(['id', 'name']);
            $channels_selected = $client->channels()->pluck('channel_id')->toArray();
        } else {
            $user = Auth::user();
            $channels = $user->channels()->get(['id', 'name']);
        }

        return view('client')->with([
            'user' => $user,
            'client'    =>  $client,
            'channels'  =>  $channels,
            'channels_selected' => $channels_selected,
        ]);
    }

    /**
     * Return all avialable instances
     *
     * @return view
     */
    public function getAll(Request $request)
    {
        $userId = $request->get('user');
        $user = Auth::user();
        
        $selectedUser = null;
        $users = [];
        $apps;

        if ($user->isAdmin()) {
            $users = User::all()->sortByDesc('id');
        } else {
            $users = [$user];
        }

        if (empty($userId)) {
            if ($user->isAdmin()) {
                $apps = Client::orderBy('id', 'desc')->paginate(10);
            } else {
                $apps = $user->clients()->orderBy('id', 'desc')->paginate(10);
            }
        } else {
            $selectedUser = User::where('id', $userId)->first();
            $apps = $selectedUser->clients()->orderBy('id', 'desc')->paginate(10);
        }

        foreach ($apps as $app) {
            $app->username = $app->user->name;
            $app->access_token = $app->getAccessToken();
            unset($app->user);
        }

        return view('clients')->with([
            'list' => $apps->appends(Request::capture()->except(['page'])),
            'selectedUser' => $selectedUser,
            'users' => $users,
        ]);
    }

    /**
     * Create or Edit an instance
     *
     * @param PostClient $request
     * @param string $uid [if not specified, new will be created]
     * @return redirect
     */
    public function post(PostClient $request, $uid = null)
    {
        $user = Auth::user();
        $client = null;
        
        if ($user->isAdmin()) {
            $client = $this->createOrUpdate(Client::all(), $uid);
        } else {
            $client = $this->createOrUpdate($user->clients(), $uid);
        }

        if ($client) {
            new UpdateClient($client, $request);
        } else {
            new CreateClient($request);
        }

        return redirect()->route('clients.page');
    }

    /**
    * Delete an instance
    *
    * @param 
    * @return redirect
    */
    public function delete(Request $request)
    {
        $uids = $request->uids;

        $clients = Client::whereIn('uid', $uids)->get();
        foreach ($clients as $app) {
            new DeleteClient($app);
        }

        return redirect()->route('clients.page');
    }
    
    /**
     * Show clients JWT token
     *
     * @param Request $request
     * @param string $uid
     * @return view
     */
    public function getToken(Request $request, $uid)
    {
        try {
            $client = Auth::user()->clients->where('uid', $uid)->first();
            if ($client) {
                return '<div style="word-wrap: break-word">' . $client->getAccessToken() . '</div>';
            }
            
            return 'Unauthorised';
        } catch (Exception $e) {
            return 'Unauthorised';
        }
    }
}
