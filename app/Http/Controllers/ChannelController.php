<?php

namespace App\Http\Controllers;

use Auth;
//use Extract;
use App\Client;
use App\Channel;
use App\Category;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\PostChannel;
use App\Routines\Crud\Create\Channel as CreateChannel;
use App\Routines\Crud\Update\Channel as UpdateChannel;
use App\Routines\Crud\Delete\Channel as DeleteChannel;

/**
 * @resource Channels
 */
class ChannelController extends Controller
{
    /**
     * Edit a single instance
     *
     * @param string $uid
     * @return view
     */
    public function getEdit($uid = null)
    {
        $user = null;
        $channel = Channel::where('uid', $uid)->first();

        if ($channel) {
            $user = $channel->user;
        } else {
            $user = Auth::user();
        }

        $clients =  $user->clients()->get(['id', 'name']);
        $videos = $user->contents()->get(['id',/*'uid',*/ 'title']);

        $categories_selected = [];
        $videos_selected = [];
        $clients_selected = [];

        if($channel) {
            $videos_selected = $channel->contents()->pluck('content_id')->toArray();
            $clients_selected = $channel->clients()->pluck('client_id')->toArray();
        }
        
        return view('channel')->with([
            'channel'   =>  $channel,
            'user' => $user,

            'videos'  =>  $videos,
            'apps'  =>  $clients,

            'videos_selected'  =>  $videos_selected,
            'apps_selected'  =>  $clients_selected,

        ]);
    }
    
    /**
     * Return all channels avialable to the client
     *
     * @return view
     */
    public function getAll(Request $request)
    {
        $userId = $request->get('user');

        $channels = [];
        $user = Auth::user();

        $selectedUser = null;
        
        if (empty($userId)) {
            if ($user->isAdmin()) {
                $channels = Channel::orderBy('id', 'desc')->paginate(10);
            } else {
                $channels = $user->channels()->orderBy('id', 'desc')->paginate(10);
            }
        } else {
            $selectedUser = User::where('id', $userId)->first();
            $channels = $selectedUser->channels()->orderBy('id', 'desc')->paginate(10);
        }

        foreach ($channels as $ch) {
            //$ch->explicit = $ch->isExplicit();
           // $ch->formatted_created_at = date('d-M-y', strtotime($ch->created_at));
            //$ch->formatted_updated_at = $ch->updated_at->diffForHumans();
            $ch->username = $ch->user->name;
            unset($ch->user);
        }

        return view('channels')->with([
            'list' => $channels->appends(Request::capture()->except(['page'])),
            'selectedUser' => $selectedUser,
        ]);
    }
    
    /**
     * View videos of one category
     *
     * @param string $uid
     * @return view
     */
    public function viewVideos($uid)
    {
        $user = Auth::user();

        $channel =  Channel::where('uid', $uid)->first();
        $contents = $channel->contents()->orderBy('id', 'desc')->paginate(10);

        foreach ($contents as $v) {
            //$ch->explicit = $ch->isExplicit();
            $v->can_push = $v->canPush();
            $v->username = $v->user->name;
            unset($v->user);
        }

        return view('channel_view')->with([
            'channel'  =>  $channel,
            'list'  =>  $contents,
            'active' => 'videos'
        ]);
    }

    public function viewApps($uid)
    {
        $user = Auth::user();

        $channel =  Channel::where('uid', $uid)->first();
        $apps = $channel->clients()->orderBy('id', 'desc')->paginate(10);

        foreach ($apps as $v) {
            //$ch->explicit = $ch->isExplicit();
            $v->username = $v->user->name;
            unset($v->user);
        }

        return view('channel_view')->with([
            'channel'  =>  $channel,
            'list'  =>  $apps,
            'active' => 'apps'
        ]);
    }

    /**
     * Create or edit an instance
     *
     * @param Request $request
     * @param string $uid
     * @return redirect
     */
    public function post(PostChannel $request, $uid = null)
    {
        $user = Auth::user();
        $channel = null;

        if ($user->isAdmin()) {
            $channel = $this->createOrUpdate(Channel::all(), $uid);
        } else {
            $channel = $this->createOrUpdate($user->channels(), $uid);
        }

        if ($channel) {
            $exec = new UpdateChannel($channel,$request);
        } else {
            $exec = new CreateChannel($request);
        }

        return redirect()->route('channels.page');
    }

    /**
    * Delete an instance
    *
    * @param $request
    * @return redirect
    */
    public function delete(Request $request)
    {
        $uids = $request->uids;
        //$channels = Auth::user()->channels()->whereIn('uid', $uids)->get();
        $channels = Channel::whereIn('uid', $uids)->get();

        foreach ($channels as $ch) {
           new DeleteChannel($ch);
        }

        return redirect()->route('channels.page');
    }

    public function dissociate(Request $request, $cat, $type)
    {
        $uids = $request->uids;

        $channel = Channel::where('uid', $cat)->first();
        //dd($request);

        if (empty($channel)) {
            return redirect()->route('channels.'.$type, $cat );
        }

        if ($type == 'videos')
        {
            $ids = $channel->contents()->wherein('uid', $uids)->pluck('content_id')->toArray();
            $channel->contents()->detach($ids);
            return redirect()->route('channels.contents', $cat);
        }
        else if ($type == 'apps')
        {
            $ids = $channel->clients()->wherein('uid', $uids)->pluck('client_id')->toArray();
            $channel->clients()->detach($ids);
            return redirect()->route('channels.apps', $cat);
        }
        return redirect()->route('channels.'.$type, $cat);
    }

    public function postSearch(Request $request)
    {
        $fields = ['id','uid','label AS name', 'asset_path'];

        if (!empty($request->user) && $request->user > 0) {
            $selectedUser = User::where('id', $request->user)->first();
            $channels = $selectedUser->channels()->where('name', 'like', '%'.$request->searchkey.'%' )->get($fields);
        } else {
            $channels = Channel::where('label', 'like', '%'.$request->searchkey.'%' )->get($fields);
        }
        
        return response()->json($channels);
    }

    public function getInfo(Request $request)
    {
        $channel = Channel::where('id', $request->id )->first(['id','uid','name', 'asset_path']);
        return response()->json($channel);
    }

}