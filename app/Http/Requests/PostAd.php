<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PostAd extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' =>  'required|max:255|min:3',
            'description' => 'required',
            'url' => 'url|nullable',
            'asset_path' => 'string|nullable',
            'price' => 'required',
            'dismissible' => 'boolean|nullable',
            'type' => ['required', Rule::in(['video', 'banner'])],
            'resource_uri' => 'string|required_if:type,==,video',
            'position' => 'string|required_if:type,==,banner',
            'categories' => 'array',
        ];
    }
}
