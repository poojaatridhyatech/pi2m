<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecallPush extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'content_uid'   =>  'required|exists:contents,uid',
            'channel_uid'   =>  'required|exists:channels,uid',
        ];
    }
}
