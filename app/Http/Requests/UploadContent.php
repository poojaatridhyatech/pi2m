<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UploadContent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'asset_path'      =>  'required',
            'filename'      =>  'required',
            'title'         =>  'required|max:100',
            'description'   =>  'required|max:200',
            'expires_at'    =>  'required',
            'categories'    =>  'array|nullable',
            'channels'      =>  'array|nullable',
            'ads'           =>  'array|nullable',
        ];
    }
}
