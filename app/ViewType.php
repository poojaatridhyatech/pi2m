<?php

namespace App;

class ViewType extends AbstractModel
{
    const TYPE_WATCH_LATER = 'watch_later';
    const TYPE_STARTED = 'started';
    const TYPE_FINISHED = 'finished';
    const TYPE_STOPPED = 'stopped';
    const TYPE_LOADED = 'loaded';

    protected $fillable = [
        'name'
    ];

    public function scopeWatchLater($query)
    {
        return $query->where('name', 'watch_later')->first();
    }

    public function scopeFinished($query)
    {
        return $query->where('name', 'finished')->first();
    }
}
