<?php

namespace App;

use App\Events\ModelCreated;
use App\Events\ModelDeleted;
use App\Events\ModelUpdated;
use App\User;
use App\Device;
use App\Channel;
use App\Services\JwtService;

class Client extends AbstractModel
{
    public $logAction = true;

    protected $fillable = [
        'uid',
        'name',
        'description',
        'user_id',
        'secret',
        'onesignal_app_id',
        'onesignal_api_key',
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => ModelCreated::class,
        'updated' => ModelUpdated::class,
        'deleted' => ModelDeleted::class,
    ];

    public function devices()
    {
        return $this->belongsToMany(Device::class, 'client_devices', 'client_id', 'device_id');
    }

    public function channels()
    {
        return $this->belongsToMany(Channel::class, 'client_channels', 'client_id', 'channel_id');
    }

    public function getAccessToken()
    {
        $jwt = new JwtService;
        $jwt->setPayload([ 'secret' => encrypt($this->secret) ]);

        return 'Bearer ' . $jwt->generateToken();
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

}
