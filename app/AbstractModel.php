<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

abstract class AbstractModel extends Model
{
    protected static function byUid($uid)
    {
        return self::where('uid', $uid)->first();
    }
}