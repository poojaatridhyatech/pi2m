---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)
<!-- END_INFO -->

#Channels
<!-- START_cfc37c070ff79f06a6fba165138e1674 -->
## All Channels

Returns all channels that are avialable to the application.

> Example request:

```bash
curl -X GET "https://staging-snique.eskimo.uk.com/api/v1/channels" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://staging-snique.eskimo.uk.com/api/v1/channels",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthorised"
}
```

### HTTP Request
`GET api/v1/channels`

`HEAD api/v1/channels`


<!-- END_cfc37c070ff79f06a6fba165138e1674 -->

<!-- START_402a5f8b6e3360fa29615a9d6c1f5d7a -->
## Subscribe

Subscribe to a specific channel.

> Example request:

```bash
curl -X POST "https://staging-snique.eskimo.uk.com/api/v1/channels/{uid}/subscribe" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://staging-snique.eskimo.uk.com/api/v1/channels/{uid}/subscribe",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/channels/{uid}/subscribe`


<!-- END_402a5f8b6e3360fa29615a9d6c1f5d7a -->

#Social Authentication
<!-- START_9fdb8043fc5e257c078eade7db86aa8b -->
## Facebook

> Example request:

```bash
curl -X POST "https://staging-snique.eskimo.uk.com/api/v1/auth/{type}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://staging-snique.eskimo.uk.com/api/v1/auth/{type}",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/auth/{type}`


<!-- END_9fdb8043fc5e257c078eade7db86aa8b -->

#Videos

Access and modify uploaded content.
<!-- START_1c3b0811cea6d55f7a89fea34492ba92 -->
## About Video

Returns information about a video.

> Example request:

```bash
curl -X GET "https://staging-snique.eskimo.uk.com/api/v1/videos/{uid}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://staging-snique.eskimo.uk.com/api/v1/videos/{uid}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthorised"
}
```

### HTTP Request
`GET api/v1/videos/{uid}`

`HEAD api/v1/videos/{uid}`


<!-- END_1c3b0811cea6d55f7a89fea34492ba92 -->

<!-- START_ec7fe384656e907f8c61ca9e79e28937 -->
## Download

Download content.

> Example request:

```bash
curl -X GET "https://staging-snique.eskimo.uk.com/api/v1/videos/{uid}/download" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://staging-snique.eskimo.uk.com/api/v1/videos/{uid}/download",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthorised"
}
```

### HTTP Request
`GET api/v1/videos/{uid}/download`

`HEAD api/v1/videos/{uid}/download`


<!-- END_ec7fe384656e907f8c61ca9e79e28937 -->

<!-- START_ec5dc2518cb0aa86622286918b901778 -->
## Download Finished

Confirm that a certain video has been downloaded to the device.

> Example request:

```bash
curl -X POST "https://staging-snique.eskimo.uk.com/api/v1/videos/{uid}/downloaded" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://staging-snique.eskimo.uk.com/api/v1/videos/{uid}/downloaded",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/videos/{uid}/downloaded`


<!-- END_ec5dc2518cb0aa86622286918b901778 -->

<!-- START_29d8e4201f9d6182364650def04b2355 -->
## Video Deleted

Mark the video as deleted.

> Example request:

```bash
curl -X POST "https://staging-snique.eskimo.uk.com/api/v1/videos/{uid}/deleted" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://staging-snique.eskimo.uk.com/api/v1/videos/{uid}/deleted",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/videos/{uid}/deleted`


<!-- END_29d8e4201f9d6182364650def04b2355 -->

<!-- START_16a92dde04adeeab2bc28e436c75fa69 -->
## Search

Search content.

> Example request:

```bash
curl -X POST "https://staging-snique.eskimo.uk.com/api/v1/videos/search" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://staging-snique.eskimo.uk.com/api/v1/videos/search",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/videos/search`


<!-- END_16a92dde04adeeab2bc28e436c75fa69 -->

<!-- START_2b6aef1c009d99b89fc2bb3653874faf -->
## Opinion

Post an opinion, like or dislike content.

> Example request:

```bash
curl -X POST "https://staging-snique.eskimo.uk.com/api/v1/videos/{uid}/opinion" \
-H "Accept: application/json" \
    -d "like"="1" \
    -d "flfl"="1" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://staging-snique.eskimo.uk.com/api/v1/videos/{uid}/opinion",
    "method": "POST",
    "data": {
        "like": true,
        "flfl": true
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/videos/{uid}/opinion`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    like | boolean |  required  | 
    flfl | boolean |  required  | 

<!-- END_2b6aef1c009d99b89fc2bb3653874faf -->

<!-- START_51a4f7acf7fd02e2bba06e20e2233459 -->
## Bookmark

Bookmark content.

> Example request:

```bash
curl -X POST "https://staging-snique.eskimo.uk.com/api/v1/videos/{uid}/bookmark" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://staging-snique.eskimo.uk.com/api/v1/videos/{uid}/bookmark",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/videos/{uid}/bookmark`


<!-- END_51a4f7acf7fd02e2bba06e20e2233459 -->

