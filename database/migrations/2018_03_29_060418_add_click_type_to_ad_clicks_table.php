<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClickTypeToAdClicksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ad_clicks', function (Blueprint $table) {
            $table->foreign('click_type')->references('id')->on('ad_click_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ad_clicks', function (Blueprint $table) {
            $table->dropForeign('click_type');
        });
    }
}
