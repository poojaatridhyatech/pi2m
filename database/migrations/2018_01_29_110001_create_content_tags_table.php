<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_tags', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('content_id')->unsigned();
            $table->foreign('content_id')->references('id')->on('contents');

            $table->integer('tag_id')->unsigned();
            $table->foreign('tag_id')->references('id')->on('tags');


            $table->timestamps();
        });

        \App\Tag::create([
            'name'  =>  'explicit'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('content_tags', function (Blueprint $table) {
            $table->dropForeign(['content_id']);
            $table->dropColumn('content_id');

            $table->dropForeign(['tag_id']);
            $table->dropColumn('tag_id');

        });

        Schema::dropIfExists('content_tags');
    }
}
