<?php

use App\Device;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOnesignalUidToDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('devices', function (Blueprint $table) {
            $table->string('onesignal_uid')->nullable();
        });

        foreach (Device::all() as $device) {
            if ($device->onesignal_uid === null) {
                // make a fake one
                $device->onesignal_uid = uniqid('DEFAULT_');
                $device->save();
            }
        }

        // then change to unique
        Schema::table('devices', function (Blueprint $table) {
            $table->string('onesignal_uid')->unique()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('devices', function (Blueprint $table) {
            $table->dropColumn('onesignal_uid');
        });
    }
}
