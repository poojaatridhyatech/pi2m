<?php

use App\Wishlist;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DisallowDuplicatesInWishlistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wishlists', function (Blueprint $table) {
            $records = Wishlist::all();
            $distinct = [];

            foreach ($records as $record) {
                $key = $record->device_id . '-' . $record->ad_id;
                if (!isset($distinct[$key])) {
                    $distinct[$key] = $record;
                } else {
                    $record->delete();
                }
            }

            $table->dropForeign(['device_id']);
            $table->dropForeign(['ad_id']);
            $table->dropIndex('wishlists_device_id_foreign');
            $table->unique(['device_id', 'ad_id']);

            $table->foreign('device_id')->references('id')->on('devices')->onDelete('cascade');
            $table->foreign('ad_id')->references('id')->on('ads')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
