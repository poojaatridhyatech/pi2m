<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdCampaignContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_campaign_content', function (Blueprint $table) {
            $table->integer('ad_campaign_id')->unsigned();
            $table->integer('content_id')->unsigned();

            $table->unique(['ad_campaign_id', 'content_id']);

            $table->foreign('ad_campaign_id')->references('id')->on('ad_campaigns')
                ->onUpdate('restrict')->onDelete('cascade');
            $table->foreign('content_id')->references('id')->on('contents')
                ->onUpdate('restrict')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_campaign_content');
    }
}
