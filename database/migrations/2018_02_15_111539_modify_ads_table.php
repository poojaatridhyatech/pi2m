<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ads', function (Blueprint $table) {
            $table->string('asset_path')->nullable()->change();
            $table->string('resource_uri')->nullable()->change();
            $table->string('url')->nullable()->after('description');
            $table->char('currency', 3)->after('price');
        });

        DB::statement('ALTER TABLE ads CHANGE expires_at expires_at TIMESTAMP NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ads', function (Blueprint $table) {
            $table->dropColumn(['url', 'currency']);
        });

        DB::statement('ALTER TABLE ads CHANGE expires_at expires_at DATETIME;');
    }
}
