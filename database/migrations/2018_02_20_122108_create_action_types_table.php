<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
        });

        try {
            $actionTypes = ['created', 'updated', 'uploaded'];
            foreach ($actionTypes as $actionType) {
                DB::table('action_types')->insert(['name' => $actionType]);
            }
        } catch (Exception $e) {
            // Do nothing.
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('action_types');
    }
}
