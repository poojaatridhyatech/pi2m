<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RevertTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('series_categories');
        Schema::dropIfExists('series_channels');
        Schema::dropIfExists('series_contents');
        Schema::dropIfExists('channel_categories');

        Schema::table('contents', function (Blueprint $table) {
            $table->dropForeign(['season_id']);
            $table->dropColumn('season_id');
        });
        Schema::dropIfExists('seasons');
        Schema::dropIfExists('series');

        Schema::dropIfExists('ad_ad_campaign');
        Schema::dropIfExists('ad_campaign_category');
        Schema::dropIfExists('ad_campaign_content');
        Schema::dropIfExists('ad_campaigns');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
