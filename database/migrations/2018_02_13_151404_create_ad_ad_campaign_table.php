<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdAdCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_ad_campaign', function (Blueprint $table) {
            $table->integer('ad_id')->unsigned();
            $table->integer('ad_campaign_id')->unsigned();

            $table->unique(['ad_id', 'ad_campaign_id']);

            $table->foreign('ad_id')->references('id')->on('ads')
                ->onUpdate('restrict')->onDelete('cascade');
            $table->foreign('ad_campaign_id')->references('id')->on('ad_campaigns')
                ->onUpdate('restrict')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_ad_campaign');
    }
}
