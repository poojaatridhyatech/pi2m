<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');

            $table->string('uid')->unique();
            $table->string('name');
            $table->dateTime('starts_at');
            $table->string('status');

            $table->integer('content_id')->unsigned();
            $table->foreign('content_id')->references('id')->on('contents');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->dropForeign(['content_id']);
            $table->dropColumn('content_id');

        });
        

        Schema::dropIfExists('campaigns');
    }
}
