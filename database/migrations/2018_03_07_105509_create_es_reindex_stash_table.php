<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEsReindexStashTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('es_reindex_stash', function (Blueprint $table) {
            $table->increments('id');
            $table->string('entity_class_name');
            $table->integer('entity_id')->unsigned();
            $table->tinyInteger('action')->unsigned()->comment('1 - created, 2 - updated, 3 - deleted');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('es_reindex_stash');
    }
}
