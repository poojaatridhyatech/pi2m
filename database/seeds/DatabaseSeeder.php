<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ChannelsTableSeeder::class);
        $this->call(DeviceTableSeeder::class);
//        $this->call(ActionTypesTableSeeder::class);
    }
}

class DeviceTableSeeder extends Seeder
{
    public function run()
    {
        $device = \App\Device::create([
            'uid'   =>  \Generate::noDashUid(),
            'info'  =>  '{}',
            'onesignal_uid' =>  '50ecded6-191c-4f1d-889b-0999f9c90935',
        ]);
    }
}


/**
* 
*/
class ChannelsTableSeeder extends Seeder
{
    public function run()
    {
        $channel = \App\Channel::create([
            'label' =>  'ESPN Sports',
            'name'  =>  'espn',
            'asset_path'    =>  'test',
            'uid'   =>  \Generate::noDashUid(),
            'user_id'   =>  \App\User::first()->id,
        ]);

        $channel = \App\Channel::create([
            'label' =>  'VH1',
            'name'  =>  'music',
            'asset_path'    =>  'test',
            'uid'   =>  \Generate::noDashUid(),
            'user_id'   =>  \App\User::first()->id,
        ]);
    }
}

/**
* 
*/
class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $user = \App\User::create([
            'email' =>  'imants@eskimo.uk.com',
            'password'  =>  bcrypt('letmein'),
            'uid'   =>  \Generate::noDashUid(),
            'name'  =>  'imants kusins',
        ]);

        // add a role
        $user->roles()->attach(\App\Role::where('name', 'admin')->first()->id);
    }
}

/**
* 
*/
class RolesTableSeeder extends Seeder
{
    public function run()
    {
        \App\Role::create([
            'name'  =>  'admin',
            'label' =>  'Administrator',
        ]);

        \App\Role::create([
            'name'  =>  'user',
            'label' =>  'User',
        ]);
    }
}
