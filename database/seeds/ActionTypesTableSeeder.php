<?php

use Illuminate\Database\Seeder;

class ActionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::table('action_types')->insert([
                ['name' => 'created'],
                ['name' => 'updated'],
                ['name' => 'uploaded'],
            ]);
        } catch (Exception $e) {
            // Do nothing.
        }
    }
}
