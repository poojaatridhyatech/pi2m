<?php

use App\Ad;
use App\Category;
use App\Channel;
use App\Client;
use App\Content;
use App\Device;
use App\Push;
use App\User;
use App\ViewType;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;

class DummyContentSeeder extends Seeder
{
    protected $user;
    protected $faker;

    public function __construct()
    {
        $this->user = User::find(1);
        $this->faker = Faker\Factory::create();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        set_time_limit(0);

        DB::transaction(function () {
            $this->command->info('Seeding client.');
            $this->seedClient();
            $this->command->info('Seeding categories.');
            $this->seedCategories();
            $this->command->info('Seeding channels.');
            $this->seedChannels();
            $this->command->info('Seeding devices.');
            $this->seedDevices();
            $this->command->info('Seeding ads.');
            $this->seedAds();
            $this->command->info('Seeding pushes, views, likes and ad clicks.');
            $this->seedPushesViewsLikesAndAdClicks();
            $this->command->info('Seeding subscriptions.');
            $this->seedSubscriptions();
        });
    }

    protected function seedClient()
    {
        $dt = Carbon::now();
        Client::create([
            'uid' => Uuid::uuid4(),
            'name' => 'Client A',
            'description' => $this->faker->paragraph,
            'user_id' => $this->user->id,
            'secret' => str_random(),
            'created_at' => $dt,
            'updated_at' => $dt,
        ]);
    }

    protected function seedCategories()
    {
        $dt = Carbon::now();

        for ($i = 1; $i <= 16; $i++) {
            DB::table('categories')->insert([
                'uid' => Uuid::uuid4(),
                'name' => 'Category ' . $i,
                'description' => $this->faker->paragraph,
                'user_id' => $this->user->id,
                'asset_path' => '',
                'created_at' => $dt,
                'updated_at' => $dt,
            ]);
        }
    }

    protected function seedChannels()
    {
        for ($i = 0; $i < 5; $i++) {
            $days = rand(2, 60);
            $dt = Carbon::now()->subDays($days);

            $channelId = DB::table('channels')->insertGetId([
                'uid' => Uuid::uuid4(),
                'name' => $this->faker->name,
                'label' => $this->faker->name,
                'description' => $this->faker->paragraph,
                'asset_path' => '',
                'user_id' => $this->user->id,
                'created_at' => $dt,
                'updated_at' => $dt,
            ]);
            $channel = Channel::find($channelId);

            $this->seedVideosForChannel($channel, rand(10, 30), $days);
        }
    }

    protected function seedDevices()
    {
        $dt = Carbon::now();
        $records = [];

        for ($i = 0; $i < 50; $i++) {
            $records[] = [
                'uid' => Uuid::uuid4(),
                'created_at' => $dt,
                'updated_at' => $dt,
            ];
        }

        DB::table('devices')->insert($records);
    }

    protected function seedAds()
    {
        $dt = Carbon::now();

        for ($i = 0; $i < 30; $i++) {
            $this->seedAd($dt);
        }
    }

    protected function seedPushesViewsLikesAndAdClicks()
    {
        $client = Client::first();
        $devices = Device::all();
        $videos = Content::all();
        $clickTypes = DB::table('ad_click_types')->pluck('id')->toArray();
        $viewTypeWatchLater = ViewType::where('name', ViewType::TYPE_WATCH_LATER)->first();
        $viewTypeStarted = ViewType::where('name', ViewType::TYPE_STARTED)->first();
        $viewTypeFinished = ViewType::where('name', ViewType::TYPE_FINISHED)->first();
        $viewTypeStopped = ViewType::where('name', ViewType::TYPE_STOPPED)->first();
        $viewTypeLoaded = ViewType::where('name', ViewType::TYPE_LOADED)->first();
        $scenarios = [
            [$viewTypeWatchLater],
            [$viewTypeLoaded],
            [$viewTypeLoaded, $viewTypeStarted, $viewTypeFinished],
            [$viewTypeLoaded, $viewTypeStarted, $viewTypeStopped],
        ];

        foreach ($devices as $device) {
            $this->command->info('Seeding for the device: ' . $device->id);

            foreach ($videos as $video) {
                $videoCats = $video->categories()->pluck('categories.id')->toArray();
                $adId = (rand(0, 3)) ? $this->getRandomAdByCategories($videoCats) : null;

                $pushId = DB::table('pushes')->insertGetId([
                    'device_id' => $device->id,
                    'content_id' => $video->id,
                    'ad_id' => $adId,
                    'created_at' => $video->created_at,
                    'updated_at' => $video->updated_at,
                    'deleted' => 0,
                    'pushed' => rand(0, 1),
                ]);
                $push = Push::find($pushId);

                if ($push->pushed) {
                    $scenario = $scenarios[rand(0, 3)];

                    foreach ($scenario as $viewType) {
                        $dt = Carbon::createFromFormat('Y-m-d H:i:s', $push->created_at)->addSeconds(rand(60, 300));
                        $duration = null;

                        if ($viewType->name === ViewType::TYPE_LOADED) {
                            $dt->addSeconds(rand(3, 7));
                        }

                        if (in_array($viewType->name, [ViewType::TYPE_STOPPED, ViewType::TYPE_FINISHED])) {
                            $duration = rand(10, $video->duration);
                            if ($viewType->name === ViewType::TYPE_FINISHED) {
                                $duration = $video->duration;
                            }

                            $dt->addSeconds($duration);
                        }

                        DB::table('views')->insert([
                            'device_id' => $device->id,
                            'content_id' => $video->id,
                            'view_type_id' => $viewType->id,
                            'duration' => $duration,
                            'data' => '',
                            'created_at' => $dt,
                            'updated_at' => $dt,
                        ]);

                        if ($viewType->name == ViewType::TYPE_FINISHED && rand(1, 5) > 3) {
                            DB::table('opinions')->insert([
                                'device_id' => $device->id,
                                'content_id' => $video->id,
                                'like' => (rand(1, 10) < 10) ? 1 : 0,
                                'created_at' => $dt,
                                'updated_at' => $dt,
                            ]);
                        }

                        if ($adId && $viewType->name == ViewType::TYPE_STARTED) {
                            DB::table('ad_clicks')->insert([
                                'ad_id' => $adId,
                                'client_id' => $client->id,
                                'device_id' => $device->id,
                                'click_type' => $clickTypes[rand(0, count($clickTypes) - 1)],
                                'created_at' => $dt,
                                'updated_at' => $dt,
                            ]);

                            if (rand(0, 1)) {
                                $exists = DB::table('wishlists')->where([
                                    ['device_id', $device->id],
                                    ['ad_id', $adId],
                                ])->exists();

                                if (!$exists) {
                                    DB::table('wishlists')->insert([
                                        'device_id' => $device->id,
                                        'ad_id' => $adId,
                                        'created_at' => $dt,
                                        'updated_at' => $dt,
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    protected function seedSubscriptions()
    {
        $devices = Device::all();
        $channels = Channel::all();

        foreach ($channels as $channel) {
            $channelCreatedAt = Carbon::createFromFormat('Y-m-d H:i:s', $channel->created_at);

            foreach ($devices as $device) {
                if (rand(1, 10) > 7) {
                    $dt = $channelCreatedAt->addSeconds(rand(3600, 86400));

                    DB::table('subscriptions')->insert([
                        'device_id' => $device->id,
                        'channel_id' => $channel->id,
                        'created_at' => $dt,
                        'updated_at' => $dt,
                    ]);

                    if (rand(1, 10) > 9) {
                        $dt->addHours(rand(12, 24));

                        DB::table('subscriptions')->insert([
                            'device_id' => $device->id,
                            'channel_id' => $channel->id,
                            'subscribed' => 0,
                            'created_at' => $dt,
                            'updated_at' => $dt,
                        ]);
                    }
                }
            }
        }
    }

    protected function seedVideosForChannel(Channel $channel, int $count, int $daysAgo)
    {
        $this->command->info('Seeding videos for the channel: ' . $channel->id);

        for ($i = 0; $i < $count; $i++) {
            $days = rand(1, $daysAgo);
            $dt = Carbon::now()->subDays($days);

            $video = $this->seedVideo($dt);
            $video->channels()->sync([$channel->id]);
        }
    }

    protected function seedVideo(Carbon $dt)
    {
        $videoId = DB::table('contents')->insertGetId([
            'uid' => Uuid::uuid4(),
            'title' => $this->faker->name,
            'description' => $this->faker->paragraph,
            'resource_uri' => '',
            'extension' => 'mp4',
            'mime' => 'video/mp4',
            'asset_path' => '',
            'user_id' => $this->user->id,
            'expires_at' => Carbon::now()->addMonths(3)->toDateTimeString(),
            'duration' => rand(15, 300),
            'created_at' => $dt,
            'updated_at' => $dt,
        ]);
        $video = Content::find($videoId);
        $video->categories()->sync($this->getRandomCategories());

        return $video;
    }

    protected function seedAd(Carbon $dt)
    {
        $type = rand(0, 1) ? Ad::TYPE_BANNER : Ad::TYPE_VIDEO;
        $adId = DB::table('ads')->insertGetId([
            'uid' => Uuid::uuid4(),
            'title' => $this->faker->name,
            'description' => $this->faker->paragraph,
            'price' => rand(3, 10),
            'currency' => 'EUR',
            'user_id' => $this->user->id,
            'type' => json_encode([
                'type' => $type,
                'position' => ($type === Ad::TYPE_BANNER) ? Ad::POSITION_TOP : null,
                'dismissible' => rand(0, 1),
            ]),
            'created_at' => $dt,
            'updated_at' => $dt,
        ]);
        $ad = Ad::find($adId);
        $ad->categories()->sync($this->getRandomCategories());

        return $ad;
    }

    protected function getRandomCategories()
    {
        $categories = Category::pluck('id')->toArray();
        $catsTotal = count($categories);
        $ids = [];

        for ($i = 0, $catsNum = rand(1, 3); $i < $catsNum; $i++) {
            $ids[] = $categories[rand(0, $catsTotal - 1)];
        }

        return $ids;
    }

    protected function getRandomAdByCategories(array $categoryIds)
    {
        $ads = DB::table('ad_category')
            ->whereIn('category_id', $categoryIds)
            ->distinct()
            ->pluck('ad_id')
            ->toArray();

        return (count($ads)) ? $ads[rand(0, count($ads) - 1)] : null;
    }
}
