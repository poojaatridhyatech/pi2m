<?php

use Illuminate\Database\Seeder;

class AdClickTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::table('ad_click_types')->insert([
                ['key' => 'skip', 'name' => 'Skip'],
                ['key' => 'open', 'name' => 'Opened'],
                ['key' => 'click', 'name' => 'Click through'],
            ]);
        } catch (Exception $e) {
            // Do nothing.
        }
    }
}
