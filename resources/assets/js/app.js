
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


window.Vue = require('vue');

// You need a specific loader for CSS files
import 'vue-datetime/dist/vue-datetime.css';

window.$ = window.jQuery = require('jquery');
import Moment from 'moment';
import axios from 'axios';

window.axios = require('axios');

import route from './route.js';
import { Datetime } from 'vue-datetime';
import { Settings } from 'luxon';
import SimpleBar from 'simplebar';

window.route = route;
Settings.defaultLocale = 'gb';

import VeeValidate from 'vee-validate';
Vue.use(VeeValidate, {
    //errorBagName: 'vErrors'
});

import ModalProgrammatic, { Modal } from './components/modal';
import Dialog from './components/dialog';
import tabs from './utils/tabs';
window.tabs = tabs;
import store from './utils/store';
window.store = store;

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('notifications', require('./components/Notifications.vue'));
Vue.component('thumbnail-upload', require('./components/ThumbnailUpload.vue'));
Vue.component('content-upload', require('./components/ContentUpload.vue'));
// Vue.component('push-video', require('./components/PushVideo.vue'));
Vue.component('push-content', require('./components/PushContent.vue'));
Vue.component('modal-title', require('./components/ModalTitle.vue'));
Vue.component('validation-alerts', require('./components/ValidationAlerts.vue'));
Vue.component('csrf', require('./components/Csrf.vue'));
Vue.component('datetime', Datetime);

Vue.component('user-selector', require('./components/UserSelector.vue'));
Vue.component('category-selector', require('./components/CategorySelector.vue'));
Vue.component('channel-selector', require('./components/ChannelSelector.vue'));
Vue.component('video-selector', require('./components/VideoSelector.vue'));
Vue.component('ads-selector', require('./components/AdsSelector.vue'));

Vue.component('ad-settings', require('./components/AdSettings.vue'));
Vue.component('account-setting', require('./components/AccountSetting.vue'));
Vue.component('ads-attacher', require('./components/AdsAttacher.vue'));
Vue.component('categories-attacher', require('./components/CategoriesAttacher.vue'));
Vue.component('channels-attacher', require('./components/ChannelsAttacher.vue'));
Vue.component('videos-attacher', require('./components/VideosAttacher.vue'));
Vue.component('client-attacher', require('./components/ClientAttacher.vue'));

Vue.component('Modal', Modal);
Vue.prototype.$dialog = Dialog;
Vue.prototype.$modal = ModalProgrammatic;

Vue.component('manage-ads', require('./components/ManageAds.vue'));
Vue.component('manage-categories', require('./components/ManageCategories.vue'));
Vue.component('manage-channels', require('./components/ManageChannels.vue'));
Vue.component('manage-videos', require('./components/ManageVideos.vue'));
Vue.component('manage-apps', require('./components/ManageApps.vue'));
Vue.component('manage-users', require('./components/ManageUsers.vue'));
Vue.component('manage-devices', require('./components/ManageDevices.vue'));
Vue.component('manage-push', require('./components/ManagePush.vue'));
// Vue.component('push-video-to-channel', require('./components/PushVideoToChannel.vue'));

Vue.component('viewer-engagement', require('./components/dashboard/ViewerEngagement.vue'));
Vue.component('filter-viewer', require('./components/dashboard/FilterViewer.vue'));
Vue.component('popular-items-viewer', require('./components/dashboard/PopularItemsViewer.vue'));
Vue.component('popular-item', require('./components/dashboard/PopularItem.vue'));
Vue.component('popularity-item', require('./components/dashboard/PopularityItem.vue'));
Vue.component('bar-chart-view', require('./components/dashboard/BarChartView.vue'));
Vue.component('percent-chart-view', require('./components/dashboard/PercentChartView.vue'));
Vue.component('graph-chart-view', require('./components/dashboard/GraphChartView.vue'));

Vue.component('dashboard-summary', require('./components/dashboard/DashboardSummary.vue'));
Vue.component('ad-dashboard-summary', require('./components/dashboard/AdDashboardSummary.vue'));
Vue.component('dashboard-reindex', require('./components/dashboard/DashboardReindex.vue'));


Vue.mixin({
    methods: {
        glToggleModal(id) {
            let el = document.getElementById(id);
            if (el.classList.contains('is-active')) {
                el.classList.remove('is-active');
            } else {
                el.classList.add('is-active');
            }
        },
        glToday(format = '') {
            return Moment(Moment.now()).format(format);
        },
        glFilterValidationErrors(before) {
            let after = [];

            for (let i in before) {
                let errors = before[i];
                for (let i in errors) {
                    let error = errors[i];

                    after.push(error);
                }
            }

            return after;
        },
        getUrlParameterByName(name) {
            name = name.replace(/[\[\]]/g, '\\$&');
            let regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)');
            let results = regex.exec(document.location.href);

            if (!results) {
                return null;
            }

            if (!results[2]) {
                return '';
            }

            return decodeURIComponent(results[2].replace(/\+/g, ' '));
        }
    }
});

window.app = new Vue({
    el: '#app',
    mounted: function() {

    },
    methods: {

    },
    //data: function() {
    //    return app_data;
    //}
    
});


document.addEventListener('DOMContentLoaded', function () {

    // Get all "navbar-burger" elements
    let $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // Check if there are any nav burgers
    if ($navbarBurgers.length > 0) {

        // Add a click event on each of them
        $navbarBurgers.forEach(function ($el) {
            $el.addEventListener('click', function () {

                // Get the target from the "data-target" attribute
                let target = $el.dataset.target;
                let $target = document.getElementById(target);

                // Toggle the class on both the "navbar-burger" and the "navbar-menu"
                $el.classList.toggle('is-active');
                $target.classList.toggle('is-active');

            });
        });
    }

    $(document)
    .on('click', '.navbar-item.has-dropdown', function(e) {
        $(this).toggleClass('is-active');
    })

    $('.menu-list li.folder > a').click(function(e) {
        $(this).parent().toggleClass('collapsed');
    })


});
