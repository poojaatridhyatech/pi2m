let tabs = function(e) {
    let target = e.target;
    let cls = 'is-active';
    let parent = $(target).parents('.tab-item');

    if (!parent.hasClass(cls)) {
        let tab = $(target).data('tab');
        $('.tab-item.' + cls + ', .tab-content.' + cls).removeClass(cls);
        parent.addClass(cls);
        $('.tab-content[data-tab-content="' + tab + '"]').addClass(cls);
    }
};

export default tabs;