let store = {
    debug: false,
    state: {
        selectedAdCampaignCats: []
    },
    setProperty(name, value) {
        if (this.debug) {
            console.log('setProperty triggered');
        }

        this.state[name] = value;
    },
    deleteProperty(name) {
        if (this.debug) {
            console.log('deleteProperty triggered');
        }

        delete this.state[name];
    }
};

export default store;