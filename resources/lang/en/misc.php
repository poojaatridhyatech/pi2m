<?php

return [
    /* Actions */
    'action' => [
        'cancel' => 'Cancel',
        'return-to-ads' => 'Return to ads',
        'return-to-users' => 'Return to users',
        'save' => 'Save',
    ],

    /* Content action types */
    'action-types' => [
        'created' => 'Created',
        'updated' => 'Updated',
    ],

    /* Descriptive pieces of text. */
    'descr' => [

    ],

    /* Entities. */
    'entities' => [
        'ad' => 'Ad',
        'app' => 'App',
        'category' => 'Category',
        'channel' => 'Channel',
        'video' => 'Video',
    ],

    /* Labels */
    'label' => [
        'active' => 'Active',
        'deactivated' => 'Deactivated',
        'description' => 'Description',
        'email' => 'E-mail',
        'name' => 'Name',
        'new-ad' => 'New Ad',
        'new-user' => 'New User',
        'price' => 'Price',
        'pushed' => 'Pushed',
        'roles' => 'Roles',
        'status' => 'Status',
        'subbed' => 'Subscribed',
        'thumbnail' => 'Thumbnail',
        'title' => 'Title',
        'unsubbed' => 'Unsubscribed',
        'url' => 'URL',
    ],

    /* Response messages */
    'resp' => [
        'error' => [

        ],
        'success' => [

        ],
    ],

    /* Titles */
    'title' => [
        'ads' => 'Ads',
        'ad-saves' => 'Ad Saves',
        'apps' => 'Apps',
        'advertising' => 'Advertising',
        'ad-settings' => 'Ad Settings',
        'banner-ads' => 'Banner Ads',
        'categories' => 'Categories',
        'channels' => 'Channels',
        'clicks' => 'Clicks',
        'content-dashboard' => 'Content Dashboard',
        'ctr' => 'CTR',
        'ads-dashboard' => 'Advertising Dashboard',
        'edit-ad' => 'Edit Ad',
        'edit-user' => 'Edit User',
        'general-info' => 'General Information',
        'impressions' => 'Impressions',
        'likes' => 'Likes',
        'new-ad' => 'New Ad',
        'new-user' => 'New User',
        'popularity' => 'Popularity',
        'product-info' => 'Product Information',
        'pushes' => 'Pushes',
        'summary' => 'Summary',
        'total-ads' => 'Total Ads',
        'total-subs' => 'Total Subscriptions',
        'total-time' => 'Total Time',
        'total-videos' => 'Total Videos',
        'unique-views' => 'Unique Views',
        'users' => 'Users',
        'videos' => 'Videos',
        'video-ads' => 'Video Ads',
        'view-count' => 'View Count',
        'views' => 'Views',
        'viewer-engagement' => 'Viewer Engagement',
    ],
];