<?php

return [
    'no_auth_header_found' => 'No Authorization header sent',
    'not_valid_jwt' => 'JWT token is not valid',
    'client_not_found' => 'This Client is not authorised',
];