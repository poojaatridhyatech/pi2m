@extends('layouts.app')

@section('script.data')
    <script>
        var app_data = {
            all_videos : {!!json_encode($videos)!!},
            all_apps : {!!json_encode($apps)!!},

            sel_videos: {!!json_encode($videos_selected)!!},
            sel_apps: {!!json_encode($apps_selected)!!},
        };
    </script>
@endsection

@section('content')

    <?php
        $isEditMode = isset($channel->uid) 
    ?>

    @if ($isEditMode)
        <form method="post" enctype="multipart/form-data" action="{{ route('channels.edit', $channel->uid) }}" class="form" id="channelForm">
    @else
        <form method="post" enctype="multipart/form-data" action="{{ route('channels.edit', 'new') }}" class="form" id="channelForm">
    @endif

    {{ csrf_field() }}

    <section class="top-container">
        <nav class="breadcrumb">
            <a href="{{route('channels.page')}}">
                <i class="icon-nav-arrow"></i>
                Return to Channels
            </a>
        </nav>
        <div class="level">
            <div class="level-left">
                <h1 class="title">{{$isEditMode? "Edit Channel" : "New Channel"}}</h1>
            </div>
            <div class="level-right">
                <button type="submit" class="button is-danger is-rounded vpp-btn">Save</button>
                <a class="button is-danger is-rounded is-outlined vpp-btn" onClick="window.history.back();">Cancel</a>
            </div>
        </div>
    </section>

    <div class="content-wrapper">

        <h2 class="subtitle">Gerneral Information</h2>

        <div class="box">

            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label">Title:</label>
                        <div class="control">
                            <input type="text" name="name" 
                                value="{{ $isEditMode? $channel->name : '' }}"
                                class="input" />
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">Description:</label>
                        <div class="">
                            <textarea rows="6" type="text" name="description" class="textarea">{{ $isEditMode? $channel->description : '' }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label">Label:</label>
                        <div class="control">
                            <input type="text" name="label" 
                                value="{{ $isEditMode? $channel->label : '' }}"
                                class="input" />
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">Thumbnail:</label>
                        <div class="control">
                        @if ($isEditMode)
                            <thumbnail-upload upload-link="{{ $channel->asset_path }}"></thumbnail-upload>
                        @else
                            <thumbnail-upload></thumbnail-upload>
                        @endif
                        </div>
                    </div>

                </div>
            </div>

            <div><strong>Creator</strong>: {{$user->name}}</div>

        </div>

        <h2 class="subtitle">APPS</h2>
        <?php // include('partials.user_clients') ?>
        <client-attacher></client-attacher>

        <h2 class="subtitle">VIDEOS</h2>
        <videos-attacher></videos-attacher>

        <div class="level">
            <div class="level-left"></div>
            <div class="level-right">
                <button type="submit" class="button is-danger is-rounded vpp-btn">Save</button>
                <a class="button is-danger is-rounded is-outlined vpp-btn" onClick="window.history.back();">Cancel</a>
            </div>
        </div>

    </div>
</form>

@endsection