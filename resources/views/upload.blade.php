@extends('layouts.app')

@section('script.data')
    <script>
        var app_data = {
            all_categories: {!!json_encode($categories)!!},
            all_channels: {!!json_encode($channels)!!},

            sel_categories: {!!json_encode($categories_selected)!!},
            sel_channels: {!!json_encode($channels_selected)!!},
        };
    </script>
@endsection

@section('content')

<?php
    $isEditMode = isset($content->uid);
?>

@if ($isEditMode)
    <form method="post" enctype="multipart/form-data" action="{{ route('content.upload', $content->uid) }}" class="form">
@else
    <form method="post" enctype="multipart/form-data" action="{{ route('content.upload') }}" class="form">
@endif

    {{ csrf_field() }}

    <section class="top-container">
        <nav class="breadcrumb">
            <a href="{{route('content.page')}}">
                <i class="icon-nav-arrow"></i>
                Return to Videos
            </a>
        </nav>
        <div class="level">
            <div class="level-left">
                <h1 class="title">{{$isEditMode? "Edit Video":"New Video"}}</h1>
            </div>
            <div class="level-right">
                <button type="submit" class="button is-danger is-rounded vpp-btn">Save</button>
                <a class="button is-danger is-rounded is-outlined vpp-btn" href="{{route('content.page')}}">Cancel</a>
            </div>
        </div>
    </section>

    <div class="content-wrapper">

        @if ($errors->any())
            <div class="notification is-danger">
                @foreach ($errors->all() as $error)
                    {{ $error }} <br>
                @endforeach
            </div>
        @endif

        <h2 class="subtitle">Gerneral Information</h2>
        <div class="box">

            <div class="columns">
                <div class="column">
                    <div class="field required">
                        <label class="label">Title:</label>
                        <div class="control">
                            <input type="text" name="title" 
                                value="{{ $isEditMode ? $content->title : old('title') }}"
                                class="input" required />
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">Description:</label>
                        <div class="">
                            <textarea rows="6" type="text" name="description" class="textarea">{{ $isEditMode? $content->description : old('description') }}</textarea>
                        </div>
                    </div>
                    
                    <div class="field required">
                        <label class="label">Expires At:</label>
                        <div class="control">
                            <datetime :format="{ year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: '2-digit'}" name="expires_at" type="datetime"
                            @if ($isEditMode)
                                value="{{ date('Y-m-d', strtotime($content->expires_at)) }}"
                            @else
                                value=""
                            @endif
                            ></datetime>
                        </div>
                    </div>
                    <div><strong>Creator</strong>: {{$user->name}}</div>
                </div>
                <div class="column">
                    @if ($isEditMode)
                        <div class="field">
                            <label class="label">Thumbnail:</label>
                            <div class="control">
                                <thumbnail-upload upload-link="{{ $content->asset_path }}"></thumbnail-upload>
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">Upload:</label>
                            <div class="control">
                                <content-upload upload-link="{{ $content->resource_uri }}"></content-upload>
                            </div>
                        </div>
                    @else
                        <div class="field required">
                            <label class="label">Thumbnail:</label>
                            <div class="control">
                                <thumbnail-upload></thumbnail-upload>
                            </div>
                        </div>
                        <div class="field required">
                            <label class="label">Upload:</label>
                            <div class="control">
                                <content-upload></content-upload>
                            </div>
                        </div>
                    @endif
                </div>
            </div>

        </div>

        <?php
        //include('partials.user_channels')
        
        //include('partials.user_categories')
        
        //include('partials.user_ads')
        ?>

        <h2 class="subtitle">CATEGORIES</h2>
        <categories-attacher></categories-attacher>

        <h2 class="subtitle">CHANNELS</h2>
        <channels-attacher></channels-attacher>

        <div class="level">
            <div class="level-left"></div>
            <div class="level-right">
                <button type="submit" class="button is-danger is-rounded vpp-btn">Save</button>
                <a class="button is-danger is-rounded is-outlined vpp-btn" href="{{route('content.page')}}">Cancel</a>
            </div>
        </div>
        
    </div>

</form>


@endsection