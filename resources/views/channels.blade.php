@extends('layouts.app')

@section('script.data')
    <script>
        var app_data = {
            selectedUser: {!!json_encode($selectedUser)!!},
            list: {!!json_encode($list->getCollection())!!},
        };
    </script>
@endsection

@section('content')

    <section class="top-container">
        <div class="level">
            <div class="level-left">
                <h1 class="title">Channels</h1>
            </div>
            <div class="level-right">
                <a href="{{ route('channels.edit_page', 'new') }}" class="button is-danger is-rounded is-pulled-right vpp-btn">
                    New Channel
                </a>
            </div>
        </div>
    </section>

    <div class="content-wrapper">
        
        <manage-channels show-user-select="true"></manage-channels>

        @include('partials.paginate')

    </div>


@endsection
