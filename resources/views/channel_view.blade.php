@extends('layouts.app')

@section('script.data')
    <script>
        var app_data = {
            list: {!! $list? json_encode($list->getCollection()) : '[]' !!},
        };
    </script>
@endsection

@section('content')

    <section class="top-container">
        <nav class="breadcrumb">
            <a href="{{route('channels.page')}}">
                <i class="icon-nav-arrow"></i>
                Return to Channels
            </a>
        </nav>
        <div class="level">
            <div class="level-left">
                <h1 class="title">{{$channel->name}}</h1>
            </div>
            <div class="level-right">
                <a  class="button is-danger is-rounded is-fullwidth vpp-btn"
                    href={{route('channels.edit', $channel->uid)}}>
                    Edit Channel
                </a>
            </div>
        </div>
        <div class="tabs">
            <ul>
                <li class="{{$active=='videos'? 'is-active' : ''}}">
                    <a href="{{ route('channels.contents', $channel->uid ) }}">{{ __('misc.title.videos') }}</a>
                </li>
                <li class="{{$active=='apps'? 'is-active' : ''}}">
                    <a href="{{ route('channels.apps', $channel->uid) }}">{{ __('misc.title.apps') }}</a>
                </li>
            </ul>
        </div>
    </section>

    <div class="content-wrapper">

        @if ($active=='videos')
            <manage-videos :parent-channel="'{{$channel->uid}}'"></manage-videos>
        @endif

        @if ($active=='apps')
            <manage-apps :parent-channel="'{{$channel->uid}}'"></manage-apps>
        @endif


        @if ($list)
            @include('partials.paginate')
        @endif

    </div>

@endsection
