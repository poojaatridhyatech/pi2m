@extends('layouts.app')

@section('script.data')
    <script>
        var app_data = {
            list: {!!json_encode($list)!!},
            content: {!!json_encode($content)!!},
        };
    </script>
@endsection

@section('content')

<section class="top-container">
        <nav class="breadcrumb">
            <a href="{{route('content.page')}}">
                <i class="icon-nav-arrow"></i>
                Return to Videos
            </a>
        </nav>
        <div class="level">
            <div class="level-left">
                <h1 class="title">{{ $content->title }} 
                    @if ($content->isExplicit())
                    <span class="explicit-tag">EXPLICIT</span>
                    @endif
                </h1>
            </div>
            <div class="level-right">

                <div style="flex-direction:column; display:flex; justify-content:space-between; position:absolute; top:30px; right:20px; height:100px;">
                    <a  class="button is-danger is-rounded is-fullwidth vpp-btn"
                        href={{route('content.edit_page', $content->uid)}}>
                        Edit Video
                    </a>
                    <div class="dropdown is-hoverable vpp-dropdown">
                        <div class="dropdown-trigger">
                            <button class="button is-danger is-fullwidth is-rounded is-outlined vpp-btn" aria-haspopup="true" aria-controls="dropdown-menu4">
                                <div>Add</div>
                                <span class="icon is-small">
                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                </span>
                            </button>
                        </div>
                        <div class="dropdown-menu" id="dropdown-menu-new" role="menu">
                            <div class="dropdown-content">
                                <a href={{route('channels.edit_page', 'new')}} class="dropdown-item">Channel</a>
                                <a href={{route('content.edit_page', 'new')}} class="dropdown-item">Video</a>
                            </div>
                        </div>
                    </div>

                <div>
            </div>
        </div>
    </section>

<div class="content-wrapper">

@php
$chans = [];
foreach ($list as $l) {
    $chans[] = $l;
}
@endphp
    
    <manage-push :contentname="'{{ str_replace("'", "", $content->title) }}'"></manage-push>

    @include('partials.paginate')

</div>

@stop
