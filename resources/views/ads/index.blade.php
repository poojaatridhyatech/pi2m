@extends('layouts.app')

@section('content')
    <section class="top-container">
        <div class="level">
            <div class="level-left">
                <h1>{{ __('misc.title.advertising') }}</h1>
            </div>
            <div class="level-right">
                <a href="{{ route('ads.create') }}" class="button is-danger is-rounded is-pulled-right vpp-btn">
                    {{ __('misc.label.new-ad') }}
                </a>
            </div>
        </div>
    </section>
    <div class="content-wrapper">
        <manage-ads></manage-ads>
    </div>
@endsection
