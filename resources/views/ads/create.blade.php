@extends('layouts.app')

@section('content')
    {!! Form::open(['url' => route('ads.store')]) !!}

    <section class="top-container">
        <nav class="breadcrumb">
            <a href="{{route('ads.index')}}">
                <i class="icon-nav-arrow"></i>
                {{ __('misc.action.return-to-ads') }}
            </a>
        </nav>
        <div class="level">
            <div class="level-left">
                <h1 class="title">{{ __('misc.title.new-ad') }}</h1>
            </div>
            <div class="level-right">
                {!! Form::submit(__('misc.action.save'), ['class' => 'button is-danger is-rounded vpp-btn']) !!}
                <a class="button is-danger is-rounded is-outlined vpp-btn" href="{{ route('ads.index') }}">
                    {{ __('misc.action.cancel') }}
                </a>
            </div>
        </div>
    </section>

    <div class="content-wrapper">
        <h2 class="subtitle">{{ __('misc.title.product-info') }}</h2>
        <div class="box">
            <div class="columns">
                <div class="column">
                    <div class="field required">
                        {!! Form::label('title', __('misc.label.title'), ['class' => 'label']) !!}
                        <div class="control">
                            {!! Form::text('title', null, [
                                'class' => 'input' . ($errors->has('title') ? ' is-danger' : ''),
                                'required',
                            ]) !!}
                        </div>
                        @if ($errors->has('title'))
                            <p class="help is-danger">{{ $errors->first('title') }}</p>
                        @endif
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        {!! Form::label('asset_path', __('misc.label.thumbnail'), ['class' => 'label']) !!}
                        <div class="control">
                            <thumbnail-upload></thumbnail-upload>
                        </div>
                    </div>
                </div>
            </div>

            <div class="columns">
                <div class="column">
                    <div class="field">
                        {!! Form::label('url', __('misc.label.url'), ['class' => 'label']) !!}
                        <div class="control">
                            {!! Form::text('url', null, [
                                'class' => 'input' . ($errors->has('url') ? ' is-danger' : ''),
                            ]) !!}
                        </div>
                        @if ($errors->has('url'))
                            <p class="help is-danger">{{ $errors->first('url') }}</p>
                        @endif
                    </div>
                </div>
                <div class="column">
                    <div class="field price required">
                        {!! Form::label('price', __('misc.label.price'), ['class' => 'label']) !!}
                        <div class="control">
                            <span class="select currency">
                                {!! Form::select('currency', $currencies, null, ['required']) !!}
                            </span>
                            {!! Form::text('price', null, [
                                'class' => 'input is-marginless' . ($errors->has('price') ? ' is-danger' : ''),
                                'required',
                            ]) !!}
                        </div>
                        @if ($errors->has('price'))
                            <p class="help is-danger">{{ $errors->first('price') }}</p>
                        @endif
                    </div>
                </div>
            </div>

            <div class="columns">
                <div class="column">
                    <div class="field required">
                        {!! Form::label('description', __('misc.label.description'), ['class' => 'label']) !!}
                        <div>
                            {!! Form::textarea('description', null, [
                                'class' => 'textarea' . ($errors->has('description') ? ' is-danger' : ''),
                                'rows' => 7,
                                'required',
                            ]) !!}
                        </div>
                        @if ($errors->has('description'))
                            <p class="help is-danger">{{ $errors->first('description') }}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <h2 class="subtitle">{{ __('misc.title.ad-settings') }}</h2>
        <ad-settings></ad-settings>
        <h2 class="subtitle">{{ __('misc.title.categories') }}</h2>
        <categories-attacher></categories-attacher>
        <div class="level">
            <div class="level-left"></div>
            <div class="level-right">
                {!! Form::submit(__('misc.action.save'), ['class' => 'button is-danger is-rounded vpp-btn']) !!}
                <a class="button is-danger is-rounded is-outlined vpp-btn" href="{{ route('ads.index') }}">
                    {{ __('misc.action.cancel') }}
                </a>
            </div>
        </div>
    </div>

    {!! Form::close() !!}
@endsection
