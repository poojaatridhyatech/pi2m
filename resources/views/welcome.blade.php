@extends('layouts.public')

@section('content')

        @include('partials.mobile_floating')
 
        <div class="with-pink slant-right-left" style="min-height: 600px;">
        
            @include('partials.public_navbar')

            <div class="section">
                <div class="container is-fluid">

                    <div class="columns lg-pads">
                        <div class="column is-half vpp-top">
                            <p class="top-large">Pushing the boundaries of video distribution</p>
                            <p class="top-small">The world’s first proactive content distribution service for mobile devices</p>

                            <a class="vpp-large-btn" onclick="goto('contact')">REQUEST DEMO</a>
                            <a class="vpp-large-btn vpp-learn-more is-hidden-touch" onclick="goto('technology')">LEARN MORE</a>
                        </div>
                        <div class="column is-half vpp-top top-img-contain is-hidden-mobile">
                            <img src="images/Iphone_1.png" alt="iPhone Home demonstration" class="vpp-img top-img" style="margin: 0; padding:0;">
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div class="with-dark technology-box" id="technology">
            <div class="section">
                <div class="container columns padded-text">
                    <div class="column is-half is-hidden-mobile">
                        <img src="/images/Iphone_2.png" alt="" style="margin-left: -3rem; margin-bottom: 40px; z-index: 1;">
                    </div>
                    <div class="column is-half">
                        <h1 class="with-dark-h1">TECHNOLOGY</h1>
                        <p class="with-dark-sub">Redefining the mobile video experience</p>
                        <p class="with-dark-main">Our proprietary technology uses a scalable framework to push personalised, short-form HD videos and contextualised advertising to mobile devices.</p>
                    </div>

                </div>

                <img src="/images/Iphone_2.png" alt="" style="margin-left: -1.5rem; margin-bottom: 40px;" class="is-hidden-desktop">
            </div>
        </div>

        <div class="with-pink slant-left-right" id="features">
            <div class="section">
                <!-- will only be shown on mobile -->
                <div class="container padded-text features-section">
                    <h1>FEATURES</h1>
                    <h2>Effective &amp; personalised  customer engagement </h2>

                    <div class="mobile-features is-hidden-desktop">
                        <div class="feature">
                            <i class="icon-search feature-icon"></i>
                            <span>Discoverability</span>
                            <p>Improve content visibility by actively pushing videos to users</p>
                        </div>

                    <div class="feature">
                        <i class="icon-like feature-icon"></i>
                        <span>Feedback</span>
                        <p>Capture customer feedback to optimise content engagement</p>
                    </div>

                    <div class="feature">
                        <i class="icon-star feature-icon"></i>
                        <span>Personalisation</span>
                        <p>Reward loyal customers with customised content</p>
                    </div>

                    <div class="feature">
                        <i class="icon-thumb-up feature-icon"></i>
                        <span>Effortless</span>
                        <p>Streamline content access and allow easy watching</p>
                    </div>

                    <div class="feature">
                        <i class="icon-key feature-icon"></i>
                        <span>Accessibility</span>
                        <p>Deliver superior, connectivity independent video experiences</p>
                    </div>

                    <div class="feature">
                        <i class="icon-megaphone feature-icon"></i>
                        <span>Powerful Impressions</span>
                        <p>Create targeted marketing campaigns to maximize ROI</p>
                    </div>
                </div>
                <!-- // -->

                <!-- desktop -->
                    <div class="container top-mrg is-hidden-mobile">
                        <div class="columns">
                            <div class="column is-4" style="text-align: right;">
                                <div class="feature">
                                    <div class="t">
                                        <span>Discoverability</span>
                                    <i class="icon-search feature-icon"></i>
                                    </div>
                                    
                                    <p>Improve content visibility by actively pushing videos to users</p>
                                </div>

                                <div class="feature">
                                    <span>Feedback</span>
                                    <i class="icon-like feature-icon"></i>
                                    <p>Capture customer feedback to optimise content engagement</p>
                                </div>

                                <div class="feature">
                                    <span>Personalisation</span>
                                    <i class="icon-star feature-icon"></i>
                                    <p>Reward loyal customers with customised content</p>
                                </div>
                            </div>

                            <div class="column is-4" style="text-align: center;">
                                <img src="/images/Iphone_3.png" alt="" style="margin-left: -1.5rem; margin-bottom: 40px;">
                            </div>

                            <div class="column is-4">
                                <div class="feature">
                                    <i class="icon-thumb-up feature-icon-right"></i>
                                    <span>Effortless</span>
                                    <p style="text-align: left;">Streamline content access and allow easy watching</p>
                                </div>

                                <div class="feature">
                                    <i class="icon-key feature-icon-right"></i>
                                    <span>Accessibility</span>
                                    <p style="text-align: left;">Deliver superior, connectivity independent video experiences</p>
                                </div>

                                <div class="feature">
                                    <i class="icon-megaphone feature-icon-right"></i>
                                    <span>Powerful Impressions</span>
                                    <p style="text-align: left;">Create targeted marketing campaigns to maximize ROI</p>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- // -->

                </div>
            </div>
        </div>

        <div class="with-dark" id="about">
            <div class="section">
                <div class="container padded-text">
                    <div class="columns">
                        <div class="column is-half">
                            <h1 class="with-dark-h1">ABOUT</h1>
                            <p class="with-dark-sub">Delivering the next generation in content distribution</p>
                            <p class="with-dark-main">The Pushit2Mobile concept was generated because of the challenges being experienced by content owners, using traditional passive distribution models.</p>
                            <p class="with-dark-main">The explosion of mobile video had oversaturated the marketplace and publishers sought a way of improving their content visibilty. Inspired, we set about creating Pushit2Mobile; a first in proactive content distribution for mobile devices.</p>
                        </div>
{{--                        <div class="column is-half">--}}
{{--                            <div class="team is-hidden-mobile">--}}
{{--                                <table width="100%">--}}
{{--                                <tr>--}}
{{--                                    <td style="text-align: right;"><img class="is-paddingless is-marginless" src="/images/We.png"></td>--}}
{{--                                    <td></td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <td style="text-align: right;"><img class="is-paddingless is-marginless" src="/images/Windsor.png"></td>--}}
{{--                                    <td style="text-align: left;"><img class="is-paddingless is-marginless" src="/images/Lee.png"></td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <td style="text-align: right;"><img class="is-paddingless is-marginless" src="/images/Terry.png"></td>--}}
{{--                                    <td style="text-align: left;"><img class="is-paddingless is-marginless" src="/images/Rob.png"></td>--}}
{{--                                </tr>--}}
{{--                                <tr>--}}
{{--                                    <td style="text-align: right;"></td>--}}
{{--                                    <td style="text-align: left;"><img class="is-paddingless is-marginless" src="/images/Vic.png"></td>--}}
{{--                                </tr>--}}
{{--                                </table>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
                </div>
                <div class="container">
                    <div class="team is-hidden-tablet">
                        <table width="100%">
                        <tr>
                            <td><img class="is-paddingless is-marginless" src="/images/We.png"></td>
                            <td><img class="is-paddingless is-marginless" src="/images/Windsor.png"></td>
                        </tr>
                        <tr>
                            <td><img class="is-paddingless is-marginless" src="/images/Terry.png"></td>
                            <td><img class="is-paddingless is-marginless" src="/images/Lee.png"></td>
                        </tr>
                        <tr>
                            <td><img class="is-paddingless is-marginless" src="/images/Rob.png"></td>
                            <td><img class="is-paddingless is-marginless" src="/images/Vic.png"></td>
                        </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="with-pink slant-left-right" id="contact">
            <div class="section">
                <div class="container padded-text features-section req-demo">
                    <h1>CONTACT</h1>
                    <h2>Request Demo</h2>
                    <br>
                    @if (session('demo_requested'))
                        <div class="notification is-success" id="demo-requested">
                            <button class="delete" onclick="document.getElementById('demo-requested').style.display='none';"></button>
                            Thank you for submitting your request for a Pushit2Mobile demo, our sales team will be in touch shortly
                        </div>
                    @endif
                    <form action="{{ route('request_demo') }}" method="POST">
                        {{ csrf_field() }}

                        <div class="field">
                            <label class="label">Name*</label>
                            <div class="control">
                                <input class="input vpp-contact" type="text" name="name">
                            </div>
                        </div>

                        <div class="field">
                            <label class="label">Email*</label>
                            <div class="control">
                                <input class="input vpp-contact" type="email" name="email">
                            </div>
                        </div>

                        <div class="field">
                            <label class="label">Message</label>
                            <div class="control">
                                <textarea class="textarea vpp-contact" type="email" name="message" style="border-radius: 4px;"></textarea>
                            </div>
                        </div>

                        <div class="btn-box" style="text-align: center; width: 100%; margin-top: 40px;">
                            <button type="submit" class="vpp-large-btn" href="mailto:info@pushit2mobile.com">SUBMIT</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


@stop
