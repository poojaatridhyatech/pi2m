@extends('layouts.app')

@section('script.data')

<script>
    var app_data = {
        list: {!!json_encode($list->getCollection())!!},
    };
</script>
@endsection


@section('content')

<section class="top-container">
    <div class="level">
        <div class="level-left">
            <h1 class="title">Devices</h1>
        </div>
        <div class="level-right">
            <a href="{{ route('devices.edit_page', 'new') }}" class="button is-danger is-rounded is-pulled-right vpp-btn">New Test Device</a>
        </div>
    </div>
</section>

<div class="content-wrapper">

    <manage-devices></manage-devices>

    @include('partials.paginate')

</div>

@endsection

