@if ($paginator->hasPages())
<div class="paginate-contain">
    <nav class="pagination is-centered is-rounded">
        @if ($paginator->onFirstPage())
            <a class="pagination-previous" disabled style="float:right;"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> PREV</a>
        @else
            <a href="{{ $paginator->previousPageUrl() }}" rel="prev" class="pagination-previous"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Previous</a>
        @endif

        @if ($paginator->hasMorePages())
            <a class="pagination-next" href="{{ $paginator->nextPageUrl() }}" rel="next">NEXT <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
        @else
            <a class="pagination-next" disabled>Next page <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
        @endif

        <ul class="pagination-list">
            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li><span class="pagination-ellipsis"><span>{{ $element }}</span></span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li><a class="pagination-link is-current">{{ $page }}</a></li>
                        @else
                            <li><a href="{{ $url }}" class="pagination-link">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach
        </ul>
    </nav>
</div>
@endif