<div class="checkbox">
    <label>
        <input type="checkbox" name="{{ $name }}"
        @if (isset($checked))
        checked="checked"
        @endif

        @if (isset($custom))
            {!! $custom !!}
        @endif
        >
        <i class="helper"></i>
    </label>
</div>