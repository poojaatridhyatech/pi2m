@extends('layouts.app')

@section('content')

{!! Form::open(['url' => route('account.update'), 'method' => 'post']) !!}
    
    <section class="top-container">
        <div class="level">
            <div class="level-left">
                <h1>Account Setting</h1>
            </div>
            <div class="level-right">
                <button type="submit" class="button is-danger is-rounded vpp-btn">Save</button>
                <a href="{{ URL::previous() }}" class="button is-danger is-rounded is-outlined vpp-btn">
                    Cancel
                </a>
            </div>
        </div>
    </section>
    <div class="content-wrapper">
        
        <h2 class="subtitle">Gerneral Information</h2>

        <div class="box">
            <div class="columns">
                <div class="column">
                    <div class="field required">
                        {!! Form::label('name', __('misc.label.name'), ['class' => 'label']) !!}
                        <div class="control">
                            {!! Form::text('name', $user->name, [
                                'class' => 'input' . ($errors->has('name') ? ' is-danger' : ''),
                                'required',
                            ]) !!}
                        </div>
                        @if ($errors->has('name'))
                            <p class="help is-danger">{{ $errors->first('name') }}</p>
                        @endif
                    </div>
                    <div class="field required">
                        {!! Form::label('email', __('misc.label.email'), ['class' => 'label']) !!}
                        <div class="control">
                            {!! Form::text('email', $user->email, [
                                'class' => 'input' . ($errors->has('email') ? ' is-danger' : ''),
                                'required',
                            ]) !!}
                        </div>
                        @if ($errors->has('email'))
                            <p class="help is-danger">{{ $errors->first('email') }}</p>
                        @endif
                    </div>

                    @if (session('errorMsg'))
                        <p class="help is-danger">
                            {{ session('errorMsg') }}
                        </p>
                    @endif

                    <div class="field">
                        <div class="level control">
                            <div class="level_left">
                                <a  class="button is-dark is-rounded is-outlined is-fullwidth vpp-btn"
                                    @click="$refs['accountsetting'].showChangePwd()">
                                    Change Password
                                </a>
                            </div>
                            <div class="level_right">
                                <a  class="button is-danger is-rounded is-outlined is-fullwidth vpp-btn"
                                    @click="$refs['accountsetting'].confirmDelete()">
                                    Delete Account
                                </a>
                            </div>
                        </div>
                    </div>
                    <account-setting ref="accountsetting"></account-setting>
                </div>

                <div class="column">

                    <div class="field">
                        <label class="label">Avatar</label>
                        <div class="control">
                            <thumbnail-upload upload-link="{{ $user->asset_path }}"></thumbnail-upload>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </div>

{!! Form::close() !!}

@endsection
