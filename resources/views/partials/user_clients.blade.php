<div class="form-group normal-form">
    <label>Clients:</label>
    @forelse(Auth::user()->clients as $client)
        <div class="checkbox">
            <label>
                <input 
                    type="checkbox" 
                    name="client_{{ $client->uid }}" 
                    label="{{ $client->name }}"
                >
                    {{ $client->name }}
            </label>
        </div>
    @empty
        <br>no clients created <a href="{{ route('clients.edit_page', 'new') }}" target="_blank">Create</a>
    @endforelse
</div>