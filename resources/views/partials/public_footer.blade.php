<footer class="footer-box is-hidden-desktop">

    <img src="/images/logo.png" style="height: 49.4px;">

    <div class="footer-text" style="margin-top: 36px;">
        <h1>FIND US</h1>
        <p>Elizabeth House, 39 York Road, London, SE1 7NQ</p>
        <h1>GET IN TOUCH</h1>
        <p>
            <a href="mailto:info@pushit2mobile.com" style="color: inherit;">info@pushit2mobile.com</a>
            <br>
            +44 (0)0123 456 789
        </p>
        <p>
            Copyright &copy; {{ date('Y') }} Pushit2Mobile Ltd <br>
{{--            Powered by <br><br>--}}
{{--            <img src="/images/eskimo.png" alt="">--}}
        </p>
    </div>
</footer>

<footer class="footer-box is-hidden-mobile">
    <div class="container">
        <div class="columns footer-text">
            <div class="column is-4">
                <img src="/images/logo.png" style="height: 74px;">
            </div>
            <div class="column is-4 footer-lg">
                <h1>FIND US</h1>
                <p>Elizabeth House, 39 York Road, London, SE1 7NQ</p>
            </div>
            <div class="column is-4 footer-lg">
                <h1>GET IN TOUCH</h1>
                <p>
                    <a href="mailto:info@pushit2mobile.com" style="color: inherit;">info@pushit2mobile.com</a>
                    <br>
{{--                    <a href="tel:+447407218206" style="color: inherit;">+44 (0)7407 218 206</a>--}}
                    +44 (0)0123 456 789
                </p>
            </div>
        </div>
        <div class="columns footer-text">
            <div class="column is-12">

                <p style="opacity: 0.3; font-size: 12px;">
                    Copyright &copy; {{ date('Y') }} Pushit2Mobile
{{--                    <br>Powered by <br><br>--}}
{{--                    <img src="/images/eskimo.png">--}}
                </p>
                
            </div>
        </div>
    </div>
</footer>
