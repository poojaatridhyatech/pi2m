<div class="form-group normal-form">
    <label>Categories:</label>
    @forelse(Auth::user()->categories as $category)
        <div class="checkbox">
            <label>
                <input 
                    type="checkbox" 
                    name="category_{{ $category->uid }}" 
                    label="{{ $category->name }}"
                >
                    {{ $category->name }}
            </label>
        </div>
    @empty
        <br>no categories created <a href="{{ route('categories.edit_page', 'new') }}" target="_blank">Create</a>
    @endforelse
</div>