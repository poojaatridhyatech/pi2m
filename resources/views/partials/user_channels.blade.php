<div class="form-group normal-form">
    <label>Channels:</label>
    @foreach(Auth::user()->allChannels() as $k => $channel)
        <div class="checkbox">
            <label>
                <input 
                    type="checkbox" 
                    name="channel_{{ $channel->uid }}" 
                    label="{{ $channel->label }}"
                >
                    {{ $channel->name }}
            </label>
        </div>
    @endforeach
</div>