<?php
    $menu = [
        [
            'url'   =>  route('dashboard.content.summary'),
            'icon' => 'icon-dashboard',
            'label' =>  'DASHBOARD',
            'active' => Request::segment(2) == 'dashboard',
            'only_admin' =>  false,
        ],
        [
            'url'   =>  'javascript:',
            'icon' => 'icon-content',
            'label' =>  'CONTENT',
            'sub'   =>  [
                ['url' => route('categories.page'), 'label' => 'CATEGORIES', 'active' => Request::segment(2) == 'categories'],
                ['url' => route('channels.page'), 'label' => 'CHANNELS', 'active' => Request::segment(2) == 'channels'],
                ['url' => route('content.page'), 'label' => 'VIDEOS', 'active' => Request::segment(2) == 'videos'],
                ['url' => route('ads.index'), 'label' => 'ADS', 'active' => Request::segment(2) == 'ads'],
            ],
            'active' => in_array(Request::segment(2), ['videos', 'channels', 'categories', 'ads']),
            'only_admin' =>  false,
        ],
        [
            'url'   =>  route('clients.page'),
            'icon' => 'icon-apps',
            'label' =>  'APPS',
            'active' => Request::segment(2) == 'clients',
            'only_admin' =>  false,
        ],
        [
            'url'   =>  route('users.index'),
            'icon' => 'icon-users',
            'label' =>  'USERS',
            'active' => Request::segment(2) == 'users',
            'only_admin' =>  true,
        ],
        [
            'url'   =>  route('devices.page'),
            'icon' => 'icon-test-devices',
            'label' =>  'TEST DEVICES',
            'active' => Request::segment(2) == 'devices',
            'only_admin' =>  false,
        ],

    ];

?>

<aside class="menu">
  <ul class="menu-list">
    @foreach ($menu as $item)
        @php
            $current = false;
            $clsActive = $item['active'] ? ' active' : '';
            $clsActive .= isset($item['sub']) ? ' folder' : '';
            $clsActive .= isset($item['sub']) && !$item['active'] ? " collapsed" : "";
        @endphp
        <li class="{{ $clsActive }}">
            @if ($item['only_admin'] === true)
                @if (Auth::user()->isAdmin())
                    <a href="{{ $item['url'] }}">
                        <i class="icon {{ $item['icon'] }}" style="font-size: 16px;"></i> {{ $item['label'] }}
                    </a>
                @endif
            @else
                <a href="{{ $item['url'] }}">
                    <i class="icon {{ $item['icon'] }}" style="font-size: 16px;"></i> {{ $item['label'] }}
                </a>
            @endif

            @if (isset($item['sub']))
                <ul class="sub-list">
                @foreach ($item['sub'] as $subitem)
                    @if ($subitem['active'])
                    @php
                        $current = true;
                    @endphp
                    @else
                    @php
                        $current = false;
                    @endphp
                    @endif
                    <li class="sub-item @if($current == true) sub-selected @endif">
                        <a href="{{ $subitem['url'] }}">{{ $subitem['label'] }}</a>
                    </li>
                @endforeach
                </ul>
            @endif
        </li>
    @endforeach
  </ul>
</aside>