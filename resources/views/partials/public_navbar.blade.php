@php
    $indexPage = (\Route::current()->getName() === 'public.index') ? true : false;
@endphp
<script>
    let goto = function(id) {
        closeNav();
        @if ( ! $indexPage ) 
            location.href = '@php echo url('/'); @endphp' + '#' + id;
        @else
            $('html, body').animate({
                scrollTop: $("#" + id).offset().top
            }, 1000);
        @endif
    };


/* Open when someone clicks on the span element */
function openNav() {
    document.getElementById("myNav").style.width = "100%";
    document.getElementById("float_screen").classList.add("is-hidden");


}

/* Close when someone clicks on the "x" symbol inside the overlay */
function closeNav() {
    document.getElementById("myNav").style.width = "0%";
    document.getElementById("float_screen").classList.remove('is-hidden');
}
</script>

<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">

                <nav class="navbar is-right" style="background: transparent;">
                    <div class="navbar-brand">
                        <a class="vpp-navbar" href="{{ env('CORP_SITE_URL') ? env('CORP_SITE_URL') : url('/') }}">
                        <img src="/images/logo.png" style="height: 39px;">
                        </a>

                        <div class="navbar-burger burger" onclick="openNav()">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div id="navMenubd-example" class="navbar-menu" style="">
                        <div class="navbar-end">
                            <div class="navbar-item is-hoverable">
                                
                                <a class="navbar-item is-active vpp-public-item" href="{{ url('/') }}">HOME</a>
                                <a class="navbar-item is-active vpp-public-item" onclick="goto('features')">FEATURES</a>
                                <a class="navbar-item is-active vpp-public-item" onclick="goto('about')">ABOUT</a>
                                <a class="navbar-item is-active vpp-public-item" onclick="goto('contact')">CONTACT</a>
                                <a class="navbar-item is-active vpp-public-item vpp-login-btn" href="{{ route('login') }}">
                                    @guest
                                        LOGIN
                                    @endguest
                                    @auth
                                        Hi, {{Auth::user()->name}}
                                    @endauth
                                </a>
                            </div>
                        </div>
                    </div>
                </nav>

        </div>
    @endif
</div>


<div id="myNav" class="overlay">

  <!-- Button to close the overlay navigation -->
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

  <!-- Overlay content -->
  <div class="overlay-content">
    <a  href="{{ url('/') }}">HOME</a>
    <a  onclick="goto('features')">FEATURES</a>
    <a  onclick="goto('about')">ABOUT</a>
    <a  onclick="goto('contact')">CONTACT</a>
    <a href="{{ route('login') }}">
        @guest
            LOGIN
        @endguest
        @auth
            ADMIN
        @endauth
    </a>
  </div>

</div>
