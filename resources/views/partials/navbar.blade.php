<nav class="navbar vpp-nav">
  <div class="navbar-brand vpp-brand">
    <a href="{{ env('CORP_SITE_URL') ? env('CORP_SITE_URL') : url('/') }}">
      <img src="/images/logo.png">
    </a>

    <button class="button navbar-burger">
      <span></span>
      <span></span>
      <span></span>
    </button>
  </div>

    <div class="navbar-menu">
      <form method="get" action="{{ route('content.page') }}">
        <input class="vpp-search" type="text" name="search" value="{{ empty($_GET['search'])? '' : $_GET['search'] }}" placeholder="Search videos">
      </form>
    </div>

    @if (Auth::user()->isAdmin())
        <notifications></notifications>
    @endif

    <div class="navbar-item has-dropdown vpp-dropdown-box">
      <a class="navbar-link vpp-dropdown">{{ ucwords(Auth::user()->name) }} <i class="fa fa-caret-down"></i></a>
      <div class="navbar-dropdown">
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
        </form>
        <a href="{{route('account.page')}}" class="navbar-item">
          Account Settings
        </a>
        <a id="logout-btn" class="navbar-item" href="{{ route('logout') }}"
           onclick="event.preventDefault();
           document.getElementById('logout-form').submit();">
          Logout
        </a>
      </div>
    </div>

</nav>


