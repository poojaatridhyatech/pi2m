<div class="form-group normal-form">
    <label>Ads:</label>
    @forelse(Auth::user()->ads_array as $ad)
        <div class="checkbox">
            <label>
                <input 
                    type="checkbox" 
                    name="ad_{{ $ad->uid }}" 
                    label="{{ $ad->title }}"
                >
                    {{ $ad->title }}
            </label>
        </div>
    @empty
        <br>no ads created <a href="{{ route('ads.create') }}" target="_blank">Create</a>
    @endforelse
</div>