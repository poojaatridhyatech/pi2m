@extends('layouts.app')

@section('script.data')
    <script>
        var app_data = {
            all_channels: {!!json_encode($channels)!!},
            sel_channels: {!!json_encode($channels_selected)!!},
        };
    </script>
@endsection

@section('content')

    <?php
        $isEditMode = isset($client->uid) 
    ?>


    @if ($isEditMode)
        <form method="post" enctype="multipart/form-data" action="{{ route('clients.edit', $client->uid) }}" class="form">
    @else
        <form method="post" enctype="multipart/form-data" action="{{ route('clients.edit') }}" class="form">
    @endif
    
    {{ csrf_field() }}

        <section class="top-container">
            <nav class="breadcrumb">
                <a href="{{route('clients.page')}}">
                    <i class="icon-nav-arrow"></i>
                    Return to Apps
                </a>
            </nav>
            <div class="level">
                <div class="level-left">
                    <h1 class="title">{{$isEditMode? "Edit App":"New App"}}</h1>
                </div>
                <div class="level-right">
                    <button type="submit" class="button is-danger is-rounded vpp-btn">Save</button>
                    <a class="button is-danger is-rounded is-outlined vpp-btn" href="{{route('clients.page')}}">Cancel</a>
                </div>
            </div>
        </section>

        <div class="content-wrapper">

            <h2 class="subtitle">Gerneral Information</h2>
            <div class="box">

                <div class="columns">
                    <div class="column is-half">
                        <div class="field required">
                            <label class="label">Name:</label>
                            <div class="control">
                                <input type="text" name="name" 
                                @if (isset($client->name))
                                    value="{{ $client->name }}"
                                @else
                                    value=""
                                @endif
                                class="input" />
                            </div>
                        </div>

                        <div class="field">
                            <label class="label">Description:</label>
                            <div>
                                <textarea name="description" rows="6" class="textarea">@if (isset($client->description)){{ $client->description }}@endif</textarea>
                            </div>
                        </div>
                        
                        <div>
                            <strong>Creator</strong> : {{$user->name}}
                        </div>

                    </div>
                    <div class="column is-half">
                        <div class="field required">
                            <label class="label">Onesignal App Id:</label>
                            <div class="control">
                                <input type="text" name="onesignal_app_id" 
                                    value="{{isset($client->onesignal_app_id)? $client->onesignal_app_id : '' }}"
                                    class="input" />
                            </div>
                        </div>
                        <div class="field required">
                            <label class="label">Onesignal API Key:</label>
                            <div class="control">
                                <input type="text" name="onesignal_api_key" 
                                    value="{{isset($client->onesignal_api_key)? $client->onesignal_api_key : '' }}"
                                    class="input" />
                            </div>
                        </div>

                        @if ($isEditMode)
                        <div class="field">
                            <label class="label">Access Token:</label>
                            <div class="">
                                <div style="overflow-x:auto; line-height: 20px; padding: 10px;">
                                    {{$client->secret}}
                                </div>
                            </div>
                        </div>
                        @endif

                    </div>
                </div>

                

            </div>

            <?php
            //include('partials.user_channels')
            ?>
            <h2 class="subtitle">CHANNELS</h2>
            <channels-attacher></channels-attacher>

            <div class="level">
                <div class="level-left"></div>
                <div class="level-right">
                    <button type="submit" class="button is-danger is-rounded vpp-btn">Save</button>
                    <a class="button is-danger is-rounded is-outlined vpp-btn" href="{{route('clients.page')}}">Cancel</a>
                </div>
            </div>

    </div>

    </form>


@endsection