

@extends('layouts.public')

@section('content')

        <div class="with-pink slant-right-left" style="min-height: 600px;">
        
            @include('partials.public_navbar')

            <div class="container">
                <div class="columns login-box">
                    <div class="column is-5">
                        <h1 class="top-large is-right login-h1">Forgot your password?</h1>
                        <p class="login-p">Don’t worry, we’ll send you an email with a reset link.</p>
                    </div>
                    <div class="column is-2 is-hidden-mobile">
                        <div style="height: 200px; border-left: 1px solid #fff; width: 1px; margin:0 auto; opacity: 0.5;
"></div>
                    </div>
                    <div class="column is-5 login-form">

                        <form class="form-horizontal" method="POST" action="{{ route('post.reset') }}">
                        {{ csrf_field() }}

                        @if (Session::has('success'))
                            <div class="notification is-success">
                                Your reset email is on it's way
                            </div>
                        @elseif (Session::has('error'))
                            <div class="notification is-error">
                                Something went wrong when trying to reset your password. Please try again in a second.
                            </div>
                        @endif

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="login-label">Email*</label>

                            <div class="field">
                                <input id="email" type="email" class="input vpp-contact" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="vpp-large-btn" id="submitBtn">
                                    SUBMIT
                                </button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>


        </div>
@stop











