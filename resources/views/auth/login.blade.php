

@extends('layouts.public')

@section('content')

        <div class="with-pink slant-right-left" style="min-height: 600px;">
        
            @include('partials.public_navbar')

            <div class="container">
                <div class="columns login-box">
                    <div class="column is-5">
                        <h1 class="top-large is-right login-h1">Welcome to Pushit2Mobile</h1>
                        <p class="login-p">Please sign in to continue</p>
                    </div>
                    <div class="column is-2 is-hidden-mobile">
                        <div style="height: 200px; border-left: 1px solid #fff; width: 1px; margin:0 auto; opacity: 0.5;
"></div>
                    </div>
                    <div class="column is-5 login-form">
                        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="login-label">Email*</label>

                            <div class="field">
                                <input id="email" type="email" class="input vpp-contact" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="login-label">Password*</label>

                            <div class="field">
                                <input id="password" type="password" class="input vpp-contact" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="vpp-large-btn" id="submitBtn">
                                    LOGIN
                                </button>

                                <a class="btn btn-link vpp-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>


        </div>
@stop
