<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.ico" />

        <title>Pushit2Mobile</title>

        <link href="{{ asset('css/public.css') }}" rel="stylesheet">

    </head>
    <body>

    @include('partials.forensics')

    @yield('content')

    @include('partials.public_footer')

        <script src="{{ mix('js/app.js') }}"></script>

{{--  @todo Update GA snippet  --}}
{{--        @include('partials.google_analytics')--}}

    </body>
