<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.ico" />
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>window.Laravel = { csrfToken: '{{ csrf_token() }}' }</script>

    <title>Pushit2Mobile Admin</title>
    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css" />
    
    <script src="https://static.filestackapi.com/v3/filestack.js"></script> 
    <script>
        let fsClient = filestack.init('AMkBO5YIeTSL3rN6gyUw7z');
        // let fsClient = filestack.init('AwUFgdeoLQqubjLe2IYk9z');

        //window.URL = '{{ url("/") }}';
    </script>
</head>
<body>
    <div id="app">

        @if(Auth::user())
            @include('partials.navbar')
            <div id="full">
                <div class="is-sidebar-menu is-hidden-mobile">
                    @include('partials.sidebar')
                </div>
                <div class="is-main-content">
                    @yield('content')
                </div>
            </div>
        @endif

    </div>

    <!-- Scripts -->
    @yield('script.data')
    <script src="{{ mix('js/app.js') }}"></script>
    
</body>
</html>
