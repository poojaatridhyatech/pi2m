@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <form method="post" enctype="multipart/form-data" action="{{ route('content.push', $content->uid) }}" class="form">
                {{ csrf_field() }}

                <div class="form-group normal-form">
                    <label>Channels:</label>
                    @foreach($content->channels as $channel)
                        <div class="checkbox">
                            <label>
                                <input 
                                    type="checkbox" 
                                    name="channel_{{ $channel->uid }}" 
                                    label="{{ $channel->label }}"
                                    checked
                                >
                                    {{ $channel->label }}
                            </label>
                        </div>
                    @endforeach
                </div>

                @if (Auth::user()->hasRole(App\Role::where('name', 'admin')->first()))
                <h2>Send to One device only</h2>
                    <div class="form-group normal-form">
                        <label>Single Device (OneSignal UUID):</label>
                        <input type="text" name="device_uid" class="form-control">
                    </div>
                @endif

                <div class="form-group normal-form">
                    <label>Notification Content:</label>
                    <input type="text" name="notification" class="form-control" value="{{ $content->title }} is now available!">
                </div>

                <div class="form-group normal-form">
                    <button class="btn btn-primary btn-sm pull-right">Push</button>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection