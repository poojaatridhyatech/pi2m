@extends('layouts.app')

@section('script.data')
    <script>
        var app_data = {
            all_videos : {!!json_encode($videos)!!},

            sel_videos: {!!json_encode($videos_selected)!!},
        };
    </script>
@endsection

@section('content')

    <?php
        $isEditMode = isset($category->name) 
    ?>

    @if ($isEditMode)
        <form method="post" enctype="multipart/form-data" action="{{ route('categories.edit', $category->uid) }}" class="form" id="categoryForm">
    @else
        <form method="post" enctype="multipart/form-data" action="{{ route('categories.edit', 'new') }}" class="form" id="categoryForm">
    @endif

        {{ csrf_field() }}
        
        <section class="top-container">
            <nav class="breadcrumb">
                <a href="{{route('categories.page')}}">
                    <i class="icon-nav-arrow"></i>
                    Return to Categories
                </a>
            </nav>
            <div class="level">
                <div class="level-left">
                    <h1 class="title">{{$isEditMode? "Edit Category":"New Category"}}</h1>
                </div>
                <div class="level-right">
                    <button type="submit" class="button is-danger is-rounded vpp-btn">Save</button>
                    <a class="button is-danger is-rounded is-outlined vpp-btn" href="{{route('categories.page')}}">Cancel</a>
                </div>
            </div>
        </section>

        <div class="content-wrapper">

            <h2 class="subtitle">Gerneral Information</h2>
            <div class="box">

                    <div class="columns">
                        <div class="column">
                            <div class="field">
                                <label class="label">Title:</label>
                                <div class="control">
                                    <input type="text" name="name" 
                                        value="{{ $isEditMode? $category->name : '' }}"
                                        class="input" />
                                </div>
                            </div>
                        </div>
                        <div class="column">
                            <div class="field">
                                <label class="label">Thumbnail:</label>
                                <div class="control">
                                @if ($isEditMode)
                                    <thumbnail-upload upload-link="{{ $category->asset_path }}"></thumbnail-upload>
                                @else
                                    <thumbnail-upload></thumbnail-upload>
                                @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="field">
                        <label class="label">Description:</label>
                        <div class="">
                            <textarea rows="6" type="text" name="description" class="textarea">{{ $isEditMode? $category->description : '' }}</textarea>
                        </div>
                    </div>
                    <div><strong>Creator</strong>: {{$user->name}}</div>
                    
            </div>

            <h2 class="subtitle">VIDEOS</h2>
            <videos-attacher></videos-attacher>

            <div class="level">
                <div class="level-left"></div>
                <div class="level-right">
                    <button type="submit" class="button is-danger is-rounded vpp-btn">Save</button>
                    <a class="button is-danger is-rounded is-outlined vpp-btn" href="{{route('categories.page')}}">Cancel</a>
                </div>
            </div>
            
        </div>

    </form>

@endsection