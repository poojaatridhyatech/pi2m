@extends('layouts.app')

@section('script.data')
    <script>
        var app_data = {
            selectedUser: {!!json_encode($selectedUser)!!},
            selectedCategory: {!!json_encode($selectedCategories)!!},
            list: {!!json_encode($list->getCollection())!!},
            params: {!!json_encode($params)!!},
        };
    </script>
@endsection

@section('content')
    <?php
        $link_videos = route('content.page');
        if (!empty($channel)) {
            $link_videos = route('channels.contents', $channel->uid);
        }
    ?>
    <section class="top-container">
        <div class="level">
            <div class="level-left">
                <h1 class="title">{{!empty($channel)? $channel->name : 'Videos'}}</h1>
            </div>
            <div class="level-right">
                <a href="{{ route('content.edit_page', 'new') }}" class="button is-danger is-rounded is-pulled-right vpp-btn">New Video</a>
            </div>
        </div>
    </section>

    <div class="content-wrapper">
        @php
            $userSelect = false;
            if (Auth::user()->isAdmin()) {
                $userSelect = true;
            }
        @endphp
        <manage-videos show-user-select="{{ $userSelect }}"></manage-videos>
        
        @include('partials.paginate')

    </div>

@endsection
