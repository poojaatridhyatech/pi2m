@extends('layouts.app')

@section('content')

    <?php
        $isEditMode = isset($device->uid) 
    ?>


    @if ($isEditMode)
        <form method="post" enctype="multipart/form-data" action="{{ route('devices.edit', $device->uid) }}" class="form">
    @else
        <form method="post" enctype="multipart/form-data" action="{{ route('devices.edit') }}" class="form">
        
    @endif
    
    {{ csrf_field() }}

        <section class="top-container">
            <nav class="breadcrumb">
                <a href="{{route('devices.page')}}">
                    <i class="icon-nav-arrow"></i>
                    Return to Test Devices
                </a>
            </nav>
            <div class="level">
                <div class="level-left">
                    <h1 class="title">{{$isEditMode? "Edit Test Device":"New Test Device"}}</h1>
                </div>
                <div class="level-right">
                    <button type="submit" class="button is-danger is-rounded vpp-btn">Save</button>
                    <a class="button is-danger is-rounded is-outlined vpp-btn" href="{{route('devices.page')}}">Cancel</a>
                </div>
            </div>
        </section>

        <div class="content-wrapper">

            <h2 class="subtitle">Gerneral Information</h2>
            <div class="box">

                <div class="field required">
                    <label class="label">Name:</label>
                    <div class="control">
                        <input type="text" name="name" 
                        @if (isset($device->name))
                            value="{{ $device->name }}"
                        @else
                            value=""
                        @endif
                        class="input" />
                    </div>
                </div>

                <div class="field required">
                    <label class="label">Single Device (OneSignal UUID):</label>
                    <div class="control">
                        <input type="text" name="onesignal_uid" value="{{$isEditMode? $device->onesignal_uid: ''}}" class="input" />
                    </div>
                </div>

            </div>

    </div>

    </form>


@endsection