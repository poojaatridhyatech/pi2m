@extends('layouts.app')

@section('script.data')

<script>
    var app_data = {
        selectedUser: {!!json_encode($selectedUser)!!},
        users: {!!json_encode($users)!!},
        list: {!!json_encode($list->getCollection())!!},
    };
</script>
@endsection


@section('content')

<section class="top-container">
    <div class="level">
        <div class="level-left">
            <h1 class="title">Apps</h1>
        </div>
        <div class="level-right">
            <a href="{{ route('clients.edit_page', 'new') }}" class="button is-danger is-rounded is-pulled-right vpp-btn">New App</a>
        </div>
    </div>
</section>

<div class="content-wrapper">

    <manage-apps show-user-select="true"></manage-apps>

    @include('partials.paginate')

</div>

@endsection

