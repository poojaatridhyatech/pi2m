@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <p class="has-text-danger is-uppercase has-text-weight-bold">Do you really need to be here?</p>
        <dashboard-reindex></dashboard-reindex>
    </div>
@endsection
