@extends('layouts.app')

@section('content')
    <section class="top-container">
        <div class="level">
            <div class="level-left">
                <h1 class="title">{{ __('misc.title.content-dashboard') }}</h1>
            </div>
            <div class="level-right"></div>
        </div>
        <div class="tabs">
            <ul>
                <li>
                    <a href="{{ route('dashboard.content.summary') }}">{{ __('misc.title.summary') }}</a>
                </li>
                <li class="is-active">
                    <a href="{{ route('dashboard.content.channels') }}">{{ __('misc.title.channels') }}</a>
                </li>
                <li>
                    <a href="{{ route('dashboard.content.videos') }}">{{ __('misc.title.videos') }}</a>
                </li>
            </ul>
        </div>
    </section>
    <div class="content-wrapper">

        <dashboard-channel></dashboard-channel>
        
    </div>
@endsection
