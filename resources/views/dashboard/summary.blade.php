@extends('layouts.app')

@section('content')
    <section class="top-container">
        <div class="level" >
            <div class="level-left">
                    <h1 class="title">
                        Hi {{$user->name}}!
                        <p style="font-weight:normal; font-size:16px;letter-spacing: 2px; opacity:0.5;">
                            Checkout the performance of your latest videos & advertising
                        </p>
                    </h1>
            </div>
            <div class="level-right"></div>
        </div>
        <div class="tabs">
            <ul>
                <li class="{{$active=='videos'? 'is-active' : ''}}">
                    <a href="{{ route('dashboard.content.summary') }}">{{ __('misc.title.videos') }}</a>
                </li>
                <li class="{{$active=='ads'? 'is-active' : ''}}">
                    <a href="{{ route('dashboard.ad.summary') }}">{{ __('misc.title.advertising') }}</a>
                </li>
            </ul>
        </div>
    </section>
    <div class="content-wrapper">

        @if ($active=='videos')
            <dashboard-summary></dashboard-summary>
        @elseif ($active=='ads')
            <ad-dashboard-summary></ad-dashboard-summary>
        @endif

    </div>
@endsection
