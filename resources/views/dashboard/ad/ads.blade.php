@extends('layouts.app')

@section('content')

    <section class="top-container">
        <div class="level">
            <div class="level-left">
                <h1 class="title">{{ __('misc.title.ads-dashboard') }}</h1>
            </div>
            <div class="level-right"></div>
        </div>
        <div class="tabs">
            <ul>
                <li>
                    <a href="{{ route('dashboard.ad.summary') }}">{{ __('misc.title.summary') }}</a>
                </li>
                <li class="is-active">
                    <a href="{{ route('dashboard.ad.ads') }}">{{ __('misc.title.ads') }}</a>
                </li>
            </ul>
        </div>
    </section>
    <div class="content-wrapper">

        <ad-dashboard-ads></ad-dashboard-ads>

    </div>

@endsection