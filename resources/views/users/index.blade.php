@extends('layouts.app')

@section('content')
    <section class="top-container">
        <div class="level">
            <div class="level-left">
                <h1>{{ __('misc.title.users') }}</h1>
            </div>
            <div class="level-right">
                <a href="{{ route('users.create') }}" class="button is-danger is-rounded is-pulled-right vpp-btn">
                    {{ __('misc.label.new-user') }}
                </a>
            </div>
        </div>
    </section>
    <div class="content-wrapper">
        <manage-users></manage-users>
    </div>
@endsection
