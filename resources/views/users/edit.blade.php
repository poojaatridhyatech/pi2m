@extends('layouts.app')

@section('content')
    {!! Form::open(['url' => route('users.update', $user->uid), 'method' => 'put']) !!}

    <section class="top-container">
        <nav class="breadcrumb">
            <a href="{{route('users.index')}}">
                <i class="icon-nav-arrow"></i>
                {{ __('misc.action.return-to-users') }}
            </a>
        </nav>
        <div class="level">
            <div class="level-left">
                <h1 class="title">{{ __('misc.title.edit-user') }}</h1>
            </div>
            <div class="level-right">
                {!! Form::submit(__('misc.action.save'), ['class' => 'button is-danger is-rounded vpp-btn']) !!}
                <a class="button is-danger is-rounded is-outlined vpp-btn" href="{{ route('users.index') }}">
                    {{ __('misc.action.cancel') }}
                </a>
            </div>
        </div>
    </section>

    <div class="content-wrapper">
        <h2 class="subtitle">{{ __('misc.title.general-info') }}</h2>
        <div class="box">
            <div class="columns">
                <div class="column">
                    <div class="field required">
                        {!! Form::label('name', __('misc.label.name'), ['class' => 'label']) !!}
                        <div class="control">
                            {!! Form::text('name', $user->name, [
                                'class' => 'input' . ($errors->has('name') ? ' is-danger' : ''),
                                'required',
                            ]) !!}
                        </div>
                        @if ($errors->has('name'))
                            <p class="help is-danger">{{ $errors->first('name') }}</p>
                        @endif
                    </div>
                    <div class="field required">
                        {!! Form::label('email', __('misc.label.email'), ['class' => 'label']) !!}
                        <div class="control">
                            {!! Form::text('email', $user->email, [
                                'class' => 'input' . ($errors->has('email') ? ' is-danger' : ''),
                                'required',
                            ]) !!}
                        </div>
                        @if ($errors->has('email'))
                            <p class="help is-danger">{{ $errors->first('email') }}</p>
                        @endif
                    </div>
                </div>

                <div class="column">
                    <div class="field">
                        <label class="label">{{ __('misc.label.roles') }}</label>
                        <div class="control">
                            @foreach ($roles as $role)
                                <label>
                                    {!! Form::checkbox('roles[]', $role->id, in_array($role->id, $activeRoles)) !!}
                                    {{ $role->label }}
                                </label>
                            @endforeach
                        </div>
                        @if ($errors->has('roles'))
                            <p class="help is-danger">{{ $errors->first('roles') }}</p>
                        @endif
                    </div>
                    <div class="field">
                        <label class="label">{{ __('misc.label.status') }}</label>
                        <div class="control">
                            <label>
                                {!! Form::radio('status', 1, $user->status) !!}
                                {{ __('misc.label.active') }}
                            </label>
                            <label>
                                {!! Form::radio('status', 0, !$user->status) !!}
                                {{ __('misc.label.deactivated') }}
                            </label>
                        </div>
                        @if ($errors->has('status'))
                            <p class="help is-danger">{{ $errors->first('status') }}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="level">
            <div class="level-left"></div>
            <div class="level-right">
                {!! Form::submit(__('misc.action.save'), ['class' => 'button is-danger is-rounded vpp-btn']) !!}
                <a class="button is-danger is-rounded is-outlined vpp-btn" href="{{ route('users.index') }}">
                    {{ __('misc.action.cancel') }}
                </a>
            </div>
        </div>
    </div>

    {!! Form::close() !!}
@endsection
