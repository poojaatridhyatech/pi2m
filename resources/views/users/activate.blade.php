

@extends('layouts.public')

@section('content')

        <div class="with-pink slant-right-left" style="min-height: 600px;">
        
            @include('partials.public_navbar')

            <div class="container">
                <div class="columns login-box">
                    <div class="column is-5">
                    @if ($activate)
                        <h1 class="top-large is-right login-h1">Activate account</h1>
                        <p class="login-p">Choose a password for your account</p>
                    @else
                        <h1 class="top-large is-right login-h1">Reset your password</h1>
                        <p class="login-p">Enter a new password for your account</p>
                    @endif
                    </div>
                    <div class="column is-2 is-hidden-mobile">
                        <div style="height: 200px; border-left: 1px solid #fff; width: 1px; margin:0 auto; opacity: 0.5;
"></div>
                    </div>
                    <div class="column is-5 login-form">

                        <form method="POST" action="{{ route('post.activate') }}?token={{ $hash }}">
                        {{ csrf_field() }}

                        @if (Session::has('passwords_dont_match'))
                            <div class="notification is-danger">
                                Passwords must match
                            </div>
                        @endif

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="login-label">New Password*</label>

                            <div class="field">
                                <input id="password" type="password" class="input vpp-contact" name="password" value="{{ old('password') }}" required autofocus>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_verify') ? ' has-error' : '' }}">
                            <label for="password_verify" class="login-label">Confirm Password*</label>

                            <div class="field">
                                <input id="password_verify" type="password" class="input vpp-contact" name="password_verify" required>

                                @if ($errors->has('password_verify'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_verify') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="vpp-large-btn" id="submitBtn">
                                    SUBMIT
                                </button>

                                <!-- <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a> -->
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>


        </div>
@stop
