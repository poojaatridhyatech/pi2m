@extends('layouts.app')

@section('script.data')
    <script>
        var app_data = {
            list: {!! $list? json_encode($list->getCollection()) : '[]' !!},
        };
    </script>
@endsection

@section('content')

    <section class="top-container">
        <nav class="breadcrumb">
            <a href="{{route('categories.page')}}">
                <i class="icon-nav-arrow"></i>
                Return to Categories
            </a>
        </nav>
        <div class="level">
            <div class="level-left">
                <h1 class="title">{{$category->name}}</h1>
            </div>
            <div class="level-right">
                <a  class="button is-danger is-rounded is-fullwidth vpp-btn"
                    href={{route('categories.edit', $category->uid)}}>
                    Edit Category
                </a>
            </div>
        </div>
        <div class="tabs">
            <ul>
                <li class="{{$active=='videos'? 'is-active' : ''}}">
                    <a href="{{ route('categories.videos', $category->uid ) }}">{{ __('misc.title.videos') }}</a>
                </li>
                <li class="{{$active=='ads'? 'is-active' : ''}}">
                    <a href="{{ route('categories.ads', $category->uid) }}">{{ __('misc.title.ads') }}</a>
                </li>
            </ul>
        </div>
    </section>

    <div class="content-wrapper">

        @if ($active=='videos')
            <manage-videos :parent-category="'{{$category->uid}}'"></manage-videos>
        @endif

        @if ($active=='ads')
            <manage-ads :parent-category="'{{$category->uid}}'"></manage-ads>
        @endif

        @if ($list)
            @include('partials.paginate')
        @endif

    </div>

@endsection
